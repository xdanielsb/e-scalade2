package com.example.judith.bluetoothtest.Classes.GeneralRFID;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.content.Context;
import android.util.Log;

import com.acs.bluetooth.Acr1255uj1Reader;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCReader;
import com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFBluetoothGattCallback;
import com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFReader;

/**
 * RFIDBluetoothReader is a wrapper class for every instance of a RFID Reader. This class is used
 * so far to wrapp two types of RFID readers : NFC readers from ACS, and UHF readers from
 * SmartLink.
 * This class implements the RFID_Reader Interface which aims to contain all possible methods to
 * be necessary for any RFID reader.
 */
public class RFIDBluetoothReader implements RFID_Reader {

    /**
     * Instance for a NFC reader
     */
    private NFCReader mNFCReader;

    /**
     * Instance for a UHF reader
     */
    private com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFReader mUHFReader;

    /**
     * int to identify the reader, 1 or 2. Not used so far but might be.
     */
    private int mDeviceNumber;

    /**
     * An int indicating which type of reader is used : 1 for UHF and 2 for NFC. This type is set
     * in the reader constructor thanks to the readerType String which contains the formal name
     * of the BluetoothDevice.
     * This int allows then the class to know which one of its inner reader variable to use when
     * called in ReadersActivity.
     */
    private int mReaderType;

    private Context mContext;


    public RFIDBluetoothReader(Activity activity, int deviceNumber, String readerType){
        mContext = (Context)activity;
        mDeviceNumber = deviceNumber;
        if(readerType.contains("smartLINK")) {
            mUHFReader = new com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFReader(activity, deviceNumber);
            mReaderType = 1;
            Log.d("RFID READER", "CREATION OF " + readerType);
        }
        else if(readerType.contains("ACR1255U-J1")){
            mNFCReader = new NFCReader(activity, deviceNumber);
            mReaderType = 2;
            Log.d("RFID READER", "CREATION OF " + readerType);
        }
    }

    /**
     * Starts the tag polling of the reader connected. Depending of the reader type, a different
     * type of polling is started.
     */
    @Override
    public boolean startPolling() {
        if(mReaderType == 1)
            return mUHFReader.startTagPollingUHF();
        else if (mReaderType == 2)
            return mNFCReader.startTagPollingNFC();
        else
            return false;
    }

    /**
     * Stops the tag polling of the reader connected. Depending of the reader type, a different
     * type of polling is started.
     */
    @Override
    public boolean stopPolling() {
        if(mReaderType == 1) {
            mUHFReader.stopTagPollingUHF(false);
            return true;
        }
        else if(mReaderType == 2)
            return (mNFCReader.stopTagPollingNFC());
        else
            return false;
    }

    /**
     * Authenticate the reader connected. If the reader is a ACS NFC reader, the authentication is
     * made thanks to an authentication key.
     * If the reader is a UHF SmartLink reader, no authentication is needed.
     */
    @Override
    public boolean authenticateReader(byte[] authenticationKey) {
        if(mReaderType == 2){
            return(mNFCReader.authenticateNFC(authenticationKey));
        }
        else if(mReaderType == 1)
            return true;
        else
            return false;
    }

    /**
     * @return a boolean indicating if the instance of BluetoothReader is null. True if yes, false if
     * not.
     */
    @Override
    public boolean isReaderNull() {
        if (mReaderType == 1)
            return mUHFReader == null;
        else if(mReaderType == 2)
            return mNFCReader.isACSReaderNull();
        return true;
    }

    /**
     * Activates the reader connected. If the reader is a ACS NFC reader, the activation is
     * necessary by calling the enableNotification method.
     * If the reader is a UHF SmartLink reader, no special operation is required.
     * @see com.acs.bluetooth.BluetoothReader#enableNotification(boolean)
     */
    @Override
    public void enableNotification(boolean enable) {
        if(mReaderType == 2 && mNFCReader.isReaderInstanceOfACR1255UJ1())
            mNFCReader.enableNotificationNFC(enable);

    }

    /**
     * @return the ACS NFC reader instance of this object.
     */
    @Override
    public NFCReader getNFCReader() {
        return mNFCReader;
    }

    /**
     * @return the SmartLink UHF instance of this object.
     */
    @Override
    public UHFReader getUHFReader() {
        return mUHFReader;
    }

    /**
     * Sets the BluetoothGattCallback and BluetoothGatt instances corresponding to the reader
     * instance. This operation is needed only for the SmartLink UHF reader because it needs to
     * communicate whit its callback and gatt instances.
     * @param bluetoothGatt
     *                  the BluetoothGatt instance corresponding to the reader connection
     * @param bluetoothGattCallback
     *                  the BluetoothGattCallback instance corresponding to the reader connection
     */
    @Override
    public void setGattAndCallback(BluetoothGatt bluetoothGatt, BluetoothGattCallback bluetoothGattCallback) {
        if(mReaderType == 1){
            mUHFReader.setBluetoothGatt(bluetoothGatt);
            mUHFReader.setBluetoothGattCallback((UHFBluetoothGattCallback)bluetoothGattCallback);
        }
    }
}
