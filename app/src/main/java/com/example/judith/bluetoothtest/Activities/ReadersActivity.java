package com.example.judith.bluetoothtest.Activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.acs.bluetooth.BluetoothReader;
import com.example.judith.bluetoothtest.Classes.DataSampleHashMap;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DataSample;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCBluetoothGattCallback;
import com.example.judith.bluetoothtest.Classes.GeneralRFID.RFIDBluetoothGattCallback;
import com.example.judith.bluetoothtest.Classes.GeneralRFID.RFIDBluetoothManager;
import com.example.judith.bluetoothtest.Classes.GeneralRFID.RFIDBluetoothReader;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCReader;
import com.example.judith.bluetoothtest.Classes.ReaderToActivity;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCBluetoothManager;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCUtils;
import com.example.judith.bluetoothtest.Fragments.RFIDClimbingFragment;
import com.example.judith.bluetoothtest.Fragments.RFIDRecordingFragment;
import com.example.judith.bluetoothtest.R;
import com.opencsv.CSVWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * <b> Activity which handles the connection and communication with the RFID readers.</b>
 * <p>
 *     ReadersActivity is the activity to which the user arrives whereas he choose climbing mode
 *     or recording mode.
 *     The activity is the same, but it communicates with two different fragments to display
 *     different UIs according to the mode chosen.
 * </p>
 * <p>
 *     The first fragment is RFIDClimbingFragment, displayed during the climbing mode and the second
 *     one is RFIDRecordingFragment, displayed during the recording mode.
 *     The activity communicates with these fragment to update them whenever necessary.
 * </p>
 * <p>
 *     The first important object in this activity is the BroadcastReceiver object, used to keep
 *     continuity in the communication to the readers.
 *     Then, the objects representing the readers and their managers are instantiated by the activity
 *     in its onResume method.
 *     Finally, the activity has several methods to communicate with the readers : connection and
 *     authentication.
 * </p>
 * @see BroadcastReceiver
 * @see NFCBluetoothManager
 * @see NFCReader
 * @see NFCBluetoothGattCallback
 * @see RFIDClimbingFragment
 * @see RFIDRecordingFragment
 */


public class ReadersActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, RFIDClimbingFragment.onButtonClicked, RFIDRecordingFragment.onButtonClicked, ReaderToActivity {

    public static final String TAG = ReadersActivity.class.getSimpleName();
    public static final String EXTRAS_DEVICE1_NAME = "DEVICE1_NAME";
    public static final String EXTRAS_DEVICE1_ADDRESS = "DEVICE1_ADDRESS";
    public static final String EXTRAS_DEVICE2_NAME = "DEVICE2_NAME";
    public static final String EXTRAS_DEVICE2_ADDRESS = "DEVICE2_ADDRESS";

    /**
     * boolean to indicate if the mode is climbing or recording, true for climbing, false for
     * recording.
     */
    private boolean mClimbingActivity;

    /**
     * boolean to indicate if the climbing difficulty is beginner or advanced, true if beginner,
     * false for advanced.
     */
    private boolean mBeginnerMode;

    /**
     * boolean to indicate if the activity is restarting, i.e. if it is the first time the activity
     * is launched or if it's getting out of onPause state.
     */
    private boolean mOnRestart;


    /* Parameters for audio playback */
    private TextToSpeech mSpeaker;
    /**
     * Default duration of the silence of the TTS object
     */
    private final int SHORT_DURATION = 1000;


    /**
     * boolean used in recording mode, indicates when the user wants to restart the recording from
     * a certain tag.
     */
    private boolean mOnRestartRecording = false;

    /**
     * LinkedHashMap<UUID,DataSample> to store the data of the read CSV file. In climbing mode, this
     * data is read to the user.
     * In recording mode, the data is copied to a DataSampleHashMap mDataSampleListToWrite,
     * with new keys which are the UUIDs of the tags scanned.
     */
    private DataSampleHashMap mDataSampleListToRead = new DataSampleHashMap();

    /**
     * LinkedHashMap<UUID,DataSample> to store the DataSample copied from mDataSampleListToRead.
     * Keys of this new LinkedHashMap are the new UUIDs scanned from the wall.
     *
     */
    private DataSampleHashMap mDataSampleListToWrite = new DataSampleHashMap();


    /**
     * int to store how many next holds above the user the app has to indicate in advanced mode.
     */
    private int mHoldNumberExpertMode = DataKeeper.getInstance().getHoldNumberAdvancedMode();


    /* Result names and addresses of the scan. */
    private String mDeviceName1;
    private String mDeviceName2;
    private String mDeviceAddress1;
    private String mDeviceAddress2;

    /* Writer to register new tag IDs*/
    private CSVWriter mCSVWriter;
    private OutputStreamWriter mOutputStreamWriter;
    private ParcelFileDescriptor mParcelFileDescriptor;
    private FileOutputStream mFileOutputStream;

    private Uri mUriRecordingFile;

    /**
     * Default authentication key for NFC Readers. The key is the same for all readers in the same
     * series.
     */
    private static final String DEFAULT_1255_MASTER_KEY = "ACR1255U-J1 Auth";
    private String mAuthenticationKey = NFCUtils.toHexString(DEFAULT_1255_MASTER_KEY.getBytes(StandardCharsets.UTF_8));

    /* Detected readers */

    private RFIDBluetoothReader mRFIDBluetoothReader1;
    private RFIDBluetoothReader mRFIDBluetoothReader2;

    /* Managers and callback to perform when each reader is detected */

    private RFIDBluetoothManager mRFIDBluetoothManager1;
    private RFIDBluetoothManager mRFIDBluetoothManager2;

    private RFIDBluetoothGattCallback mRFIDBluetoothGattCallback1;
    private RFIDBluetoothGattCallback mRFIDBluetoothGattCallback2;

    /* Bluetooth GATT client */
    private BluetoothGatt mBluetoothGatt1;
    private BluetoothGatt mBluetoothGatt2;

    /**
     * Thread used to connect the readers.
     */
    private Thread mConnectionThread;

    private Handler mHandler = new Handler();

    //To remove ?
    private ArrayList<String> mEpcList1;
    private ArrayList<String> mEpcList2;

    /* UI control */
    private RFIDClimbingFragment mRFIDClimbingFragment;
    private RFIDRecordingFragment mRFIDRecordingFragment;

    /**
     * BroadcastReceiver instance to keep continuity in the BLE communication with the RFID
     * readers.
     * When the activity method onResume is called (i.e. when the activity is started or
     * restarted), and intent is sent to the broadcast receiver,indicating a change in the bond
     * state of a remote device (here, the readers).
     * The receiver then has the duty to reinitialize the Bluetooth components of the phone : the
     * BluetoothManager, the BluetoothAdapter, and to get back the 2 BluetoothDevice instances
     * representing the remote readers (and activate them in the NFC case).
     * @see ReadersActivity#onResume()
     */
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothAdapter mBluetoothAdapter;
            BluetoothManager mBluetoothManager;
            final String action = intent.getAction();

            if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                Log.i(TAG, "ACTION_BOND_STATE_CHANGED");

                /* Get bond (pairing) state */
                if (mRFIDBluetoothManager1.isBluetoothManagerNull()) {
                    Log.w(TAG, "Unable to initialize BluetoothReaderManager of reader 1");
                    return;
                }
                if (mRFIDBluetoothManager2.isBluetoothManagerNull()) {
                    Log.w(TAG, "Unable to initialize BluetoothReaderManager of reader 2");
                    return;
                }

                mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                if (mBluetoothManager == null) {
                    Log.w(TAG, "Unable to initialize BluetoothManager");
                    return;
                }

                mBluetoothAdapter = mBluetoothManager.getAdapter();
                if (mBluetoothAdapter == null) {
                    Log.w(TAG, "Unable to initialize BluetoothAdapter");
                    return;
                }

                final BluetoothDevice mDevice1 = mBluetoothAdapter.getRemoteDevice(mDeviceAddress1);
                final int mBondState1 = mDevice1.getBondState();
                final BluetoothDevice mDevice2;
                final int mBondState2;

                if(mClimbingActivity) {
                    mDevice2 = mBluetoothAdapter.getRemoteDevice(mDeviceAddress2);
                    mBondState2 = mDevice2.getBondState();
                }
                else{
                    mBondState2 = 0;
                }

                Log.i(TAG, "BroadcastReceiver - getBondState of first device. state = " + mBondState1);
                Log.i(TAG, "BroadcastReceiver - getBondState of second device. state = " + mBondState2);

                /* Enable notification for each reader */
                if (mBondState1 == BluetoothDevice.BOND_BONDED) {
                    if (!mRFIDBluetoothReader1.isReaderNull()) {
                        mRFIDBluetoothReader1.enableNotification(true);
                    }
                }
                if (mBondState2 == BluetoothDevice.BOND_BONDED) {
                    if (mRFIDBluetoothReader2.isReaderNull()) {
                        mRFIDBluetoothReader2.enableNotification(true);
                    }
                }
                /*Update bond status and show in the readersConnection status field.*/
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateConnectionState(mBondState1,1);
                        updateConnectionState(mBondState2,2);
                    }
                });
            }
        }
    };

    /**
     * Enable NFC reader's notification. Calls the wrapper NFCReader to get
     * an object from the NFC API readers BluetoothReader. Then calls the enableNotification
     * method on the BluetoothReader object.
     * @param deviceNumber
     *                  an int indicating the reader, 1 for the first or 2 for the second reader.
     * @see NFCReader
     */
    /* Start the process to enable the reader's notifications. */
    public void activateReader(int deviceNumber) {
        /* If this is about the first reader */
        if (deviceNumber == 1) {
            if(mRFIDBluetoothReader1.isReaderNull())
                return;
            else
                /* Enable notification. */
                mRFIDBluetoothReader1.enableNotification(true);
            /* If this is about the second reader */
        }else if(deviceNumber == 2) {
            if(mRFIDBluetoothReader1.isReaderNull())
                return;
            else
                mRFIDBluetoothReader2.enableNotification(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_rfid_readers);

        mRFIDClimbingFragment = new RFIDClimbingFragment();
        mRFIDRecordingFragment = new RFIDRecordingFragment();

        mOnRestart = false;

        getSupportFragmentManager().beginTransaction().add(R.id.container_nfc_activity, mRFIDClimbingFragment,"NFCFragment").commit();

        /* Get names and addresses of the two devices scanned */
        final Intent intent = getIntent();
        mDeviceName1 = intent.getStringExtra(EXTRAS_DEVICE1_NAME);
        mDeviceName2 = intent.getStringExtra(EXTRAS_DEVICE2_NAME);
        mDeviceAddress1 = intent.getStringExtra(EXTRAS_DEVICE1_ADDRESS);
        mDeviceAddress2 = intent.getStringExtra(EXTRAS_DEVICE2_ADDRESS);

        mClimbingActivity = (mDeviceName2 != null && mDeviceAddress2 != null);

        Log.d("DEVICE 1", " RECEIVED NAME : " + mDeviceName1);
        Log.d("DEVICE 2", " RECEIVED NAME : " + mDeviceName2);
        Log.d("DEVICE 1", " RECEIVED ADDRESS : " + mDeviceAddress1);
        Log.d("DEVICE 2", " RECEIVED ADDRESS : " + mDeviceAddress2);

        /* Lecture of the CSV file */
        Uri uri = DataKeeper.getInstance().getUriLectureFile();
        Log.d(TAG, "URI = " + uri);

        InputStream inputStream;
        if(uri.toString().equals("Default"))
            inputStream = getApplicationContext().getResources().openRawResource(R.raw.table);
        else
            inputStream = GeneralUtils.getStreamFromUri(uri,getApplicationContext());
        mDataSampleListToRead = GeneralUtils.readTableData(getApplicationContext(),inputStream);

        mBeginnerMode = DataKeeper.getInstance().isBeginnerMode();

        /* Counting the number of hold in the path for the beginner - guided mode */
        if(mBeginnerMode){
            int holdCountBeginnerMode = mDataSampleListToRead.getSize();
            Log.d(TAG, "NUMBER OF HOLD, BEGINNER MODE = " + holdCountBeginnerMode);
        }

        try {
            inputStream.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        /* Set the CSV browser to 0 to start with the first element in NFCReader in
         * recording mode */
        if(!mClimbingActivity)
            DataKeeper.getInstance().setCSVBrowser(0);

        /* Speaker for audio playback */
        mSpeaker = new TextToSpeech(this, this);

        mEpcList1 = new ArrayList<>();
        mEpcList2 = new ArrayList<>();
    }

    /**
     * Overwritten method called when the activity enters in onResume state, i.e. juste after it's
     * created, or after getting out of onPause state.
     * The BroadcastReceiver object retrieves the actual bond state of the app to the Bluetooth
     * devices, and update the UI.
     * Then, new instances of RFIDBluetoothReader, manager and callbacks are created, a Gatt
     * connection is established with the reader and they authentication is called.
     * If the mOnRestart boolean is true, i.e. if it's not the first time this state is called (the
     * activity is getting out of pause state for example) an audio message is played.
     */
    @Override
    protected void onResume(){
        Log.i(TAG, "onResume()");
        super.onResume();
        final IntentFilter intentFilter = new IntentFilter();
        /* Start to monitor bond state change */
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver, intentFilter);

        /* Instantiation of the NFCReader objects to represent the NFC Readers */
        mRFIDBluetoothReader1 = new RFIDBluetoothReader(this, 1, mDeviceName1);

        /* Instantiation of BluetoothReaderManager for the readers. Needs to be done on onResume for readers disconnection  */
        mRFIDBluetoothManager1 = new RFIDBluetoothManager(this, 1, mRFIDBluetoothReader1, mDeviceName1);
        //Log.d("NFC READERS","CREATION OF BLE MANAGER 1");

        /* Initialization of callbacks for the readers*/
        mRFIDBluetoothGattCallback1 = new RFIDBluetoothGattCallback(this, 1, mRFIDBluetoothReader1,mRFIDBluetoothManager1, mDeviceName1);



        /* Need of 2 readers if climbing, only one if recording */
        if(mClimbingActivity) {
            mRFIDBluetoothReader2 = new RFIDBluetoothReader(this, 2, mDeviceName2);

            mRFIDBluetoothManager2 = new RFIDBluetoothManager(this, 2, mRFIDBluetoothReader2, mDeviceName2);
            mRFIDBluetoothGattCallback2 = new RFIDBluetoothGattCallback(this, 2, mRFIDBluetoothReader2, mRFIDBluetoothManager2, mDeviceName2);
        }

        /* Connect the readers after 2 seconds */
        final Handler connectionHandler = new Handler();
        connectionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                prepareConnection();
            }
        },2000);

        /* Authenticate the readers after 4 seconds */
        final Handler mHandler1 = new Handler();
        mHandler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                authenticateReader(1);
            }
        },4000);
        if(mClimbingActivity){
            final Handler mHandler2 = new Handler();
            mHandler2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    authenticateReader(2);
                }
            },4000);
        }

        /* If the activity is getting out of onPause, then reconnect readers and tell the user to press the start polling button */
        if(mOnRestart) {
            mSpeaker = new TextToSpeech(this, this);
            Handler mOnResumeHandler = new Handler(Looper.getMainLooper());
            mOnResumeHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.toast_restart_polling,
                            getApplicationContext().getString(R.string.nfc_reader_start_polling),
                            getApplicationContext().getString(R.string.nfc_reader_restart_polling)), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
                    toast.show();
                }
            }, 5000);
        }
    }

    @Override
    protected void onPause(){
        Log.i(TAG, "onPause()");
        super.onPause();

        mOnRestart = true;

        /* Stop to monitor bond state change */
        unregisterReceiver(mBroadcastReceiver);

        /* Disconnect Bluetooth reader */
        if(!mRFIDBluetoothReader1.isReaderNull())
            disconnectReader(1);
        if(mClimbingActivity && !mRFIDBluetoothReader2.isReaderNull())
            disconnectReader(2);
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop()");
        /*  Don't forget to release resources allowed for the text to speech engine */
        if(mSpeaker != null){
            mSpeaker.shutdown();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }

    /**
     * Prepare the Bluetooth components for Bluetooth connection. This include :
     * <ul>
     *     <li> clearing the old Gatt connection by clearing the BluetoothGatt instances, </li>
     *     <li> initializing the BluetoothManager and the BluetoothAdapter</li>
     *     <li> updating connection states</li>
     *     <li> creating the BluetoothDevice objects representing the readers</li>
     * </ul>
     * Once these steps are done, the method creates a Thread to handle the connection of the
     * readers by calling the connectionReaders method.
     * @see BluetoothManager
     * @see BluetoothAdapter
     */
    private boolean prepareConnection(){

        mRFIDClimbingFragment.setDeviceUI(mDeviceName1, mDeviceName2, mDeviceAddress1, mDeviceAddress2);

        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if(bluetoothManager == null){
            Log.w(TAG, "Impossible d'initializer un  BluetoothManager");
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 1);
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 2);
            return false;
        }
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null) {
            Log.w(TAG, "Impossible d'obtenir un BluetoothAdapter");
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 1);
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 2);
            return false;
        }
        /* Clear old GATT connections. */
        if (mBluetoothGatt1 != null) {
            Log.i(TAG, "Suppression of old Gatt connection - reader 1");
            mBluetoothGatt1.disconnect();
            mBluetoothGatt1.close();
            mBluetoothGatt1 = null;
        }
        if (mBluetoothGatt2 != null) {
            Log.i(TAG, "Suppression of old Gatt connection - reader 2");
            mBluetoothGatt2.disconnect();
            mBluetoothGatt2.close();
            mBluetoothGatt2 = null;
        }
        /* Get the two BluetoothDevice objects with their mac Address */
        final BluetoothDevice device1 = bluetoothAdapter.getRemoteDevice(mDeviceAddress1);
        final BluetoothDevice device2;
        if(mClimbingActivity) device2 = bluetoothAdapter.getRemoteDevice(mDeviceAddress2);
        else
            device2 = null;

        /* Check that both devices are available. For now, if one device is not available, the readersConnection won't be made for both devices. */

        if (device1 == null) {
            Log.w(TAG, "Périphérique 1 non trouvé. Impossible de se connecter.");
            return false;
        }
        updateConnectionState(BluetoothReader.STATE_CONNECTING, 1);

        if (device2 == null){
            Log.w(TAG, "Périphérique 2 non trouvé. Impossible de se connecter.");
        }
        else
            updateConnectionState(BluetoothReader.STATE_CONNECTING, 2);
        if(mConnectionThread == null){
            mConnectionThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    readersConnection(device1, device2);
                    mConnectionThread.interrupt();
                    mConnectionThread = null;
                }
            });

            mConnectionThread.start();

        }
        return true;
    }

    /**
     * Connects the phone to both readers (only to one if recording mode).
     * Uses a thread by connection, in order not to perform both connections simultaneously.
     * Each thread calls the connectGatt method of one BluetoothDevice. The result of this method is
     * a BluetoothGatt instance representing the Gatt profile of a BluetoothDevice.
     * The connectGatt method requires a BluetoothCallback object passed as a parameter. The
     * callbacks passed here are of the type BluetoothReaderCallback, which a type from the NFC
     * readers API.
     * @param device1
     *              the BluetoothDevice object representing the first reader
     * @param device2
     *              the BluetoothDevice object representing the second reader
     * @see BluetoothGatt
     * @see BluetoothDevice
     * @see BluetoothReader
     */
    /* Function of connection for NFC readers, because the callback is of type BluetoothReaderGattCallback */
    private void readersConnection(BluetoothDevice device1, BluetoothDevice device2){
        //mBluetoothGatt1 = device1.connectGatt(this, false, mGattCallback1.getBluetoothReaderGattCallback());
        mBluetoothGatt1 = device1.connectGatt(this, false, mRFIDBluetoothGattCallback1.getBluetoothGattCallback());
        mRFIDBluetoothReader1.setGattAndCallback(mBluetoothGatt1, mRFIDBluetoothGattCallback1.getBluetoothGattCallback());
        try{
            Thread.sleep(250);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        if(mClimbingActivity) {
            //mBluetoothGatt2 = device2.connectGatt(this, false, mGattCallback2.getBluetoothReaderGattCallback());
            mBluetoothGatt2 = device2.connectGatt(this, false, mRFIDBluetoothGattCallback2.getBluetoothGattCallback());
            mRFIDBluetoothReader2.setGattAndCallback(mBluetoothGatt2, mRFIDBluetoothGattCallback2.getBluetoothGattCallback());


            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "GATT 1 = " + mBluetoothGatt1);
        Log.d(TAG, "GATT 2 = " + mBluetoothGatt2);
    }

    /**
     * Disconnect an established connection to readers. Updates the UI by calling
     * updateConnectionState.
     * Then, call the disconnect method of the BluetoothGatt object, representing the Gatt profile
     * of a BluetoothDevice.
     * @param deviceNumber
     *                  an int indicating the reader to disconnect (1 for the first, 2 for the
     *                  second)
     * @see RFIDClimbingFragment#updateConnectionState(String, int)
     * @see BluetoothGatt
     */
    /* Disconnects an established readersConnection.  */
    public void disconnectReader(int deviceNumber) {
        if (deviceNumber ==1) {
            if (mBluetoothGatt1 == null) {
                updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 1);
                return;
            }
            updateConnectionState(BluetoothReader.STATE_DISCONNECTING, 1);
            mBluetoothGatt1.disconnect();
            mRFIDBluetoothReader1.stopPolling();
        }
        else if(deviceNumber == 2){
            if(mClimbingActivity) {
                if (mBluetoothGatt2 == null) {
                    updateConnectionState(BluetoothReader.STATE_DISCONNECTED, 2);
                }
                updateConnectionState(BluetoothReader.STATE_DISCONNECTING, 2);
                mBluetoothGatt2.disconnect();
                mRFIDBluetoothReader2.stopPolling();
            }
        }
    }

    /**
     * Authenticates an NFC reader with a authentication key. Checks that this key is not null or
     * empty, and then call the authenticateReader method on the BluetoothReader object (got from the
     * wrapper NFCReader). This authenticate method is from the NFC Readers API.
     * Maybe smart to displace this method into the wrapper NFCReader.
     * @param deviceNumber
     *                  an int indicating the reader to authenticate (1 for the first, 2 for the
     *                  second)
     * @see NFCUtils#getTextInHexBytes(String)
     * @see BluetoothReader#authenticate(byte[])
     */
    private void authenticateReader(int deviceNumber){
        byte[] authenticationKey = NFCUtils.getTextInHexBytes(mAuthenticationKey);
        //Log.d(TAG,"AUTHENTICATE CALLED, MASTErKEY = " + mAuthenticationKey);
        if(authenticationKey != null && authenticationKey.length > 0){
            if(deviceNumber == 1){
                if(mRFIDBluetoothReader1.isReaderNull()) {
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
                    return;
                }
                if(mRFIDBluetoothReader1.authenticateReader(authenticationKey) && mRFIDClimbingFragment.getConnectionState(1))
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_ready_to_use_nfc_reader,1));
                else
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
            }
            else if(deviceNumber == 2){
                if(mRFIDBluetoothReader2.isReaderNull()){
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,2));
                    return;
                }
                if(mRFIDBluetoothReader2.authenticateReader(authenticationKey) && mRFIDClimbingFragment.getConnectionState(2))
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_ready_to_use_nfc_reader,2));
                else
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,2));
            }
        }else{
            GeneralUtils.toast(getApplicationContext(), getString(R.string.error_keyFormat_nfc_reader));
        }
    }


    /**
     * Update the connection states displayed on RFIDClimbingFragment, by calling its method
     * updateConnectionState.
     * @param connectState
     *                  the new connection state to be displayed
     * @param deviceNumber
     *                  an int indicating the reader whose state has to be updated (1 for the
     *                  first, 2 for the second)
     * @see RFIDClimbingFragment#updateConnectionState(String, int)
     * @see BluetoothReader
     */
    public void updateConnectionState(final int connectState, final int deviceNumber) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (connectState == BluetoothReader.STATE_CONNECTING)
                    mRFIDClimbingFragment.updateConnectionState(getString(R.string.connection_nfc_reader),deviceNumber);
                else if (connectState == BluetoothReader.STATE_CONNECTED)
                    mRFIDClimbingFragment.updateConnectionState(getString(R.string.connected_nfc_reader),deviceNumber);

                else if (connectState == BluetoothReader.STATE_DISCONNECTING)
                    mRFIDClimbingFragment.updateConnectionState(getString(R.string.disconnection_nfc_reader),deviceNumber);
                else
                    mRFIDClimbingFragment.updateConnectionState(getString(R.string.disconnected_nfc_reader),deviceNumber);
            }
        });

    }


    /* Init the text to speech engine */
    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            Log.d("TEXT TO SPEECH", "SUCCESS");
        }
        else
            Log.d("TEXT TO SPEECH", "FAILURE");
    }

    /**
     * Method called when the Start Polling Button is pressed in RFIDClimbingFragment. Checks first that
     * the readers are available (i.e. that the NFCReader objects aren't null) and then
     * transmit them an escape command to start polling, via the transmitEscapeCommand method. This
     * method is also from the NFC readers API, and it needs to be called on a BluetoothReader
     * object (i.e. the representation of the reader in the NFC API, wrapped in this app into a
     * NFCReader object).
     * Then, the method reads a audio message to indicate the user he can start to climb, or to
     * record (following the selected mode).
     * @see BluetoothReader#transmitEscapeCommand(byte[])
     */
    @Override
    public void startPollingClimbingFragment() {
        if (mRFIDBluetoothReader1.isReaderNull()) {
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
            return;
        }
        if(mClimbingActivity && mRFIDBluetoothReader2.isReaderNull()) {
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,2));
            return;
        }
        if (!mRFIDBluetoothReader1.startPolling())
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
        if(mClimbingActivity){
            if (!mRFIDBluetoothReader2.startPolling())
                GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,2));
        }
        /* If it's the first time that the button is pressed*/
        if(!mOnRestart){
            if(mClimbingActivity){
                mSpeaker.speak(getApplicationContext().getString(R.string.tts_start_climbing), TextToSpeech.QUEUE_ADD, null, null);
                mSpeaker.playSilentUtterance(1000,TextToSpeech.QUEUE_ADD, null);
                mSpeaker.speak(getApplicationContext().getString(R.string.tts_first_instruction), TextToSpeech.QUEUE_ADD, null, null);
            }
            else{
                mSpeaker.speak(getString(R.string.tts_start_recording), TextToSpeech.QUEUE_ADD, null, null);
                getSupportFragmentManager().beginTransaction().replace(R.id.container_nfc_activity, mRFIDRecordingFragment,"TAGListFragment").commit();
            }
            mOnRestart = true;
        }
        else{
            mSpeaker.speak("Vous pouvez recommencer l'escalade", TextToSpeech.QUEUE_ADD, null, null);
        }
    }

    /**
     * Reconnects the readers by calling first the prepareConnection method, and then the
     * authenticateReaders method after 4 seconds. The call of authenticate is delayed thanks to
     * a Handler object.
     */
    @Override
    public void reconnectReaders() {
        mRFIDBluetoothReader1.stopPolling();
        mRFIDBluetoothReader2.stopPolling();
        prepareConnection();
        final Handler authenticationHandler = new Handler();
        authenticationHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                authenticateReader(1);
                if(mClimbingActivity)
                authenticateReader(2);
            }
        },4000);

    }

    /**
     * Method called when the Start Polling Button is pressed in RFIDRecordingFragment. Checks first that
     * the first reader is available (i.e. that the NFCReader objects isn't null, in this
     * mode we only need one reader) and then transmit an escape command to it to start polling via
     * the transmitEscapeCommand method. This method is also from the NFC readers API, and it needs
     * to be called on a BluetoothReader object (i.e. the representation of the reader in the NFC
     * API, wrapped in this app into a NFCReader object).
     * Then, the method reads a audio message to indicate the user he can start to climb, or to
     * record (following the selected mode).
     * @see BluetoothReader#transmitEscapeCommand(byte[])
     */
    @Override
    public void startPollingTagListFragment() {
        if (mRFIDBluetoothReader1.isReaderNull()) {
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
            return;
        }
        if (!mRFIDBluetoothReader1.startPolling())
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_not_ready_nfc_reader,1));
        if(mOnRestart)
            mSpeaker.speak(getString(R.string.tts_continue_recording), TextToSpeech.QUEUE_ADD, null, null);
        else {
            mSpeaker.speak(getString(R.string.tts_start_recording), TextToSpeech.QUEUE_ADD, null, null);
            mOnRestart = true;
        }
    }

    /**
     * Method called when the Ok button is pressed on the RFIDRecordingFragment. This validation calls the
     * writeOnCSV method to fill the new CSV file, and then takes the user to SeparationActivity to
     * either record a new route, or to start climbing (but not with the file juste recorded which
     * needs to be treated).
     * @see ReadersActivity#writeOnCSV()
     */
    @Override
    public void validateTagListFragment() {
        writeOnCSV();
        Intent okIntent = new Intent(ReadersActivity.this, SeparationActivity.class);
        startActivity(okIntent);
    }

    /**
     * Method called when the "Restart recording from tag" button is pressed in RFIDRecordingFragment.
     * Sets the value of mOnRestartRecording in NFCReader object to true. Indicates to the
     * NFCReader that the user wants to restart the recording.
     */
    @Override
    public void restartTagListFragment() {
        mOnRestartRecording = true;
    }

    /**
     * Updates the listView in RFIDRecordingFragment. Then updates the fragment with FragmentManager.
     * Also update the CSV browser index in DataKeeper. This index will be used by the
     * NFCReader to know from which position restart from.
     * @param numberToRestart
     *                      the number of the tag to restart from. All the tags scanned with a
     *                      higher position in the ListView are erased.
     * @see NFCReader
     * @see androidx.fragment.app.FragmentManager
     * @see DataKeeper#setCSVBrowser(int)
     *
     */
    public void restartRecording(final int numberToRestart){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mRFIDRecordingFragment.clearList(numberToRestart);
                getSupportFragmentManager().beginTransaction().replace(R.id.container_nfc_activity, mRFIDRecordingFragment,"TAGListFragment").commit();
            }
        });
        DataKeeper.getInstance().setCSVBrowser(numberToRestart);
    }

    /**
     * Updates the listView in RFIDRecordingFragment by calling the addListItem method from the fragment.
     * Called normally by the NFCReader object when is scans a tag in recording mode.
     * Among the parameters passed, we can fin the result of the tag scanning (is UUID), and several
     * parameters of the hold corresponding to the tag.The correspondence between the tag and the
     * hold is done thanks to the CSVBrowser index in the NFCReader object,
     * who associates the number of the tag scanned with the item position in the DataSampleHashMap
     * object.
     * @param apduResponse
     *                  the String UUID of the tag just scanned
     * @param xAbsValue
     *                the coordinates on the x-axis of the hold corresponding to the tag scan
     * @param yAbsValue
     *              the coordinates on the y-axis of the hold corresponding to the tag scan
     * @param holdSize
     *              the size of the hold corresponding to the tag scan
     * @param holdShape
     *              the shape of the hold corresponding to the tag scan
     * @see NFCReader
     * @see RFIDRecordingFragment#addListItem(String, String, String, String, String)
     *
     */
    public void updateTagListFragment(String apduResponse, String xAbsValue, String yAbsValue, String holdSize, String holdShape){
        mRFIDRecordingFragment.addListItem(apduResponse,xAbsValue, yAbsValue, holdSize, holdShape);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_nfc_activity, mRFIDRecordingFragment,"TAGListFragment").commit();
    }


    /**
     * Writes the content of the activity attribute mDataSampleListToWrite on a CSV file stored in
     * the phone filesystem.The writing is done with a CSVWriter object from the openCSV Java
     * library.
     * Each DataSample of mDataSampleListToWrite is decomposed to get the attributes to write on a
     * line.
     * Each line is written in the order by calling the writeNextLine method of the writer.
     * The method is called a first time to write the headers of the CSV file. Then, it is called
     * again to write the first line which has 3 more elements than the following lines.
     * Then all the other elements are written in the CSV file line after line.
     * This method is called at the end of the recording when validated by the user by pressing the
     * Ok button in the RFIDRecordingFragment.
     * @see ReadersActivity#validateTagListFragment()
     * @see com.example.judith.bluetoothtest.Classes.DataSample
     * @see ParcelFileDescriptor
     * @see OutputStreamWriter
     * @see CSVWriter
     */
    public void writeOnCSV(){
        mRFIDBluetoothReader1.stopPolling();
        mUriRecordingFile = DataKeeper.getInstance().getUriRecordingFile();
        try {
            mParcelFileDescriptor = getContentResolver().openFileDescriptor(mUriRecordingFile,"w");
        } catch (IOException e){
            e.printStackTrace();
            Log.i(TAG, "fail to re-open stream on recording file");
        }

        mFileOutputStream = new FileOutputStream(mParcelFileDescriptor.getFileDescriptor());
        mOutputStreamWriter = new OutputStreamWriter(mFileOutputStream);
        mCSVWriter = new CSVWriter(mOutputStreamWriter,',', mCSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER,CSVWriter.DEFAULT_LINE_END);

        String[] headers = {getString(R.string.table_title_tagNumber),
                getString(R.string.table_title2_part_of_path),
                getString(R.string.table_title_tagID),
                getString(R.string.table_title_xAxis),
                getString(R.string.table_title_yAxis),
                getString(R.string.table_title_handPrecision),
                getString(R.string.table_title_holdShape),
                getString(R.string.table_title_holdSize),
                getString(R.string.table_title_climberSize),
                getString(R.string.table_title_hold_number)};

        mCSVWriter.writeNext(headers);

        String[] firstLine = {String.valueOf(1),
                String.valueOf(1),
                mDataSampleListToWrite.getKeyByIndex(0),
                String.valueOf(mDataSampleListToWrite.getDataSampleByIndex(0).getxAbs()),
                String.valueOf(mDataSampleListToWrite.getDataSampleByIndex(0).getyAbs()),
                mDataSampleListToWrite.getDataSampleByIndex(0).getHandPrecision(),
                mDataSampleListToWrite.getDataSampleByIndex(0).getHoldShape(),
                mDataSampleListToWrite.getDataSampleByIndex(0).getHoldSize(),
                String.valueOf(DataKeeper.getInstance().getClimberSize()),
                String.valueOf(DataKeeper.getInstance().getHoldNumberAdvancedMode())};
        Log.d(TAG, "FIRST LINE TO WRITE = " + firstLine[0] + firstLine[1] + firstLine[2] +
                firstLine[3] + firstLine[4] + firstLine[5] + firstLine[6] + firstLine[7]);

        mCSVWriter.writeNext(firstLine);
        for (int i = 1; i< mDataSampleListToWrite.getSize(); i++){
            String[] stringLine = {String.valueOf(i+1),
                    mDataSampleListToWrite.getDataSampleByIndex(i).isTagInPath() ? String.valueOf(1) : String.valueOf(0),
                    mDataSampleListToWrite.getKeyByIndex(i),
                    String.valueOf(mDataSampleListToWrite.getDataSampleByIndex(i).getxAbs()),
                    String.valueOf(mDataSampleListToWrite.getDataSampleByIndex(i).getyAbs()),
                    mDataSampleListToWrite.getDataSampleByIndex(i).getHandPrecision(),
                    mDataSampleListToWrite.getDataSampleByIndex(i).getHoldShape(),
                    mDataSampleListToWrite.getDataSampleByIndex(i).getHoldSize()};
            Log.d(TAG, "LINE TO WRITE = " + stringLine[0] + stringLine[1] + stringLine[2] +
                    stringLine[3] + stringLine[4] + stringLine[5] + stringLine[6] + stringLine[7]);
            mCSVWriter.writeNext(stringLine);
        }
        try {
            mOutputStreamWriter.close();
            Log.d(TAG, "CLOSING WRITER");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * <p>
     *     Implements 3 different behaviours, depending on the option chosen by
     *     the user. The options are, in the following order in the methods:
     *     <ul>
     *         <li> climbing mode, beginner, </li>
     *         <li> climbing mode, advanced, </li>
     *         <li> recording mode</li>
     *     </ul>
     *     The conditions checked to choose either one option or another are the boolean
     *     mClimbingActivity (true if one of the 2 climbing mode, false if recording mode),
     *     and mBeginnerMode (true if beginner mode, false if advanced mode).
     * </p>
     * <p>
     *     Then, this step is followed by the research of the ID passed as an argument in the
     *     DataSampleHashMap mDataSampleListToRead to check if the tag is in the path (in
     *     climbing mode),or in mDataSampleListToWrite to see if its has already been
     *     scanned (in recording mode).
     * </p>
     * <p>
     *     In climbing mode, following the option chosen, the speaker will either read
     *     the next element of mDataSampleListToRead (beginner), or the
     *     mHoldNumberExpertMode number of closest elements in mDataSampleListToRead,
     *     ordered by their x-axis distance to the found element (advanced).
     * </p>
     * <p>
     *     In the recording mode, the Xth element (following the counter mCSVBrowser) will
     *     be copied in mDataSampleListToRead to mDataSampleListToWrite, adding the UUID
     *     as the key (a key cannot be modified in a HashMap object, so the solution is
     *     to create another HashMap).
     *     Then, when all the tags have been scanned, the content of mDataSampleListToWrite
     *     is written in a CSV filed with a call to the writeOnCSV method in the activity.
     * </p>
     * @param tagID
     *          a String containing the tag ID
     * @param deviceNumber
     *          the number to identify the reader, 1 or 2
     * @see ReadersActivity#writeOnCSV()
     */

    @Override
    public void onTagRead(final String tagID, final int deviceNumber){
        if (mClimbingActivity && mBeginnerMode) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mRFIDClimbingFragment.setTagID(tagID, deviceNumber);
                }
            });
            /* Case where the app is supposed to announce only the one ot two following instructions, which means the climber is guided */
            Log.d(TAG, "UUID SIZE = " + tagID.length());
            /* If the  uuid is not in the table */
            if ((!mDataSampleListToRead.foundUUID(tagID)) || !mDataSampleListToRead.getDataSampleWithUUID(tagID).isTagInPath())
                mSpeaker.speak(getString(R.string.tts_id_notFound_nfc_reader), TextToSpeech.QUEUE_ADD, null, null);
            else {
                DataSample actualHold = mDataSampleListToRead.getDataSampleWithUUID(tagID);
                /*the index wanted is the tagNumber - 1, so to have nextHold take the tagNumber of current hold */
                DataSample nextHold = mDataSampleListToRead.getDataSampleByIndex(actualHold.getHoldNumber());
                /* If we are at the end of the path */
                if (actualHold.getHoldNumber() >= mDataSampleListToRead.getSize()) {
                    mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                    mSpeaker.speak(getString(R.string.tts_end_message), TextToSpeech.QUEUE_ADD, null, null);
                } else {
                    Log.d("TABLE BROWSING", "UUID FOUND ! " + actualHold);
                    mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                    GeneralUtils.audioLectureUtils(getApplicationContext(), actualHold, nextHold, mSpeaker, mBeginnerMode,0);
                }
            }
        } else if (mClimbingActivity) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mRFIDClimbingFragment.setTagID(tagID, deviceNumber);
                }
            });
            /* Case where the app is supposed to announce the X holds above the climber */
            /* Loop over the tags ID to find the corresponding one */
            if (!mDataSampleListToRead.foundUUID(tagID))
                mSpeaker.speak(getString(R.string.tts_id_notFound_nfc_reader), TextToSpeech.QUEUE_ADD, null, null);
            else {
                DataSample actualHold = mDataSampleListToRead.getDataSampleWithUUID(tagID);
                Log.d("TABLE BROWSING", "UUID FOUND ! " + actualHold);
                Log.d(TAG, "NUMBER OF HOLD = " + mHoldNumberExpertMode);
                Log.d(TAG, "NUMBER OF HOLD = " + DataKeeper.getInstance().getHoldNumberAdvancedMode());
                mSpeaker.speak(getString(R.string.tts_X_holds_close, String.valueOf(mHoldNumberExpertMode), actualHold.getHandPrecision()), TextToSpeech.QUEUE_ADD, null, null);
                mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                /* Find the closest holds in a radius of 2 meters*/
                HashMap<Integer, DataSample> tempDataSampleList = new HashMap<Integer, DataSample>();
                for (int i = 0; i < mDataSampleListToRead.getHashMap().size(); i++) {
                    DataSample sample = mDataSampleListToRead.getDataSampleByIndex(i);
                    int hypotenuse = GeneralUtils.getHypotenuse((sample.getxAbs() - actualHold.getxAbs()), (mDataSampleListToRead.getDataSampleByIndex(i).getyAbs() - actualHold.getyAbs()));
                    Log.d(TAG, "HYPOTENUSE BETWEEN SAMPLE AND " + i+ "TH HOLD : " + hypotenuse);
                    /* 3 conditions to check : if the distance between the 2 hold is < 2m, if the hold is above our current hold, and is the hold is not our current hold*/
                    if ((sample.getyAbs() >= actualHold.getyAbs()) && hypotenuse < 200 && sample != actualHold)
                        tempDataSampleList.put(hypotenuse, mDataSampleListToRead.getDataSampleByIndex(i));
                }
                Log.d(TAG, "DISTANCE TO 6 NEXT HOLDS :" + tempDataSampleList.toString());
                /* Fill a table containing the 6 next holds sorted by distance */
                TreeMap<Integer, DataSample> sortedByDistance = new TreeMap<Integer, DataSample>(tempDataSampleList);
                sortedByDistance = GeneralUtils.getFirstEntries(sortedByDistance, mHoldNumberExpertMode);
                Log.d(TAG, "INDEX DISTANCE TO" + mHoldNumberExpertMode +" NEXT HANDLES AFTER SORTING:" + sortedByDistance.toString());
                /* Loop over the number of instructions desired */
                int j = 0;
                for (Iterator i = sortedByDistance.entrySet().iterator(); i.hasNext();) {
                    j++;
                    Map.Entry entrySample =(Map.Entry)i.next();
                    DataSample nextHold = (DataSample)entrySample.getValue();                                mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                    mSpeaker.speak(getString(R.string.tts_hold, String.valueOf(j)), TextToSpeech.QUEUE_ADD, null, null);
                    GeneralUtils.audioLectureUtils(getApplicationContext(), actualHold, nextHold, mSpeaker, mBeginnerMode,(int)entrySample.getKey());
                    mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                }
                /* If not enough holds could be read because we are at the end of the path*/
                if (sortedByDistance.size() < mHoldNumberExpertMode)
                    mSpeaker.speak(getString(R.string.tts_no_more_hold), TextToSpeech.QUEUE_ADD, null, null);
                Log.d(TAG, "READER " + deviceNumber + ", NO MORE HOLD");
            }
        } else {
            if((DataKeeper.getInstance().getCSVBrowser()>= mDataSampleListToRead.getSize()) && !mDataSampleListToWrite.foundUUID(tagID))
                mSpeaker.speak(getString(R.string.tts_no_more_hold_to_record), TextToSpeech.QUEUE_ADD, null, null);
            else{
                /* Scan of the table to check that the tag had not been scanned already */
                if(mDataSampleListToWrite.foundUUID(tagID)&& !mOnRestartRecording){
                    mSpeaker.speak(getString(R.string.tts_tag_already_scanned), TextToSpeech.QUEUE_ADD, null, null);

                }
                else{
                    Log.d(TAG, "TAG PAS ENCORE SCANNÉ ");
                    if(mOnRestartRecording){
                        List<String> keyIndex = new ArrayList<>(mDataSampleListToWrite.getHashMap().keySet());
                        int indexToRestart = keyIndex.indexOf(tagID);
                        Log.d(TAG, "INDEX TO RESTART" + indexToRestart);
                        restartRecording(indexToRestart);
                        mSpeaker.speak(getString(R.string.tts_restart_recording,(indexToRestart+1)), TextToSpeech.QUEUE_ADD, null, null);
                        int limit = mDataSampleListToWrite.getSize()-indexToRestart;
                        Log.d(TAG, "LIMIT = " + limit);
                        for (int i = 0; i<limit; i++){
                            Log.d(TAG, "TO REMOVE : " + mDataSampleListToWrite.getKeyByIndex(mDataSampleListToWrite.getSize()-1));
                            mDataSampleListToWrite.getHashMap().remove(mDataSampleListToWrite.getHashMap().keySet().toArray()[mDataSampleListToWrite.getSize()-1]);
                        }
                        mOnRestartRecording = false;
                    }
                    final DataSample sampleToWrite = mDataSampleListToRead.getDataSampleByIndex(DataKeeper.getInstance().getCSVBrowser());
                    mDataSampleListToWrite.putDataSample(tagID, sampleToWrite);
                    mSpeaker.speak(getString(R.string.tts_handle_record_succes,(DataKeeper.getInstance().getCSVBrowser()+1)), TextToSpeech.QUEUE_ADD, null, null);
                    //Next message to indicate the direction of the next hold to scan. Maybe not necessary, and maybe more difficult with the hashmap structure
                    //mSpeaker.speak("Prochaine prise à " + DataSampleList.get(mCSVBrowser).getDirectionHour() + " heures ", TextToSpeech.QUEUE_ADD, null, null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateTagListFragment(tagID, String.valueOf(sampleToWrite.getxAbs()),
                                    String.valueOf(sampleToWrite.getyAbs()),
                                    sampleToWrite.getHoldSize(), sampleToWrite.getHoldShape());
                        }
                    });
                    //Handler to delay the incrementation for the last line. Otherwise, mCSVBrowser updates to soon
                    Handler waitHandler = new Handler(Looper.getMainLooper());
                    waitHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DataKeeper.getInstance().incrCSVBrowser();
                            Log.d(TAG, "CSV BROWSER AFTER INCR = " + DataKeeper.getInstance().getCSVBrowser());
                            if (DataKeeper.getInstance().getCSVBrowser() >= (mDataSampleListToRead.getSize())) {
                                mSpeaker.speak(getString(R.string.tts_end_hold_record,getString(R.string.ok),getString(R.string.restart_recording)), TextToSpeech.QUEUE_ADD, null, null);
                            } else {
                                //mSpeaker.speak("Prochaine prise à " + DataSampleList.get(mCSVBrowser).getDirectionHour() + " heures ", TextToSpeech.QUEUE_ADD, null, null);  -- indication of the nex hold to scan
                                Log.d(TAG, "CSV BROWSER = " + DataKeeper.getInstance().getCSVBrowser());
                            }
                        }
                    }, 3000);
                }

            }
        }
    }

    /**
     * Overwritten method from the ReadersToActivity Interface implemented by this activity.
     * Receives a runnable and adds it to the message queue.
     * @param runnable
     *             The runnable to be added to the message queue
     * @see Handler#post(Runnable)
     */
    @Override
    public void postRunnableToHandler(Runnable runnable) {
        mHandler.post(runnable);
    }

    /**
     * Overwritten method from the ReadersToActivity Interface implemented by this activity.
     * Receives a runnable and adds it to the message queue after the specific time passed as a
     * parameter.
     * @param runnable
     *             The runnable to be added to the message queue
     * @param delayTime
     *              The delay time in milliseconds
     * @see Handler#postDelayed(Runnable, long)
     */
    @Override
    public void postDelayedRunnableToHandler(Runnable runnable, long delayTime) {
        mHandler.postDelayed(runnable,delayTime);
    }

    /**
     * Overwritten method from the ReadersToActivity Interface implemented by this activity.
     * Remove any pending posts of the runnable passed as an argument that are in the message queue.
     * @param runnable
     *             The runnable to be remove from the message queue
     * @see Handler#removeCallbacks(Runnable)
     */
    @Override
    public void removeRunnableFromHandler(Runnable runnable) {
        mHandler.removeCallbacks(runnable);

    }

    /**
     * Overwritten method from the ReadersToActivity Interface implemented by this activity.
     * Post a toast message on the main UI thread.
     * @param message
     *             The message to be displayed in a toast
     * @see GeneralUtils#toast(Context, String)
     */
    @Override
    public void postToastMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                GeneralUtils.toast(getApplicationContext(),message);
            }
        });
    }


}