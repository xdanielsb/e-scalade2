package com.example.judith.bluetoothtest.Classes.GeneralRFID;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCBluetoothGattCallback;
import com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFBluetoothGattCallback;

/**
 * RFIDBluetoothGattCallback is a wrapper class for every instance of a Gatt callback for a RFID
 * reader. This class is used so far to wrap callbacks for two types of RFID readers : NFC readers
 * from ACS, and UHF readers from SmartLink.
 */
public class RFIDBluetoothGattCallback extends BluetoothGattCallback {

    /**
     * Instance of a Gatt callback for a UHF SmartLink reader.
     */
    private UHFBluetoothGattCallback mUHFBluetoothGattCallback;

    /**
     * Instance of a Gatt callback for a ACS NFC reader.
     */
    private NFCBluetoothGattCallback mNFCBluetoothGattCallback;

    /**
     * Instance for the Bluetooth Manager corresponding to the Bluetooth reader and the callback.
     * Not in use so far, might be later.
     */
    private RFIDBluetoothManager mRFIDBluetoothManager;

    /**
     * Instance for the Bluetooth reader callback.
     * Not in use so far, might be later.
     */
    private RFIDBluetoothReader mRFIDBluetoothReader;

    /**
     * int to identify the reader, 1 or 2. Not in use so far but might be.
     */
    private int mDeviceNumber;

    /**
     * An int indicating which type of reader is used : 1 for UHF and 2 for NFC. This type is set
     * in the reader constructor thanks to the deviceName String which contains the formal name
     * of the BluetoothDevice.
     * This int allows then the class to know which type of Bluetooth manager to construct when
     * called in ReadersActivity.
     */
    private int mReaderType;

    public RFIDBluetoothGattCallback(Activity activity, int deviceNumber, RFIDBluetoothReader rfidBluetoothReader, RFIDBluetoothManager rfidBluetoothManager, String mDeviceName) {
        mDeviceNumber = deviceNumber;
        mRFIDBluetoothManager = rfidBluetoothManager;
        mRFIDBluetoothReader = rfidBluetoothReader;
        if(mDeviceName.contains("ACR")){
            mReaderType = 2;
            mNFCBluetoothGattCallback = new NFCBluetoothGattCallback(activity, deviceNumber, rfidBluetoothManager.getNFCBluetoothManager());
        }
        else if(mDeviceName.contains("smartLINK")) {
            mReaderType = 1;
            mUHFBluetoothGattCallback = new UHFBluetoothGattCallback(activity, deviceNumber, rfidBluetoothReader.getUHFReader());
        }
    }


    /**
     * Overwritten method called when the BluetoothGattCallback detects a change on the
     * BluetoothCharacteristic.
     * @see BluetoothGattCallback#onCharacteristicChanged(BluetoothGatt, BluetoothGattCharacteristic)
     */
    @Override
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if(mReaderType == 1)
            mUHFBluetoothGattCallback.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        else if(mReaderType == 2)
            mNFCBluetoothGattCallback.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
    }

    /**
     * @see BluetoothGattCallback#onCharacteristicWrite(BluetoothGatt, BluetoothGattCharacteristic, int)
     */
    @Override
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, final int status) {
        if(mReaderType == 1)
            mUHFBluetoothGattCallback.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, status);
        else if(mReaderType == 2)
            mNFCBluetoothGattCallback.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, status);
    }

    /**
     * @see BluetoothGattCallback#onConnectionStateChange(BluetoothGatt, int, int)
     */
    @Override
    public void onConnectionStateChange(final BluetoothGatt bluetoothGatt, final int status, final int newState) {
        if(mReaderType == 1)
            mUHFBluetoothGattCallback.onConnectionStateChange(bluetoothGatt, status, newState);
        else if(mReaderType == 2)
            mNFCBluetoothGattCallback.onConnectionStateChange(bluetoothGatt, status, newState);
    }

    /**
     * @see BluetoothGattCallback#onServicesDiscovered(BluetoothGatt, int)
     */
    @Override
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, final int status) {
        if(mReaderType == 1)
            mUHFBluetoothGattCallback.onServicesDiscovered(bluetoothGatt, status);
        else if(mReaderType == 2)
            mNFCBluetoothGattCallback.onServicesDiscovered(bluetoothGatt, status);
    }

    /**
     * @see BluetoothGattCallback#onDescriptorWrite(BluetoothGatt, BluetoothGattDescriptor, int)
     */
    @Override
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, final BluetoothGattDescriptor bluetoothGattDescriptor, final int status) {
        if (mReaderType == 1)
            mUHFBluetoothGattCallback.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, status);
        else if (mReaderType == 2)
            mNFCBluetoothGattCallback.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, status);
    }

    /**
     * @return the BluetoothGattCallback instance in use, depending on the readerType.
     */
    public BluetoothGattCallback getBluetoothGattCallback(){
        if(mReaderType == 1)
            return mUHFBluetoothGattCallback;
        else if(mReaderType == 2)
            return mNFCBluetoothGattCallback.getBluetoothReaderGattCallback();
        return null;
    }


}
