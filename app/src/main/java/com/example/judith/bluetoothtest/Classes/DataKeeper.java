package com.example.judith.bluetoothtest.Classes;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;

import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCReader;

import java.io.InputStream;

/**
 * <b> DataKeeper is a singleton class that keep choices made by the user and several parameters
 * useful to navigate and use the app.</b>
 * <p>
 * Among those parameters we can quote :
 * <ul>
 * <li> the mode in which the user is : climbing mode, or mode to record a new path by scannin
 * tags on the wall.</li>
 * <li> If the user is in climibing mode, the chosen climbing option : beginner or expert mode, and
 * the sub-options corresponding : how many holds in expert mode.</li>
 * <li> A file representing the climbing route to be read by the app.</li>
 * <li> A file to be filled with the new tag IDs in recording mode.</li>
 * <li> The choice of guidance made by the user : how to indicate a distance, a direction, etc...</li>
 * <li> A parameter to indicate if it's the first time the app is used. </li>
 * <li> Counters </li>
 * <li> The size of the climber</li>
 * </ul>
 * </p>
 * <p> This class contains only one instance, intiated when the app is launched.
 * @see com.example.judith.bluetoothtest.Activities.SeparationActivity
 * </p>
 */


public class DataKeeper {

    /**
     * The instance of this class currently running
     */
    private static final DataKeeper mDataKeeper = new DataKeeper();

    /**
     * A Uri object representing the file to be read by the application.
     * This file is a CSV file in a specific format. For more precisions about the format of this
     * file, see (to come)
     */
    private Uri mUriLectureFile;

    /**
     * A Uri object representing the file to be filled with the data of the file read, and the
     * Tag IDs scanned on the wall in recording mode.
     * This file is basically a copy of the file that is going to be read (here represented by
     * mUriLectureFile), but with the tag IDs added during the scanning.
     * This file is a CSV file in a specific format. For more precisions about the format of this
     * file, ??
     */
    private Uri mUriRecordingFile;

    /**
     * The type of direction guidance the user in climbing mode choose :in a clock reference, in
     * degrees, etc...
     */
    private String mDirection;

    /**
     * The type of distance guidance the user in climbing mode choose :in centimeters, meters, etc...
     */
    private String mDistance;

    /**
     * A boolean indicating if the user choose to be aware for each hold of which hand should
     * take this hold
     */
    private boolean mHandPrecision;

    /**
     * A boolean indicating if the user choose to be aware of the shape of the hold
     */
    private boolean mHoldShape;

    /**
     * A boolean indicating if the user choose to be aware of the size of the hold
     */
    private boolean mHoldSize;

    /**
     * An int to keep track of the tag scanned during the recording mode. This browser allows to
     * restart recording from one specific tag chosen by the user.
     * @see DataKeeper#getCSVBrowser()
     * @see ReadersActivity#restartRecording(int)
     * @see NFCReader
     */
    private int mCSVBrowser;

    /**
     * A boolean indicating if the user choose the beginner climbing mode or the advanced climbing
     * mode.
     * True for beginner mode, false for expert mode.
     */
    private boolean mBeginnerMode;

    /**
     * A boolean indicating if the user choose the climbing mode or the recording mode.
     * True for climbing mode, false for recording mode.
     */
    private boolean mClimbingMode;

    /**
     * The size of the climber in meters.
     */
    private float mClimberSize;

    /**
     * The number of holds the user wants to be indicated in expert mode.
     * @see NFCReader
     */
    private int mHoldNumberAdvancedMode; // the number oh holds that will be read in expert mode


    private DataKeeper(){
        Log.d("CONSTRUCTION", "Construct DataKeeper");
    }

    /**
     * Returns the DataKeeper instance currently running.
     * @return mDataKeeper, or a new instance.
     */
    public static final DataKeeper getInstance(){
        if(mDataKeeper != null) {
            Log.d("DATA KEEPER", "mDataKeeper not null");
            return mDataKeeper;
        }
        Log.d("DATA KEEPER", "mDataKeeper null, new instance initialized");
        return new DataKeeper();
    }

    /**
     * Updates the direction option chosen by the user in climbing mode.
     * @param direction
     *              the new type of direction option
     * @see com.example.judith.bluetoothtest.Activities.HandPrecisionActivity#onClick(View)
     */
    public void setDirection(String direction){
        this.mDirection = direction;
    }

    /**
     * Returns the direction option chosen by the user in climbing mode.
     * This option will be used by the audio speaker when reading an indication.
     * @return mDirection
     * @see com.example.judith.bluetoothtest.Activities.ValidationAudioActivity#onClick(View)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    public String getDirection(){
        return mDirection;
    }

    /**
     * Updates the distance option chosen by the user in climbing mode.
     * @param distance
     *              the new type of distance option
     * @see com.example.judith.bluetoothtest.Activities.HandPrecisionActivity#onClick(View)
     */
    public void setDistance(String distance){
        this.mDistance = distance;
    }

    /**
     * Returns the distance option chosen by the user in climbing mode.
     * This option will be used by the audio speaker when reading an indication.
     * @return mDistance
     * @see com.example.judith.bluetoothtest.Activities.ValidationAudioActivity#onClick(View)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    public String getDistance(){
        return mDistance;
    }

    /**
     * Updates the hand precision option the user choose.
     * @param handPrecision
     *              true if the user wants the hand which should catch the hold precised by the
     *              audio speaker, false if not.
     * @see com.example.judith.bluetoothtest.Activities.HandPrecisionActivity#onClick(View)
     */
    public void setHandPrecision(Boolean handPrecision){
        this.mHandPrecision = handPrecision;
    }

    /**
     * Returns the option chosen by the user in climbing mode for precision of the hand to catch
     * the hold.
     * This option will be used by the audio speaker when reading an indication.
     * @return mHandPrecision
     * @see com.example.judith.bluetoothtest.Activities.ValidationAudioActivity#onClick(View)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    public boolean getHandPrecision(){
        return mHandPrecision;
    }

    /**
     * Updates the hold shape precision option the user choose.
     * @param holdShape
     *              true if the user wants the hold shape precised by the audio speaker, false if
     *              not.
     */
    public void setHoldShape(boolean holdShape){
        this.mHoldShape = holdShape;
    }

    /**
     * Returns the option chosen by the user in climbing mode about the hold shape.
     * This option will be used by the audio speaker when reading an indication.
     * @return mHoldShape
     * @see com.example.judith.bluetoothtest.Activities.ValidationAudioActivity#onClick(View)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    public boolean getHoldShape(){
        return mHoldShape;
    }

    /**
     * Updates the hold size precision option the user choose.
     * @param holdSize
     *              true if the user wants the hold size precised by the audio speaker, false if
     *              not.
     */
    public void setHoldSize(Boolean holdSize){
        this.mHoldSize = holdSize;
    }

    /**
     * Returns the option chosen by the user in climbing mode about the hold size.
     * This option will be used by the audio speaker when reading an indication.
     * @return mHoldSize
     * @see com.example.judith.bluetoothtest.Activities.ValidationAudioActivity#onClick(View)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    public boolean getHoldSize(){
        return mHoldSize;
    }

    /**
     * Updates the uri of the CSV file representing the climbing route to be read by the app.
     * @param uriLectureFile
     *              the new uri of the CSV file to be read.
     * @see com.example.judith.bluetoothtest.Activities.FileChoiceActivity#onActivityResult(int, int, Intent)
     */
    public void setUriLectureFile(Uri uriLectureFile){
        this.mUriLectureFile = uriLectureFile;
    }
    /**
     * Returns the uri of the CSV file to be read.
     * @return mUriLectureFile
     * @see ReadersActivity
     * Other usages in AudioActivity, FileChoiceActivity, TableActivity and ValidationAudioActivity.
     */
    public Uri getUriLectureFile(){
        return mUriLectureFile;
    }

    /**
     * Returns the uri of the CSV file to be written in recording mode.
     * @return mUriRecordingFile
     * @see com.example.judith.bluetoothtest.Activities.BluetoothActivity#onClick(View)
     * @see ReadersActivity
     */
    public Uri getUriRecordingFile() {
        return mUriRecordingFile;
    }

    /**
     * Updates the uri of the CSV file to be filled by the data of the lecture file and the tags
     * IDs.
     * @param uriRecordingFile
     *              the new uri of the CSV file to be written.
     * @see com.example.judith.bluetoothtest.Activities.RecordingFileChoiceActivity#onActivityResult(int, int, Intent)
     * @see com.example.judith.bluetoothtest.Activities.RecordingFileChoiceActivity#onClick(View)
     */
    public void setUriRecordingFile(Uri uriRecordingFile) {
        mUriRecordingFile = uriRecordingFile;
    }


    /**
     * Returns the boolean indicating if the user chose the beginner mode or the advanced mode in
     * climbing mode.
     * @return mBeginnerMode
     * @see ReadersActivity
     * @see GeneralUtils#readTableData(Context, InputStream, boolean)
     */
    public boolean isBeginnerMode() {
        return mBeginnerMode;
    }

    /**
     * Updates mBeginnerMode, indicating if the user chose the beginner mode or the advanced mode to
     * climb. This value is set when reading the CSV file, in the 9th column and the second line.
     * @param beginnerMode
     * @see GeneralUtils#readTableData(Context, InputStream, boolean)
     */
    public void setBeginnerMode(boolean beginnerMode) {
        mBeginnerMode = beginnerMode;
    }

    /**
     * Returns the size of the climber in meters. DataKeeper keep this data from the file read,
     * to write it in the second CSV file.
     * @return mClimberSize
     * @see ReadersActivity#writeOnCSV()
     */
    public float getClimberSize() {
        return mClimberSize;
    }

    /**
     * Updates the size of the climber.
     * @param climberSize
     * @see GeneralUtils#readTableData(Context, InputStream, boolean)
     */
    void setClimberSize(float climberSize) {
        mClimberSize = climberSize;
    }

    /**
     * Returns the number of holds the user wants to be aware of above him during climbing, in
     * advanced mode. DataKeeper keep this data from the file read, to write it in the second CSV
     * file.
     * This parameter will also be used by the speaker during climbing.
     * @return mHoldNumberAdvancedMode
     * @see ReadersActivity#writeOnCSV()
     * @see NFCReader
     */
    public int getHoldNumberAdvancedMode() {
        return mHoldNumberAdvancedMode;
    }

    /**
     * Updates the number of holds the user wants to be aware of above him during climbing, in
     * advanced mode.
     * @param holdNumberAdvancedMode
     *                          retrieved when the first CSV file is read.
     * @see GeneralUtils#readTableData(Context, InputStream)
     */
    public void setHoldNumberAdvancedMode(int holdNumberAdvancedMode) {
        mHoldNumberAdvancedMode = holdNumberAdvancedMode;
    }

    /**
     * Returns number keeping track of which element of the list of hold we are working on.
     * @return mCSVBrowser
     * @see NFCReader
     */
    public int getCSVBrowser() {return mCSVBrowser;
    }

    /**
     * Updates the index mCSVBrowser. Useful only in recording mode. Needs to be done at the
     * beginning at the recording, and when the user wants to restart the recording from a specific
     * tag.
     * @param csvBrowser
     *               the new value of CSVBrowser, got from the index of the tag from which we want
     *               to restart the recording.
     * @see ReadersActivity
     * @see ReadersActivity#writeOnCSV()
     */
    public void setCSVBrowser(int csvBrowser){
        mCSVBrowser = csvBrowser;
    }

    /**
     * Increments by 1 the index mCSVBrowser, each time a new tag is scanned.
     * @see NFCReader
     */
    public void incrCSVBrowser(){
        mCSVBrowser+=1;
    }

    /**
     * Returns the boolean indicating if the user chose the climing mode or the recording mode.
     * @return mClimbingMode
     * @see com.example.judith.bluetoothtest.Activities.FileChoiceActivity#onClick(View)
     * Other usages in AudioActivity, TableActivity and ValidationAudioActivity.
     */
    public boolean isClimbingMode() {
        return mClimbingMode;
    }

    /**
     * Updates the boolean indicating if the user chose the climing mode or the recording mode.
     * @param climbingMode
     * @see com.example.judith.bluetoothtest.Activities.FileChoiceActivity#onClick(View)
     */
    public void setClimbingMode(boolean climbingMode) {
        mClimbingMode = climbingMode;
    }

}
