package com.example.judith.bluetoothtest.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * A fragment used to display the UI view on climbing mode.
 * @see Fragment
 */
public class RFIDClimbingFragment extends Fragment implements View.OnClickListener{

    private TextView mTxtConnectionState1;
    private TextView mTxtConnectionState2;
    private TextView mTxtReaderName1;
    private TextView mTxtReaderName2;
    private TextView mTxtReader1Address;
    private TextView mTxtReader2Address;
    private TextView mTxtReader1TagID;
    private TextView mTxtReader2TagID;

    private onButtonClicked mOnButtonClicked;

    /**
     * Required empty public constructor
     */
    public RFIDClimbingFragment() {
    }


    /**
     * Interface to implement in the linked activity to trigger actions when a button in the
     * fragment is pressed.
     * @see ReadersActivity#startPollingClimbingFragment()
     */
    public interface onButtonClicked {
        void startPollingClimbingFragment();

        void reconnectReaders();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rfid_climbing, container, false);

        TextView mTxtInstructions;
        TextView mChosenMode;
        Button startPollingButton;
        Button reconnectButton;

        startPollingButton = (Button) view.findViewById(R.id.btn_startPolling);
        reconnectButton = (Button)view.findViewById(R.id.btn_reconnectReaders);

        startPollingButton.setOnClickListener(this);
        reconnectButton.setOnClickListener(this);

        mTxtConnectionState1 = (TextView) view.findViewById(R.id.textView_Reader1State_climbing_fragment);
        mTxtConnectionState2 = (TextView) view.findViewById(R.id.textView_Reader2State_climbing_fragment);
        mTxtReaderName1 = (TextView) view.findViewById(R.id.textView_Reader1Name_climbing_fragment);
        mTxtReaderName2 = (TextView) view.findViewById(R.id.textView_Reader2Name_climbing_fragment);
        mTxtReader1Address = (TextView)view.findViewById(R.id.textView_Reader1Address_climbing_fragment);
        mTxtReader2Address = (TextView)view.findViewById(R.id.textView_Reader2Address_climbing_fragment);
        mTxtReader1TagID = (TextView)view.findViewById(R.id.textView_reader1_tagId_climbing_fragment);
        mTxtReader2TagID = (TextView)view.findViewById(R.id.textView_reader2_tagId_climbing_fragment);


        mTxtInstructions = (TextView)view.findViewById(R.id.instructions_climbing_fragment);
        mTxtInstructions.setText(getString(R.string.instructions_nfc_activity,getString(R.string.nfc_reader_start_polling)));

        mChosenMode = (TextView)view.findViewById(R.id.text_chosenMode_climbing_fragment);
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        /* Inflate the layout for this fragment*/
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_startPolling:
                mOnButtonClicked.startPollingClimbingFragment();
                break;
            case R.id.btn_reconnectReaders:
                mOnButtonClicked.reconnectReaders();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        Activity activity = (Activity) context;

        try {
            mOnButtonClicked = (onButtonClicked) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    /**
     * Updates the state connection of a reader in the UI view.
     * @param connectionState
     *                      the new state connection
     * @param number
     *             an int to identify the reader whose connection state has changed, and therefore
     *             the textView corresponding
     * @see ReadersActivity#updateConnectionState(int, int)
     */
    public void updateConnectionState(String connectionState, int number){
        if(number ==1)
            mTxtConnectionState1.setText(connectionState);
        else if(number ==2)
            mTxtConnectionState2.setText(connectionState);

    }

    public boolean getConnectionState(int deviceNumber) {
        if (deviceNumber == 1)
            return (mTxtConnectionState1.getText().equals(getString((R.string.connected_nfc_reader))));
        else if (deviceNumber == 2)
            return (mTxtConnectionState2.getText().equals(getString(R.string.connected_nfc_reader)));
        return false;
    }

    /**
     * Updates the state connection of a reader in the UI view.
     * @param device1
     *              the name of the first BluetoothDevice
     * @param device2
     *              the name of the second BluetoothDevice
     * @param address1
     *              the mac address of the first BluetoothDevice
     * @param address2
     *              the mac address of the second BluetoothDevice
     */
    public void setDeviceUI(String device1, String device2, String address1, String address2){
        mTxtReaderName1.setText(device1);
        mTxtReaderName2.setText(device2);
        mTxtReader1Address.setText(address1);
        mTxtReader2Address.setText(address2);
    }

    /**
     * Displays the ID of the tag just scanned on the UI.
     * @param tagID
     *          the ID of the tag
     * @param deviceNumber
     *                  the number of the reader which scanned the tag, 1 or 2.
     */
    public void setTagID(String tagID, int deviceNumber){
        if(deviceNumber == 1)
            mTxtReader1TagID.setText(tagID);
        else if(deviceNumber == 2)
            mTxtReader2TagID.setText(tagID);
    }
}
