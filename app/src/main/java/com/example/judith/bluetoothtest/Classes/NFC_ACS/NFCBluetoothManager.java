package com.example.judith.bluetoothtest.Classes.NFC_ACS;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.util.Log;

import com.acs.bluetooth.Acr1255uj1Reader;
import com.acs.bluetooth.BluetoothReader;
import com.acs.bluetooth.BluetoothReaderGattCallback;
import com.acs.bluetooth.BluetoothReaderManager;
import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.R;

/**
 * NFCBluetoothManager is a wrapper class for a BluetoothReaderManager instance from the ACS
 * API. It implement the methods of this class and its behaviour.
 */

public class NFCBluetoothManager {

    /**
     * Context object to represent the context in which the NFCReader is used.
     */
    private Context mContext;

    /**
     * Object to represent the manager of a NFC Bluetooth reader. Obtained from the ACS API.
     * @see BluetoothReaderManager
     */
    private BluetoothReaderManager mBluetoothReaderManager;

    /**
     * Activity object to represent the activity calling the NFCBluetoothManager.
     */
    private ReadersActivity mReadersActivity;

    /**
     * Variable to identify the reader, 1 for the first reader, 2 for the second reader.
     * Each BluetoothReader object has its own BluetoothReaderManager object corresponding.
     */
    private int mDeviceNumber;

    public NFCBluetoothManager(Activity activity, int deviceNumber, final NFCReader nfcReader){
        mContext = (Context) activity;

        mReadersActivity = (ReadersActivity)activity;

        mDeviceNumber = deviceNumber;

        mBluetoothReaderManager = new BluetoothReaderManager();
        Log.d("RFID READER", "CREATION OF BLUETOOTH READER MANAGER");
        mBluetoothReaderManager.setOnReaderDetectionListener(new BluetoothReaderManager.OnReaderDetectionListener() {
            /**
             * Overwritten method of the onReaderDetection method of the BluetoothReaderManager
             * class to set the behaviour of the listener when a reader is detected, e.g. when the
             * detectReader method is called.
             * This method check the version of the NFC Reader (1255UJ1), and then calls the
             * setListenerNFCReader method on the NFCReader object the manager is coupled with.
             * @param bluetoothReader
             *                  the BluetoothReader object that the manager handles
             * @see BluetoothReaderManager#detectReader(BluetoothGatt, BluetoothReaderGattCallback)
             * @see NFCReader#setListenerNFCReader()
             */
            @Override
            public void onReaderDetection(BluetoothReader bluetoothReader) {
                if (bluetoothReader instanceof Acr1255uj1Reader) {
                    /* The connected reader is ACR1255U-J1 reader. */
                    Log.v("NFC READERS", "On Acr1255uj1Reader Detected.");
                }else{
                    mReadersActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            /* Only ACR1255 and ACR 301 are supported */
                            GeneralUtils.toast(mReadersActivity, mContext.getString(R.string.toast_unsupported_device_nfc_reader,mDeviceNumber));
                            Log.v("NFC READERS", "Déconnexion du périphérique 1");
                            mReadersActivity.disconnectReader(mDeviceNumber);
                            if(mReadersActivity.getSupportFragmentManager().findFragmentById(R.id.container_nfc_activity).getTag().equals("NFCFragment"))
                                mReadersActivity.updateConnectionState(BluetoothReader.STATE_DISCONNECTED, mDeviceNumber);
                        }
                    }); return;
                }

                nfcReader.setBluetoothReader(bluetoothReader);
                nfcReader.setListenerNFCReader();
                mReadersActivity.activateReader(mDeviceNumber);
            }
        });

    }

    /**
     * Returns the BluetoothReaderManager instance from ACS API wrapped in this
     * NFCBluetoothManager instance.
     * @return mBluetoothReaderManager, the instance representing a NFC Bluetooth reader's manager
     * @see ReadersActivity
     */
    BluetoothReaderManager getBluetoothReaderManager(){
        return mBluetoothReaderManager;
    }

    /**
     * Updates the instance of BluetoothReaderManager wrapped in the NFCBluetoothManager
     * instance.
     * @param bluetoothReaderManager
     *                          the new value of the BluetoothReaderManager instance
     * @see NFCBluetoothGattCallback
     */
    void setBluetoothReaderManager(BluetoothReaderManager bluetoothReaderManager){
        this.mBluetoothReaderManager = bluetoothReaderManager;
    }
}
