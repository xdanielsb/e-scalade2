package com.example.judith.bluetoothtest.Classes.UHF_SLS;

/**
 * Frame Data is a class used to store the UHF reader answer. A Frame Data object can contain a
 * tag's EPC, an ACK frame, or data asked to the reader such as its version or serial number.
 */
public class FrameData {

    /**
     * A byte to store the second byte of the byte array. Needs to be checked when the FrameData
     * object contains a Tag's EPC. If this byte is not equal to 3, then the anwer is not valid.
     * @see UHFReader#onFrameReceived(byte[])
     */
    private byte mByte1;

    /**
     * A byte indicating if an error occurred during communication; if equal to -1.
     */
    private byte mErrorByte;


    /**
     * The byte array containing the responses frames.
     */
    private byte[] mByteArray;

    FrameData(byte b1, byte errorByte, byte[] bArr) {
        this.mByte1 = b1;
        this.mErrorByte = errorByte;
        this.mByteArray = bArr;
    }

    byte getByte1() {
        return this.mByte1;
    }

    byte getErrorByte() {
        return this.mErrorByte;
    }

    byte[] getByteArray() {
        return this.mByteArray;
    }
}
