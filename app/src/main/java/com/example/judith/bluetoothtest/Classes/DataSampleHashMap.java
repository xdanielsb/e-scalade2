package com.example.judith.bluetoothtest.Classes;

import com.example.judith.bluetoothtest.Classes.DataSample;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * DataSampleHashMap is a wrapper for a LinkedHashMap class, with method added to access easily to
 * an element of the LinkedHasMap with a given index.
 * This class might be replaced in the future by a class of Java common collection offering these
 * features.
 * The keys of the object are NFC tag UUIDs in a String format. The values are DataSample objects,
 * representing a hold on a wall. So each hold corresponds to a tag.
 */
public class DataSampleHashMap {

    /**
     * The LinkedHashMap wrapped by this class.
     */
    private LinkedHashMap<String, DataSample> mHashMap;



    public DataSampleHashMap(){
        mHashMap = new LinkedHashMap<>();
    }

    /**
     * @return the size of the LinkedHashMap wrapped by this class.
     */
    public int getSize(){
        return mHashMap.size();
    }

    /**
     * Look for the String passed as argument to see if it's contained in the LinkedHashMap as a
     * key.
     * @param uuid
     *          the uuid in a String format
     * @return true if the String is a key in the LinkedHashMap, false if not.
     */
    public boolean foundUUID(String uuid){
        return (mHashMap.get(uuid) != null);
    }

    /**
     * Gets the DataSample value of the LinkedHashMap corresponding to the key passed as a
     * parameter.
     * @param uuid
     *          the key of the value wanted
     * @return the DataSample object associated to the key
     */
    public DataSample getDataSampleWithUUID(String uuid){
        return mHashMap.get(uuid);
    }

    /**
     * Gets an element of the LinkedHashMap corresponding to the index passed as a parameter.
     * The LinkedHashMap class guarantees an order for its elements which is the order of the entry.
     * @param i
     *          the index of the element wanted
     * @return the DataSample object positioned to this index
     */
    public DataSample getDataSampleByIndex(int i){
        return mHashMap.get(mHashMap.keySet().toArray()[i]);
    }

    /**
     * Gets the key of an element of the LinkedHashMap corresponding to the index passed as a
     * parameter. The LinkedHashMap class guarantees an order for its elements which is the order
     * of entry.
     * @param i
     *          the index of the key-value couple wanted
     * @return the key corresponding to that index, in a String format
     */
    public String getKeyByIndex(int i) {
        DataSample sample = getDataSampleByIndex(i);
        for(Map.Entry<String, DataSample> entry : mHashMap.entrySet()){
            if(Objects.equals(sample, entry.getValue()))
                return entry.getKey();
        }
        return("KEY NOT FOUND");

    }

    /**
     * Adds a new key-value couple to the LinkedHashMap object.
     * @param key
     *         the key of the new element
     * @param dataSample
     *              the value of the new element
     */
    public void putDataSample(String key, DataSample dataSample){
        mHashMap.put(key, dataSample);
    }

    /**
     * Gets the LinkedHasMap instance wrapped by this DataSampleHashMap instance.
     * @return the LinkedHashMap instance
     */
    public LinkedHashMap<String, DataSample> getHashMap() {
        return mHashMap;
    }
}
