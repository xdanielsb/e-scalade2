package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> HandPrecisionActivity is the third activity of a series of 5 activities that allow the user
 * to customize the audio message he will hear during the climbing.</b>
 *<p> This activity allows him to choose or not to have an additional indication about the hand
 * that should catch the next hold. (Each hold is qualified either as "Left hand" or "Righ hand").
 *<p>
 * Once the user has made his choice by selecting one button (yes or no), the result is kept
 * in the singleton call DataKeeper.
 * @see DataKeeper#setHandPrecision(Boolean)
 *</p>
 *
 */

public class HandPrecisionActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButtonOui;
    private Button mButtonNon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hand_precision);

        mButtonOui = (Button)findViewById(R.id.button_climb_separation_activity);
        mButtonNon = (Button)findViewById(R.id.button_record_new_route_separation_activity);
        mButtonOui.setOnClickListener(this);
        mButtonNon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent precisionMainIntent = new Intent(HandPrecisionActivity.this, HoldShapeActivity.class);
        switch (v.getId()) {
            case R.id.button_climb_separation_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setHandPrecision(true);
                startActivity(precisionMainIntent);
                break;
            case R.id.button_record_new_route_separation_activity:
                // Save data in the Data Keeper
                DataKeeper.getInstance().setHandPrecision(false);
                startActivity(precisionMainIntent);
                break;
        }
    }
}
