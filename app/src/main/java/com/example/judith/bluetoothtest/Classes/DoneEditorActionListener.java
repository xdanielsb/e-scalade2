package com.example.judith.bluetoothtest.Classes;

import android.content.Context;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

/**
 * <b> DoneEditorActionListener is a class containing one single method inherited from
 * TextView.OnEditorActionListener interface.
 * This method dictates to every object of this class how to behave when the "OK" button of the
 * editor is pressed. Here, the keyboard has to close when the user press "OK".
 * </b>
 * @see com.example.judith.bluetoothtest.Activities.RecordingFileChoiceActivity
 */


public class DoneEditorActionListener implements TextView.OnEditorActionListener {
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_DONE){
            InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(),0);
        }
        return false;
    }
}
