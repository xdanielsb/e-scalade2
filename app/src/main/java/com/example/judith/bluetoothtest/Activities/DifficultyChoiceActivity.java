package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Fragments.AdvancedModeFragment;
import com.example.judith.bluetoothtest.Fragments.DifficultyChoiceFragment;
import com.example.judith.bluetoothtest.R;


/**
 * <b> DifficultyChoiceActivity is an activity that allows the user to make 2 choices once in
 * climbing mode.</b>
 *<p>
 *     The first choice is displayed by the first fragment DifficultyChoiceFragment, where the user
 *     has to choose the difficulty of the climbing; beginner or advanced (see the Fragment comments
 *     for a better description of both difficulties.
 *     The second choice is displayed in the second fragment AdvancedModeFragment, where the user
 *     has to choose the number of holds to be indicated to the user in advanced mode (this option
 *     is not possible in beginner mode).
 *</p>
 * @see DifficultyChoiceFragment
 * @see AdvancedModeFragment
 *
 */
public class DifficultyChoiceActivity extends AppCompatActivity implements AdvancedModeFragment.onButtonPressed, DifficultyChoiceFragment.onChoiceMade {

    private AdvancedModeFragment mAdvancedModeFragment;

    private DifficultyChoiceFragment mDifficultyChoiceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty_choice);

        mAdvancedModeFragment = new AdvancedModeFragment();
        mDifficultyChoiceFragment = new DifficultyChoiceFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.container_climbing_difficulty_activity,
                mDifficultyChoiceFragment,"DifficultyChoiceFragment").commit();
    }

    @Override
    public void onNumberChosen() {
        Intent intent = new Intent(DifficultyChoiceActivity.this, AudioActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBeginnerModeChosen() {
        DataKeeper.getInstance().setBeginnerMode(true);
        Intent intent = new Intent(DifficultyChoiceActivity.this, AudioActivity.class);
        startActivity(intent);
    }

    @Override
    public void onAdvancedModeChosen() {
        DataKeeper.getInstance().setBeginnerMode(false);
        getSupportFragmentManager().beginTransaction().replace(R.id.container_climbing_difficulty_activity,
                mAdvancedModeFragment,"AdvancedModeFragment").commit();
    }
}
