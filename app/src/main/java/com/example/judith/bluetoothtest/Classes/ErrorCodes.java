package com.example.judith.bluetoothtest.Classes;

import android.content.Context;

import com.acs.bluetooth.BluetoothReader;
import com.example.judith.bluetoothtest.R;

public class ErrorCodes {
    public static final int INVALID_PARAMETER = -101;

    public static final int DEVICE_NOT_OPENED = -201;

    public static final int DEVICE_BUSY = -202;

    public static final int OPERATE_FAILED = -204;

    public static final int LOW_POWER = -205;

    public static final int COMMUNICATE_TIMEOUT = -301;

    public static final int COMMUNICATE_FAILED = -303;

    /**
     * A class to handle the exception thrown by the UHF reader. An UFH exception is composed of
     * an error code and a message associated.
     */
    public class UHFException extends Exception {

        private int mErrorCode;

        public UHFException(int errorCode, String message) {
            super(message);
            this.mErrorCode = errorCode;
        }

        public int getErrorCode() {
            return this.mErrorCode;
        }
    }

    /**
     * Gets the error message from the NFC reader and returns it in a String readable format.
     * @param errorCode
     *              the error code of the communication.
     * @return a String from string.xml corresponding to the error message.
     */
    public static String getErrorNFCString(int errorCode, Context context) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            return "";
        } else if (errorCode == BluetoothReader.ERROR_INVALID_CHECKSUM) {
            return context.getString(R.string.invalid_checksum_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA_LENGTH) {
            return context.getString(R.string.invalid_dataLenght_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_INVALID_COMMAND) {
            return context.getString(R.string.invalid_command_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_UNKNOWN_COMMAND_ID) {
            return context.getString(R.string.unknown_commandID_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_CARD_OPERATION) {
            return context.getString(R.string.cardOperation_failure_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_REQUIRED) {
            return context.getString(R.string.auth_required_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_LOW_BATTERY) {
            return context.getString(R.string.battery_low_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_CHARACTERISTIC_NOT_FOUND) {
            return context.getString(R.string.caracteristic_notFound_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_WRITE_DATA) {
            return context.getString(R.string.write_command_failure_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_TIMEOUT) {
            return context.getString(R.string.timeout_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_FAILED) {
            return context.getString(R.string.toast_auth_failure_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_UNDEFINED) {
            return context.getString(R.string.undefined_error_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA) {
            return context.getString(R.string.invalid_data_nfc_reader);
        } else if (errorCode == BluetoothReader.ERROR_COMMAND_FAILED) {
            return context.getString(R.string.command_failure_nfc_reader);
        }
        return context.getString(R.string.unknown_error_nfc_reader);
    }
}
