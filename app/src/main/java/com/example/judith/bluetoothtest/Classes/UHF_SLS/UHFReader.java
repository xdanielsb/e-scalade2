package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.util.Log;

import com.example.judith.bluetoothtest.Classes.ErrorCodes;
import com.example.judith.bluetoothtest.Classes.GeneralRFID.RFID_Reader;
import com.example.judith.bluetoothtest.Classes.ReaderToActivity;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * UHFReader is the class representing a UHF reader device. Contains several methods for good
 * communication between the phone and the reader :
 * <ul>
 *     <li> several methods to process frames before they are sent to the reader (m78a,
 *     prepareFrameForSending, etc...), </li>
 *     <li> a method onAnswerReceived to process the answer received from the reader in a frame
 *     format, </li>
 *     <li> several methods to perform actions on the reader : set parameters such as buzzer, power
 *     strength, and get answers such as inner characteristics of the reader and the ids of detected
 *     tags, </li>
 * </ul>
 */

public class UHFReader {

    private final String TAG = UHFReader.class.getSimpleName();

    private static final UUID DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    /* Predefined bytes to send to the reader for communication */

    private static final byte GET_BUZZER_BYTE = (byte) -26;
    private static final byte SET_LIGHT_INDICATOR_BYTE = (byte) -25;
    private static final byte GET_LIGHT_INDICATOR_BYTE = (byte) -24;
    private static final byte SET_SERIAL_NUMBER_BYTE = (byte) -23;
    private static final byte GET_CHANNEL_BYTE = (byte) 17;
    private static final byte SET_CHANNEL_BYTE = (byte) 18;
    private static final byte GET_TAG_TEMPERATURE_BYTE = (byte) -28;
    private static final byte KILL_TAG_BYTE = (byte) 101;
    private static final byte LOCK_TAG_BYTE = (byte) -126;
    private static final byte READ_TAG_BYTE = (byte) 41;

    private static final byte GET_VERSION_BYTE = (byte) 3;
    private static final byte GET_BAND_AREA_BYTE = (byte) 6;
    private static final byte SET_BAND_AREA_BYTE = (byte) 7;
    private static final byte GET_POWER_BYTE = (byte) 21;
    private static final byte SET_POWER_BYTE = (byte) 22;
    private static final byte SINGLE_INVENTORY_BYTE = (byte) 34;
    private static final byte WRITE_TAG_BYTE = (byte) 70;
    private static final byte GET_BATTERY_BYTE = (byte) -30;
    private static final byte SET_BUZZER_BYTE = (byte) -29;
    private static final byte SET_READER_NAME_BYTE = (byte) -27;
    private static final byte GET_SERIAL_NUMBER_BYTE = (byte) -22;

    /**
     * boolean indicating if the object is currently connected to the smartphone.
     * @see UHFBluetoothGattCallback#onDescriptorWrite(BluetoothGatt, BluetoothGattDescriptor, int)
     */
    private boolean mIsConnected;

    /**
     * Permanent interface for communication with the reader.
     */
    private communicationInterface mCommunicationInterface;

    /**
     * Instance of FrameProcessing object, used during the communication with the reader in onFrameReceived
     * method and in m78a method. Useful class to exchange frames with the reader.
     * @see FrameProcessing
     */
    private FrameProcessing mFrameProcessing;

    /**
     * List of C0355 objects used in tag polling.
     * @see this#singleInventory()
     */
    private ArrayList<FrameData> mFrameDataList;

    /**
     * Runnable to handle the communication timeouts.
     */
    private Runnable mCommunicationTimeoutRunnable = new CommunicationTimeoutRunnable();

    /**
     * Interface to send data and runnables to the activity.
     * @see ReaderToActivity
     */
    private ReaderToActivity mInterfaceToActivity;


    /**
     * Instance of InventoryRunnable class called for tag polling.
     */
    private InventoryRunnable mInventoryRunnable;

    /**
     * int to identify the reader, 1 or 2.
     */
    private int mDeviceNumber;

    /**
     * Instance of BluetoothGatt representing the Gatt connection to this UHF reader.
     */
    private BluetoothGatt mBluetoothGatt;

    /**
     * Instance of BluetoothGattCallback for the connection to this UHF reader.
     */
    private UHFBluetoothGattCallback mUHFBluetoothGattCallback;

    /**
     * ArrayList of bytes used to store the frames sent and received to from the reader.
     */
    private List<byte[]> mByteArrayList;


    /**
     * Runnable class to display a message when the communication timeout is reached. After 2
     * seconds without answer from the UHF reader, the runnable performs an answer method
     */
    class CommunicationTimeoutRunnable implements Runnable {

        CommunicationTimeoutRunnable() {
        }

        public void run() {
            //mFrameProcessing = null; uncomment to imitate the app
            communicationInterface communicationInterface = mCommunicationInterface;
            mCommunicationInterface = null; // reset the permanent communication interface
            ErrorCodes.UHFException UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_TIMEOUT, "Communicate timeout");
            if (communicationInterface != null) {
                communicationInterface.onAnswerReceived(UHFException, null);
            }
        }
    }

    /**
     * Interface to communicate with the UHF reader. Method onAswerReceived to implement, which is
     * the method called when the phone received an answer from the phone.
     */
    interface communicationInterface {
        /**
         * Performs an action adequate to the answer received.
         * @param UHFException
         *              the exception sent by the reader,null if the communication succeeded.
         * @param frameData
         *          the structure containing the answer from the reader.
         */
        void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData);
    }

    public UHFReader(Activity activity, int deviceNumber) {
        mByteArrayList = new ArrayList<>();
        mInterfaceToActivity = (ReaderToActivity)activity;
        mFrameDataList = new ArrayList<>(); //line moved to singleInventory() to imitate the app
        mDeviceNumber = deviceNumber;
        mInventoryRunnable = new InventoryRunnable(activity, this);
    }

    /**
     * Method called when a frame is received from the reader trough the onCharacteristicChanged
     * method called on the
     * This method has to perform different actions whether the answer is a tag ID or any other
     * answer from the reader.
     * The methods uses the new mFrameProcessing object to operate on the frame and to extract the
     * desired values. Depending in the nature of mFrameProcessing, (i.e. if the answer is a tag ID
     * or not), different methods of this object will be called.
     * Methods m21b and m19c check that the format of the frame is correct.
     * Methods m22b and m23c extract the information desired from the frame. This information is
     * then stored into a FrameData object.
     * @param value
     *          the frame received from the reader
     */
    void onFrameReceived(byte[] value) {
        Log.i(TAG, "onFrameReceived method started");
        Log.d(TAG, "VALUE = " + Arrays.toString(value));
        if (mFrameProcessing != null) {
            //Log.i(TAG, "mFrameProcessing is not null");
            communicationInterface communicationInterface = null;
            FrameData frameData = null;
            ErrorCodes.UHFException UHFException = null;
            mFrameDataList = new ArrayList<>();
            int frameFormat;
            byte b2;

            /* If the frame received is supposed to be a tag Id, i.e. the result of a singleInventory operation */
            if (mFrameProcessing.isTagReading()) {
                /* Checking the frame format and store the result in b */
                frameFormat = mFrameProcessing.m21b(value);
                Log.d("TAG", "frame format = " + frameFormat);

                if (frameFormat == 2) {
                    do {
                        FrameData c2 = mFrameProcessing.m23c();
                        if (mFrameDataList != null) {
                            Log.i(TAG, "mFrameDataList is not null | added to list");
                            mFrameDataList.add(c2);
                        }
                        frameFormat = mFrameProcessing.m21b(new byte[0]);
                    } while (frameFormat == 2);
                }
                /* frameFormat <= 0 == the data transmission is done, no more frames are expected. */
                if (frameFormat <= 0) {
                    frameData = mFrameProcessing.m23c();
                    Log.i(TAG, "mFrameProcessing.m23c() (aka frameData) = " + Arrays.toString(frameData.getByteArray()) + " " + frameData.getByte1() + " " + frameData.getErrorByte());
                    if (frameData.getByte1() == (byte) 3 && mFrameDataList != null) {
                        mFrameDataList.add(frameData);
                        EpcReply reply = UHFUtils.frameDataToEpcReply(frameData);
                        Log.i(TAG, "EPC? = " + UHFUtils.hex2strWithSpace(reply.getEpc()));
                        Log.i(TAG, "EPC decoded = " + UHFUtils.hex2strWithSpace(reply.getEpc()).replace(" ",""));
                        mInterfaceToActivity.onTagRead(UHFUtils.hex2strWithSpace(reply.getEpc()),mDeviceNumber);
                        this.stopTagPollingUHF(true);
                    }
                    mInterfaceToActivity.removeRunnableFromHandler(mCommunicationTimeoutRunnable);
                    if (mCommunicationInterface != null) {
                        communicationInterface = mCommunicationInterface;
                        mCommunicationInterface = null;
                        if (frameFormat < 0) { // b can be equal to -1, -2, -3, -4 and 0. All except 0 are result of an error in either
                            UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_FAILED, "Communicate failed");
                        } else if (frameData.getByteArray() == null || frameData.getByteArray().length <= 0) {
                            UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_FAILED, "Info content of reply is null");
                        } else if (frameData.getErrorByte() == (byte) -1) {
                            b2 = frameData.getByteArray()[0];
                            if (b2 != (byte) 21) {
                                if (b2 != 31) {
                                    String string1 = "Inventory tag failed : ";
                                    String string2 = String.format("%02X", frameData.getByteArray()[0]);
                                    UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_FAILED, string1+string2);
                                } else {
                                    UHFException = new ErrorCodes().new UHFException(ErrorCodes.LOW_POWER, "Low power");
                                }
                            }
                        }
                        Log.d(TAG, "UHFException = " + UHFException);
                    }
                }

            /* If the frame is received from the reader is everything but a tag Id */
            } else {
                Log.i(TAG, "calling m19a ");
                frameFormat = mFrameProcessing.m19a(value);
                Log.i(TAG, "frame format = " + frameFormat);
                /* frameFormat <= 0 == the data transmission is done, no more frames are expected. */
                if (frameFormat <= 0) {
                    frameData = mFrameProcessing.m22b();
                    Log.i(TAG, "setting frameData = " + frameData);
                    mInterfaceToActivity.removeRunnableFromHandler(mCommunicationTimeoutRunnable);
                    if (mCommunicationInterface != null) {
                        Log.i(TAG, "communicationInterface is not null!");
                        communicationInterface = mCommunicationInterface;
                        Log.i(TAG, "anInterface = " + communicationInterface);
                        mCommunicationInterface = null;
                        if (frameFormat < 0) {
                            Log.i(TAG, "Communicate Failed");
                            UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_FAILED, "Communicate failed");
                        } else if (frameData.getByteArray() == null || frameData.getByteArray().length <= 0) {
                            Log.i(TAG, "Info reply is null");
                            UHFException = new ErrorCodes().new UHFException(ErrorCodes.COMMUNICATE_FAILED, "Info content of reply is null");
                        } else if (frameData.getErrorByte() == (byte) -1) {
                            b2 = frameData.getByteArray()[0];
                            if (b2 != (byte) 14) {
                                if (b2 != (byte) 31)
                                    UHFException = new ErrorCodes().new UHFException(ErrorCodes.OPERATE_FAILED, "Operate failed");
                                else
                                    UHFException = new ErrorCodes().new UHFException(ErrorCodes.LOW_POWER, "Low Power");
                            } else
                                UHFException = new ErrorCodes().new UHFException(ErrorCodes.INVALID_PARAMETER, "Invalid Parameter");
                        }
                    }
                }
            }
            if (communicationInterface != null && frameData != null) {
                Log.i(TAG, "starting onAnswerReceived");
                communicationInterface.onAnswerReceived(UHFException, frameData);
            }
            else
                Log.i(TAG, "anInterface null or frameData null : " + communicationInterface + "  " + frameData);

        } else {
            Log.i(TAG, "mFrameProcessing is null");
        }
    }

    /**
     *Performs several actions to check the frame to be send to the reader :
     * <ul>
     *     <li> Checks that the device is connected to the phone by checking the mIsConnected
     *     boolean, </li>
     *     <li> checks that the device is not already communicating with the phone, i.e. the
     *     interface for communication is null, </li>
     * </ul>
     * Then creates an instance of FrameProcessing class to process the frames to be exchanged with
     * the reader.
     * @param byteArray
     *              The frame to be sent to the reader
     * @param communicationInterface
     *                          The interface to handle communication with the reader
     */
    private void m78a(byte[] byteArray, communicationInterface communicationInterface) {
        if (!this.mIsConnected) {
            communicationInterface.onAnswerReceived(new ErrorCodes().new UHFException(ErrorCodes.DEVICE_NOT_OPENED, "Device not opened"), null);
        } else if (mCommunicationInterface != null) {
            communicationInterface.onAnswerReceived(new ErrorCodes().new UHFException(ErrorCodes.DEVICE_BUSY, "Device busy"), null);
        } else {
            mCommunicationInterface = communicationInterface;
            if (byteArray[2] == (byte) 34) //the second byte is the one specifying the question, 34 = ask for single inventory
                this.mFrameProcessing = new FrameProcessing(true); //if this is about a single inventory
            else
                this.mFrameProcessing =  new FrameProcessing(false); //if this is another demand that single inventory

            mInterfaceToActivity.postDelayedRunnableToHandler(mCommunicationTimeoutRunnable, 2000);
            prepareFrameForSending(byteArray);
        }
    }

    /**
     * Updates the connection state of the reader by setting the boolean mIsConnected.
     * @param isConnected
     *              the new state of the connection
     * @see UHFBluetoothGattCallback#onDescriptorWrite(BluetoothGatt, BluetoothGattDescriptor, int)
     */
    void setIsConnected(boolean isConnected){
        mIsConnected = isConnected;
    }

    /**
     * Updates the communication interface between the reader and the phone.
     * @param communicationInterface
     *              the new state of the mCommunicationInterface
     * @see UHFBluetoothGattCallback#onDescriptorWrite(BluetoothGatt, BluetoothGattDescriptor, int)
     */
    void setCommunicationInterface(communicationInterface communicationInterface){
        mCommunicationInterface = communicationInterface; }

    /**
     * Updates the BluetoothGatt instance representing the phone connection with this UHF reader
     * object.
     * @param bluetoothGatt
     *              The BluetoothGatt instance of this reader
     */
    public void setBluetoothGatt(BluetoothGatt bluetoothGatt){
        mBluetoothGatt = bluetoothGatt;
    }

    /**
     * Updates the BluetoothGattCallback instance representing the phone connection with this UHF reader
     * object.
     * @param uhfBluetoothGattCallback
     *              The BluetoothGattCallback instance of this reader
     */
    public void setBluetoothGattCallback(UHFBluetoothGattCallback uhfBluetoothGattCallback){
        mUHFBluetoothGattCallback = uhfBluetoothGattCallback;
    }

    /**
     * Enables the notifications on the characteristic passed as a parameter. It means that if the
     * characteristic changes, onCharacteristicChanged will be called. This method is called one
     * time for each one of the two characteristics.
     * Then, writes the ENABLE_NOTIFICATION_VALUE on the Descriptor, which means that the
     * notifications for this descriptor are enabled.
     * @return the result of the writeDescriptor method called, true if the write operation was
     * initiated successfully
     * @param bluetoothGattCharacteristic
     *                              the BluetoothGattCharacteristic on which to enable notifications
     * @see BluetoothGatt#setCharacteristicNotification(BluetoothGattCharacteristic, boolean)
     * @see BluetoothGatt#writeDescriptor(BluetoothGattDescriptor)
     */
    boolean enableNotificationOnCharacteristic(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        this.mBluetoothGatt.setCharacteristicNotification(bluetoothGattCharacteristic, true);
        BluetoothGattDescriptor descriptor = bluetoothGattCharacteristic.getDescriptor(DESCRIPTOR_UUID);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        return this.mBluetoothGatt.writeDescriptor(descriptor);
    }

    /**
     * Takes the first byte array from mByteArrayList and writes on the BluetoothGattCharacteristic
     * passed as a parameter (i.e. the second characteristic of the reader). Then removes it from
     * mBytesArrayList.
     * @param bluetoothGattCharacteristic
     *                              the BluetoothGattCharacteristic to write on
     * @see UHFBluetoothGattCallback#onCharacteristicWrite(BluetoothGatt, BluetoothGattCharacteristic, int)
     * @see UHFBluetoothGattCallback#getBluetoothGattCharacteristic2()
     */
    void writeByteArrayCharacteristic(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        Log.i(TAG, "writeByteArrayCharacteristic started");
        Log.i(TAG, "mByteArrayList.size() = " + mByteArrayList.size());
        if (this.mByteArrayList.size() > 0) {
            String string = new String(mByteArrayList.get(0), StandardCharsets.UTF_8);
            Log.i(TAG, "mByteArrayList.get(0) = " + Arrays.toString(mByteArrayList.get(0)));
            byte[] bArr = (byte[]) mByteArrayList.get(0);
            this.mByteArrayList.remove(0);
            bluetoothGattCharacteristic.setValue(bArr);
            if (this.mBluetoothGatt != null) {
                this.mBluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
            }
        }
    }

    /**
     * Prepares the frame before sending it to the reader. The phone communicates with the reader
     * by writing on its BluetoothGattCharacteristic. This methods converts the frame passed as an
     * argument from a byte array with a random size to a series of byte arrays with a maximum
     * size of 20, and sends them to the reader by calling the writeByteArrayCharacteristic method
     * on one of the reader's two caracteristics (the second).
     * @param byteArray
     *              the frame to be sent to the reader.
     */
    private void prepareFrameForSending(byte[] byteArray) {
        Log.i(TAG, "prepareFrameForSending started");
        mByteArrayList.clear();
        for (int i = 0; i < byteArray.length; i += 20) {
            byte[] obj = new byte[Math.min(20, byteArray.length - i)];
            System.arraycopy(byteArray, i, obj, 0, obj.length);
            this.mByteArrayList.add(obj);
            Log.d("UHF READER M01349", "BYE TO WRITE ON CHARACTERISTICS = " + Arrays.toString(mByteArrayList.get(0)));
        }
        writeByteArrayCharacteristic(mUHFBluetoothGattCallback.getBluetoothGattCharacteristic2());
    }


    /* Methods to perform several actions on the reader. */

    public void getBandArea(final RFID_Reader.GetAnswerByteCallback getBandAreaCallback) {
        m78a(UHFUtils.fillByteBuffer(GET_BAND_AREA_BYTE, new C0353b[0]), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | getBandArea");
                if (UHFException != null) {
                    getBandAreaCallback.onResult(UHFException.getErrorCode(), (byte) 0);
                } else {
                    getBandAreaCallback.onResult(0, frameData.getByteArray()[0]);
                }
            }
        });
    }

    public void getBattery(final RFID_Reader.GetAnswerIntCallback getBatteryCallback) {
        m78a(UHFUtils.fillByteBuffer(GET_BATTERY_BYTE, new C0353b[0]), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | getBattery");
                if (UHFException != null) {
                    getBatteryCallback.onResult(UHFException.getErrorCode(), 0);
                } else {
                    getBatteryCallback.onResult(0, frameData.getByteArray()[0]);
                }
            }
        });
    }

    public void getPower(final RFID_Reader.GetAnswerByteCallback getPowerCallback) {
        m78a(UHFUtils.fillByteBuffer(GET_POWER_BYTE, new C0353b[0]), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | getPower");
                if (UHFException != null) {
                    getPowerCallback.onResult(UHFException.getErrorCode(), (byte) 0);
                    return;
                }
                getPowerCallback.onResult(0, (byte) ((((frameData.getByteArray()[0] & 255) << 8) | (frameData.getByteArray()[1] & 255)) / 10));
            }
        });
    }

    public void getSerialNumber(final RFID_Reader.GetAnswerLongCallback getSerialNumberCallback) {
        m78a(UHFUtils.fillByteBuffer(GET_SERIAL_NUMBER_BYTE, new C0353b[0]), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | getSerialNumber");
                if (UHFException != null) {
                    getSerialNumberCallback.onResult(UHFException.getErrorCode(), 0);
                    return;
                }
                getSerialNumberCallback.onResult(0, ((long) (((((frameData.getByteArray()[0] & 255) << 24) | ((frameData.getByteArray()[1] & 255) << 16)) | ((frameData.getByteArray()[2] & 255) << 8)) | (frameData.getByteArray()[3] & 255))) & 4294967295L);
            }
        });
    }

    public void getVersion(final RFID_Reader.GetAnswerStringCallback getVersionCallback) {
        m78a(UHFUtils.fillByteBuffer(GET_VERSION_BYTE, UHFUtils.byteToC0353b((byte) 5)), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | getVersion");
                if (UHFException != null) {
                    getVersionCallback.onResult(UHFException.getErrorCode(), UHFException.getMessage());
                } else {
                    getVersionCallback.onResult(0, new String(frameData.getByteArray()).trim());
                }
            }
        });
    }

    public void setBandArea(final RFID_Reader.SetParameterCallback setBandAreaCallback, byte b) {
        m78a(UHFUtils.fillByteBuffer(SET_BAND_AREA_BYTE, UHFUtils.byteToC0353b(b)), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | setBandArea");
                if (UHFException != null) {
                    setBandAreaCallback.onResult(UHFException.getErrorCode());
                } else {
                    setBandAreaCallback.onResult(0);
                }
            }
        });
    }

    public void setBuzzer(final RFID_Reader.SetParameterCallback setBuzzerCallback, byte b) {
        m78a(UHFUtils.fillByteBuffer(SET_BUZZER_BYTE, UHFUtils.byteToC0353b(b)), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | setBuzzer");
                if (UHFException != null) {
                    setBuzzerCallback.onResult(UHFException.getErrorCode());
                } else {
                    setBuzzerCallback.onResult(0);
                }
            }
        });
    }

    public void setPower(final RFID_Reader.SetParameterCallback setPowerCallback, byte b) {
        m78a(UHFUtils.fillByteBuffer(SET_POWER_BYTE, UHFUtils.shortToC0353b((short) (b * 10))), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | setPower");
                if (UHFException != null) {
                    setPowerCallback.onResult(UHFException.getErrorCode());
                } else {
                    setPowerCallback.onResult(0);
                }
            }
        });
    }

    public void setReaderName(final RFID_Reader.SetParameterCallback setReaderNameCallback, byte[] bArr) {
        m78a(UHFUtils.fillByteBuffer(SET_READER_NAME_BYTE, UHFUtils.byteArrayToC0353b(bArr)), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | setReaderName");
                if (UHFException != null) {
                    setReaderNameCallback.onResult(UHFException.getErrorCode());
                } else {
                    setReaderNameCallback.onResult(0);
                }
            }
        });
    }

    public void writeTag(final RFID_Reader.SetParameterCallback writeTagCallback, byte[] bArr, byte[] bArr2, byte b, short s, short s2, byte[] bArr3) {
        m78a(UHFUtils.fillByteBuffer(WRITE_TAG_BYTE, UHFUtils.byteArrayToC0353b(bArr), UHFUtils.shortToC0353b((short) bArr2.length), UHFUtils.byteArrayToC0353b(bArr2), UHFUtils.byteToC0353b(b), UHFUtils.shortToC0353b(s), UHFUtils.shortToC0353b(s2), UHFUtils.byteArrayToC0353b(bArr3)), new communicationInterface() {

            /* renamed from: a */
            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                Log.i(TAG, "onAnswerReceived start | writeTag");
                if (UHFException != null) {
                    writeTagCallback.onResult(UHFException.getErrorCode());
                } else {
                    writeTagCallback.onResult(0);
                }
            }
        });
    }

    public boolean startTagPollingUHF() {
        mInterfaceToActivity.postRunnableToHandler(mInventoryRunnable);
        mInventoryRunnable.toggleIsInventorying(true);
        return true;


    }
    public void stopTagPollingUHF(boolean restart){
        mInterfaceToActivity.removeRunnableFromHandler(mInventoryRunnable);
        mInventoryRunnable.toggleIsInventorying(false);
        if(restart) {
            mInterfaceToActivity.postDelayedRunnableToHandler(new Runnable() {
                @Override
                public void run() {
                    startTagPollingUHF();
                }
            }, 5000);
        }
    }

    void singleInventory() {
        mFrameDataList = new ArrayList<>();
        //Log.d("UHF READER","START SINGLE INVENTORY");
        m78a(UHFUtils.fillByteBuffer(SINGLE_INVENTORY_BYTE, new C0353b[0]), new communicationInterface() {

            public void onAnswerReceived(ErrorCodes.UHFException UHFException, FrameData frameData) {
                //Log.i(TAG, "onAnswerReceived start | singleInventory");

                if (UHFException != null) {
                    mInventoryRunnable.responseRun(UHFException.getErrorCode(), null);

                } else {
                    //Log.i(TAG, "UHFException is null");
                    ArrayList<EpcReply> arrayList = new ArrayList<EpcReply>();
                    if (mFrameDataList != null && mFrameDataList.size() > 0) {
                        for (FrameData a : mFrameDataList) {
                            //Log.i(TAG, "Add to list" + a);
                            arrayList.add(UHFUtils.frameDataToEpcReply(a));
                        }
                    }
                    mInventoryRunnable.responseRun(0, arrayList);
                }
                //mFrameDataList = null; //Line uncommented in the app
            }
        });
    }


}
