package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;
import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.GeneralRFID.RFID_Reader;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.R;
import java.util.UUID;

/**
 * UHFBluetoothGattCallback is a class implementing the behaviours of a
 * BluetoothReaderGattCallback instance for a UHFReader object.
 */
public class UHFBluetoothGattCallback extends BluetoothGattCallback {

    /**
     * Activity object to represent the activity calling the NFCReader.
     */
    private ReadersActivity mReadersActivity;

    /**
     * Context object to represent the context in which the NFCReader is used.
     */
    private Context mContext;

    /**
     * Object to represent a UHF reader.
     */
    private UHFReader mUHFReader;

    /**
     * Variable to identify the reader, 1 for the first reader, 2 for the second reader.
     * Each UHFReader object has its own UHFBluetoothGattCallback object corresponding.
     */
    private int mDeviceNumber;

    /**
     * Variable to store the Gatt service of the Gatt profile of the reader.
     * @see BluetoothGattService
     */
    private BluetoothGattService mBluetoothGattService;

    /**
     * Variable to store the first characteristic of the Gatt profile of the reader.
     * @see BluetoothGattCharacteristic
     */

    private BluetoothGattCharacteristic mBluetoothGattCharacteristic1;
    /**
     * Variable to store the second characteristic of the Gatt profile of the reader.
     * @see BluetoothGattCharacteristic
     */
    private BluetoothGattCharacteristic mBluetoothGattCharacteristic2;

    /* UUIDs of the service and the characteristics we expect to discover from the gatt profile of
     * the device. */
    private static final UUID ACS_SERVICE_UUID = UUID.fromString("0000FFB0-0000-1000-8000-00805f9b34fb");
    private static final UUID CMD_LINE_CHARACTERISTIC_UUID = UUID.fromString("0000FFB1-0000-1000-8000-00805f9b34fb");
    private static final UUID DATA_LINE_CHARACTERISTIC_UUID = UUID.fromString("0000FFB2-0000-1000-8000-00805f9b34fb");

    public UHFBluetoothGattCallback(Activity activity, int deviceNumber, UHFReader uhfReader){
        mReadersActivity = (ReadersActivity) activity;
        mContext = (Context)activity;
        mDeviceNumber = deviceNumber;
        mUHFReader = uhfReader;
    }

    /**
     * Overwritten method of BluetoothGattCallback. Method called when the value of a gatt
     * characteristic of the device changes.
     * Displays the byte value written, and calls the writeByteArrayCharacteristic of the UFHReader
     * object to write the following frames on the characteristic.
     * @see UHFReader#writeByteArrayCharacteristic(BluetoothGattCharacteristic)
     * @see BluetoothGattCallback#onCharacteristicWrite(BluetoothGatt, BluetoothGattCharacteristic, int)
     */
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        final byte[] value = bluetoothGattCharacteristic.getValue();
        Log.i("ble", "onCharacteristicChange: <<- " + UHFUtils.binaryToHexWithSpace(value));
        mUHFReader.onFrameReceived(value);
    }

    /**
     * Overwritten method of BluetoothGattCallback. Method called when a new value is written on the
     * second gatt characteristic associated to the device.
     * Displays the byte value written, and calls the writeByteArrayCharacteristic of the UFHReader
     * object to write the following frames on the characteristic.
     * @see UHFReader#writeByteArrayCharacteristic(BluetoothGattCharacteristic)
     * @see BluetoothGattCallback#onCharacteristicWrite(BluetoothGatt, BluetoothGattCharacteristic, int)
     */
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, final int status) {
        final byte[] value = bluetoothGattCharacteristic.getValue();
        Log.i("ble", "on CharacteristicWrite:   ->> " + UHFUtils.binaryToHexWithSpace(value));
        mUHFReader.writeByteArrayCharacteristic(mBluetoothGattCharacteristic2);
    }

    /**
     * Overwritten method of the onConnectionStateChange method of BluetoothGattCallback.
     * When a change in the connection state between the device and the host (the smartphone here)
     * is detected, the UI is updated to display the new current connection state.
     * If the new state is "connected", then the discoverService method is called on the
     * BluetoothGatt object linked to this callback.
     * If the new state is "disconnected", then UHFReader is disconnected.
     * @param bluetoothGatt
     *                  the object representing the Gatt profile of the reader
     * @param status
     *             the state of the operation performed.
     * @param newState
     *              the new connection state
     * @see BluetoothGattCallback#onConnectionStateChange(BluetoothGatt, int, int)
     */
    public void onConnectionStateChange(final BluetoothGatt bluetoothGatt, final int status, final int newState) {
        Log.i("BLE GATT CALLBACK", "on connection state change");
        Log.d("BLE GATT CALLBACK", "Status = " +status + " new state = " + newState);
        mReadersActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (status == BluetoothGatt.GATT_SUCCESS) {  // Status of the connect or disconnect operation
                    if (newState == BluetoothGatt.STATE_CONNECTED) { // State of the Bluetooth Profile
                        mReadersActivity.updateConnectionState(newState, mDeviceNumber);
                        bluetoothGatt.discoverServices();
                        Log.d("BLUETOOTH GATT CALLBACK", "AFTER DISCOVERING SERVICES");
                        return;
                    } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                        mReadersActivity.updateConnectionState(newState, mDeviceNumber);
                        Log.d("BLUETOOTH GATT CALLBACK", "disconnection success of reader " +mDeviceNumber);
                        mReadersActivity.disconnectReader(mDeviceNumber);
                        return;
                    }
                }
                else { // if status != success
                    mReadersActivity.updateConnectionState(newState, mDeviceNumber);
                    if(newState == BluetoothProfile.STATE_CONNECTED)
                        GeneralUtils.toast(mContext.getApplicationContext(), mContext.getString(R.string.tts_connection_failure_nfc_reader,mDeviceNumber));
                    else if(newState == BluetoothProfile.STATE_DISCONNECTED){
                        GeneralUtils.toast(mContext.getApplicationContext(),
                                mContext.getString(R.string.tts_disconnection_nfc_reader,mDeviceNumber,mContext.getString(R.string.nfc_readers_reconnection),
                                        mContext.getString(R.string.nfc_reader_start_polling)));
                    }
                    return;
                }
            }
        });
    }

    /**
     * Overwritten method of the onServiceDiscovered method of BluetoothGattCallback.
     * Checks that the service UUID and the characteristic values are the values expected. If yes,
     * the enableNotification
     * @param bluetoothGatt
     *                  the object representing the Gatt profile of the reader
     * @param status
     *             the state of the service discovery operation performed.
     * @see BluetoothGattCallback#onServicesDiscovered(BluetoothGatt, int)
     */
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, final int status) {
        Log.i("UHF GATT CALLBACK", "on service discovered, look for service and characteristics");
        if (status != BluetoothGatt.GATT_SUCCESS)
            mReadersActivity.disconnectReader(mDeviceNumber);
        else {
            mBluetoothGattService = bluetoothGatt.getService(ACS_SERVICE_UUID);
            Log.d("UHF GATT CALLBACK", "LOOK FOR ACS SERVICE");
            if (mBluetoothGattService == null) {
                Log.d("UHF GATT CALLBACK", "ACS Service cannot be found");
            } else {
                mBluetoothGattCharacteristic1 = mBluetoothGattService.getCharacteristic(CMD_LINE_CHARACTERISTIC_UUID);
                Log.d("UHF GATT CALLBACK", "LOOK FOR CMD LINE CHARACTERISTIC");
                if (mBluetoothGattCharacteristic1 == null) {
                    Log.d("UHF GATT CALLBACK","CMD LINE Characteristic cannot be found");
                } else {
                    mBluetoothGattCharacteristic2 = mBluetoothGattService.getCharacteristic(DATA_LINE_CHARACTERISTIC_UUID);
                    Log.d("UHF GATT CALLBACK", "LOOK FOR DATA LINE CHARACTERISTIC");
                    if (mBluetoothGattCharacteristic2 == null) {
                        Log.d("UHF GATT CALLBACK","DATA LINE Characteristic cannot be found");
                    } else if (!mUHFReader.enableNotificationOnCharacteristic(mBluetoothGattCharacteristic1)) {
                        Log.d("UHF GATT CALLBACK","CMD LINE Characteristic set notification failed");
                    }
                }
            }
        }
    }

    /**
     * Overwritten method of BluetoothGattCallback. Method called when a new value is written on the
     * descriptor associated to the device, i.e. when the writeDescriptor method is called. This
     * method is only called in the UHFReader enableNotificationOnCharacteristic method.
     * Displays the byte value written, and calls the writeByteArrayCharacteristic of the UFHReader
     * object to write the following frames on the characteristic.
     * This method is called to times,
     * @see UHFReader#writeByteArrayCharacteristic(BluetoothGattCharacteristic)
     * @see BluetoothGattCallback#onCharacteristicWrite(BluetoothGatt, BluetoothGattCharacteristic, int)
     */
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, final BluetoothGattDescriptor bluetoothGattDescriptor, final int status) {
        Log.i("BLE GATT CALLBACK", "on descriptor write");
        if (status != 0) { // If the status is not GATT_SUCCESS
            Log.i("BLE GATT CALLBACK", "i != GATT_SUCCESS");
            mReadersActivity.disconnectReader(mDeviceNumber);
            /* If the characteristic this descriptor belongs to is mBluetoothGattCharacteristic1,
             * then enable also notification on the mBluetoothGattCharacteristic2 */
        } else if (bluetoothGattDescriptor.getCharacteristic().getUuid().equals(CMD_LINE_CHARACTERISTIC_UUID)) {
            mUHFReader.enableNotificationOnCharacteristic(mBluetoothGattCharacteristic2);
            /* If the characteristic this descriptor belongs to is mBluetoothGattCharacteristic2,
             * then the communication can start because both characteristics are enabled. */
        } else if (bluetoothGattDescriptor.getCharacteristic().getUuid().equals(DATA_LINE_CHARACTERISTIC_UUID)) {
            mUHFReader.setIsConnected(true);
            mUHFReader.setCommunicationInterface(null);
            Log.d("SmartLink", "smartLINK " + mDeviceNumber + " successfully connected!");
            /* Set the buzzer to 0 */
            mUHFReader.setBuzzer(new RFID_Reader.SetParameterCallback() {
                @Override
                public void onResult(int i) {
                    Log.d("SmartLink", "Buzzer set to 0 Reader " + mDeviceNumber);
                }
            }, (byte) 0);
        }
    }

    BluetoothGattCharacteristic getBluetoothGattCharacteristic2(){
        return mBluetoothGattCharacteristic2;
    }
}