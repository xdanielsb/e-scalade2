package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> HoldShapeActivity is the last activity of a series of 5 activities that allow the user
 * to customize the audio message he will hear during the climbing.</b>
 *<p> This activity allows him to choose or not to have an additional indication about next hold
 * size.
 *<p>
 * Once the user has made his choice by selecting one button (yes or no), the result is kept
 * in the singleton call DataKeeper.
 * @see DataKeeper#setHoldSize(Boolean)
 *</p>
 */

public class HoldSizeActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButtonOui;
    private Button mButtonNon;
    private TextView mTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hold_size);

        /* Layout */
        mButtonOui = (Button)findViewById(R.id.btnoui_hold_size_activity);
        mButtonNon = (Button)findViewById(R.id.btnnon_hold_size_activity);
        mTextView = (TextView)findViewById(R.id.text_hold_size_activity);
        mButtonOui.setOnClickListener(this);
        mButtonNon.setOnClickListener(this);
        mTextView.setText(getApplicationContext().getString(R.string.hold_activity_text,getApplicationContext().getString(R.string.holdSize)));
    }

    @Override
    public void onClick(View v) {
        Intent taillePriseIntent = new Intent(HoldSizeActivity.this, ValidationAudioActivity.class);
        switch (v.getId()) {
            case R.id.btnoui_hold_size_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setHoldSize(true);
                startActivity(taillePriseIntent);
                break;
            case R.id.btnnon_hold_size_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setHoldSize(false);
                startActivity(taillePriseIntent);
                break;
        }
    }
}
