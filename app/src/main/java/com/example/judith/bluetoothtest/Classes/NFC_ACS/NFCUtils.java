package com.example.judith.bluetoothtest.Classes.NFC_ACS;

import java.util.Locale;

/**
 * <b> NFCUtils is a static class that contains all the static methods related to the NFC readers
 * used in this app.</b>
 * <p>
 * These methods consist mostly in data conversion from a type to another.
 * </p>
 */

public class NFCUtils {

    /**
     * Private constructor to prevent class instantiation
     */
    private NFCUtils() {

    }

    /**
     * Creates a hexadecimal String representation of the byte array passed.
     * @param array
     * @return the string representation
     */
    /*Creates a hexadecimal String representation of the byte[] passed. */
    public static String toHexString(byte[] array) {

        String bufferString = "";

        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                String hexChar = Integer.toHexString(array[i] & 0xFF);
                if (hexChar.length() == 1) {
                    hexChar = "0" + hexChar;
                }
                bufferString += hexChar.toUpperCase(Locale.US) + " ";
            }
        }
        return bufferString;
    }

    /**
     * Checks that the byte entered is a hexadecimal value.
     * @param value
     * @return true if value is a hexadecimal value, false if not.
     */
    private static boolean isHexNumber(byte value) {
        if (!(value >= '0' && value <= '9') && !(value >= 'A' && value <= 'F')
                && !(value >= 'a' && value <= 'f')) {
            return false;
        }
        return true;
    }

    /**
     * Checks that String passed contains a hexadecimal value
     * @param string
     * @return true if string contains a hexadecimal value, false if not.
     */
    public static boolean isHexNumber(String string) {
        if (string == null)
            throw new NullPointerException("string was null");

        boolean flag = true;

        for (int i = 0; i < string.length(); i++) {
            char cc = string.charAt(i);
            if (!isHexNumber((byte) cc)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    private static byte uniteBytes(byte src0, byte src1) {
        byte _b0 = Byte.decode("0x" + new String(new byte[]{src0}));
        _b0 = (byte) (_b0 << 4);
        byte _b1 = Byte.decode("0x" + new String(new byte[]{src1}));
        byte ret = (byte) (_b0 ^ _b1);
        return ret;
    }

    /* Creates a byte[] representation of the hexadecimal String passed. */
    public static byte[] hexString2Bytes(String string) {
        if (string == null)
            throw new NullPointerException("string was null");

        int len = string.length();

        if (len == 0)
            return new byte[0];
        if (len % 2 == 1)
            throw new IllegalArgumentException(
                    "string length should be an even number");

        byte[] ret = new byte[len / 2];
        byte[] tmp = string.getBytes();

        for (int i = 0; i < len; i += 2) {
            if (!isHexNumber(tmp[i]) || !isHexNumber(tmp[i + 1])) {
                throw new NumberFormatException(
                        "string contained invalid value");
            }
            ret[i / 2] = uniteBytes(tmp[i], tmp[i + 1]);
        }
        return ret;
    }


    public static byte[] getTextInHexBytes(String editText) {

        String command = editText.replace(" ", "").replace("\n", "");

        if (command.isEmpty() || command.length() % 2 != 0
                || !isHexNumber(command)) {
            return null;
        }

        return hexString2Bytes(command);
    }
}
