package com.example.judith.bluetoothtest.Activities;

import android.graphics.Color;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Classes.DataSampleHashMap;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DataSample;
import com.example.judith.bluetoothtest.R;

import java.io.InputStream;

/**
 * <b> Activity to display the file that is going to be read during the climbing.</b>
 * <p>
 *     Table activity displays the lines of a CSV file chosen by the user in a readable way.
 *     It allows the user to check the content of the file before definitely validating his choice.
 *     The information displayed are, for each line (=each hold) :
 * <ul>
 *     <li> The number of the hold</li>
 *     <li> If the hold is in the predefined path of beginner mode, </li>
 *     <li> The coordinates on the x-axis and y-axis of the hold in centimeters, </li>
 *     <li> The distance to the next hold in centimeters,</li>
 *     <li> The direction to the next hold in degrees</li>
 *     <li> The size and shape of each hold, </li>
 * </ul>
 * </p>
 */


public class TableActivity extends AppCompatActivity{

    private TextView mTitre;
    private TextView mChosenMode;

    private DataSampleHashMap DataSampleList = new DataSampleHashMap();

    private static String TAG = TableActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_table);

        mTitre = (TextView)findViewById(R.id.text_file_selected_table_activity);
        mChosenMode = (TextView)findViewById(R.id.text_chosenMode_table_activity);

        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        Log.d(TAG, "Direction : " + DataKeeper.getInstance().getDirection());
        Log.d(TAG, "Distance : " + DataKeeper.getInstance().getDistance());
        Log.d(TAG, "PrecisionMain : " + DataKeeper.getInstance().getHandPrecision());
        Log.d(TAG, "Forme Prise : " + DataKeeper.getInstance().getHoldShape());
        Log.d(TAG, "Taille Prise : " + DataKeeper.getInstance().getHoldSize());


        /* Retrieves the csv file*/

        Uri uri = DataKeeper.getInstance().getUriLectureFile();
        Log.d(TAG, "URI = " + uri);
        InputStream inputStream;
        String fileName;

        if(uri.toString().equals("Default") || GeneralUtils.getFileName(uri, getApplicationContext()).equals("uri not valid")){
            fileName = "table.csv";
            inputStream = getApplicationContext().getResources().openRawResource(R.raw.table);
        }
        else{
            fileName = GeneralUtils.getFileName(uri,getApplicationContext());
            inputStream = GeneralUtils.getStreamFromUri(uri, getApplicationContext());
        }

        DataSampleList = GeneralUtils.readTableData(getApplicationContext(),inputStream);
        mTitre.setText(getString(R.string.display_selected_file,fileName));

        /* Layout */

        init();
    }

    /**
     * Displays the CSV file in a readable format for the user, i.e. in a TableLayout object.
     * Fills each TableRow objet of the TableLayout with a DataSampleList element. Then, sets
     * various parameters for the row, such as gravity, color, etc...
     * The first row is set differently because it reproduces the titles of the CSV file.
     */
    public void init(){
        TableLayout tableLayout = (TableLayout)findViewById(R.id.table_table_activity);

        TableRow titleRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.FILL_PARENT);
        titleRow.setLayoutParams(layoutParams);

        TextView title1 = new TextView(this);
        TextView title2 = new TextView(this);
        TextView title3 = new TextView(this);
        TextView title4 = new TextView(this);
        TextView title5 = new TextView(this);
        TextView title6 = new TextView(this);
        TextView title7 = new TextView(this);
        TextView title8 = new TextView(this);
        TextView title9 = new TextView(this);


        title1.setText(getApplicationContext().getString(R.string.table_title_tagID));
        title2.setText(getApplicationContext().getString(R.string.table_title_tagNumber));
        title3.setText(getApplicationContext().getString(R.string.table_title_part_of_path));
        title4.setText(getApplicationContext().getString(R.string.table_title_xAxis));
        title5.setText(getApplicationContext().getString(R.string.table_title_yAxis));
        title6.setText(getString(R.string.table_title_relative_distance));
        title7.setText(getString(R.string.table_title_relative_angle));
        title8.setText(getApplicationContext().getString(R.string.table_title_holdShape));
        title9.setText(getApplicationContext().getString(R.string.table_title_holdSize));

        title1.setGravity(11);
        title2.setGravity(11);
        title3.setGravity(11);
        title4.setGravity(11);
        title5.setGravity(11);
        title6.setGravity(11);
        title7.setGravity(11);
        title8.setGravity(11);
        title9.setGravity(11);

        title1.setPadding(5, 0, 5, 0);
        title2.setPadding(5, 0, 5, 0);
        title3.setPadding(5, 0, 5, 0);
        title4.setPadding(5, 0, 5, 0);
        title5.setPadding(5, 0, 5, 0);
        title6.setPadding(5, 0, 5, 0);
        title7.setPadding(5, 0, 5, 0);
        title8.setPadding(5, 0, 5, 0);
        title9.setPadding(5, 0, 5, 0);

        title1.setTextColor(Color.WHITE);
        title2.setTextColor(Color.WHITE);
        title3.setTextColor(Color.WHITE);
        title4.setTextColor(Color.WHITE);
        title5.setTextColor(Color.WHITE);
        title6.setTextColor(Color.WHITE);
        title7.setTextColor(Color.WHITE);
        title8.setTextColor(Color.WHITE);
        title9.setTextColor(Color.WHITE);

        titleRow.addView(title1);
        titleRow.addView(title2);
        titleRow.addView(title3);
        titleRow.addView(title4);
        titleRow.addView(title5);
        titleRow.addView(title6);
        titleRow.addView(title7);
        titleRow.addView(title8);
        titleRow.addView(title9);


        titleRow.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
        tableLayout.addView(titleRow,0);


        for(int i = 1; i<=DataSampleList.getSize(); i++){

            TableRow row = new TableRow(this);

            TableRow.LayoutParams rowLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.FILL_PARENT);
            rowLayoutParams.setMargins(0,1,0,1);

            TextView textView1 = new TextView(this);
            TextView textView2 = new TextView(this);
            TextView textView3 = new TextView(this);
            TextView textView4 = new TextView(this);
            TextView textView5 = new TextView(this);
            TextView textView6 = new TextView(this);
            TextView textView7 = new TextView(this);
            TextView textView8 = new TextView(this);
            TextView textView9 = new TextView(this);

            DataSample sample = DataSampleList.getDataSampleByIndex(i-1);

            textView1.setText(DataSampleList.getKeyByIndex(i-1));
            textView2.setText(String.valueOf(sample.getHoldNumber()));
            textView3.setText(sample.isTagInPath() ? "oui" : "non");
            textView4.setText(String.valueOf(sample.getxAbs()));
            textView5.setText(String.valueOf(sample.getyAbs()));
            textView6.setText(String.valueOf(sample.getDistanceQuantCM()));
            textView7.setText(String.valueOf(sample.getDirectionDegr()));
            textView8.setText(sample.getHoldShape());
            textView9.setText(sample.getHoldSize());

            textView1.setGravity(1);
            textView2.setGravity(1);
            textView3.setGravity(1);
            textView4.setGravity(1);
            textView5.setGravity(1);
            textView6.setGravity(1);
            textView7.setGravity(1);
            textView8.setGravity(1);
            textView9.setGravity(11);

            textView1.setPadding(5,3,5,3);
            textView2.setPadding(5,3,5,3);
            textView3.setPadding(5,3,5, 3);
            textView4.setPadding(5,3,5,3);
            textView5.setPadding(5,3,5,3);
            textView6.setPadding(5,3,5,3);
            textView7.setPadding(5,3,5,3);
            textView8.setPadding(5,3,5,3);
            textView9.setPadding(5,3,5,3);


            textView1.setLayoutParams(rowLayoutParams);
            textView2.setLayoutParams(rowLayoutParams);
            textView3.setLayoutParams(rowLayoutParams);
            textView4.setLayoutParams(rowLayoutParams);
            textView5.setLayoutParams(rowLayoutParams);
            textView6.setLayoutParams(rowLayoutParams);
            textView7.setLayoutParams(rowLayoutParams);
            textView8.setLayoutParams(rowLayoutParams);
            textView9.setLayoutParams(rowLayoutParams);


            textView1.setBackgroundColor(Color.WHITE);
            textView2.setBackgroundColor(Color.WHITE);
            textView3.setBackgroundColor(Color.WHITE);
            textView4.setBackgroundColor(Color.WHITE);
            textView5.setBackgroundColor(Color.WHITE);
            textView6.setBackgroundColor(Color.WHITE);
            textView7.setBackgroundColor(Color.WHITE);
            textView8.setBackgroundColor(Color.WHITE);
            textView9.setBackgroundColor(Color.WHITE);


            row.addView(textView1);
            row.addView(textView2);
            row.addView(textView3);
            row.addView(textView4);
            row.addView(textView5);
            row.addView(textView6);
            row.addView(textView7);
            row.addView(textView8);
            row.addView(textView9);


            tableLayout.addView(row, i);

        }
    }
}
