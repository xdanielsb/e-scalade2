package com.example.judith.bluetoothtest.Classes;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCReader;
import com.example.judith.bluetoothtest.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.TreeMap;

/**
 * <b> GeneralUtils is a static class that contains all the static methods used in this app not
 * related to a special type of reader.</b>
 * <p>
 * Among these operations can be found :
 * <ul>
 * <li> a method to display a toast message with default parameters. </li>
 * <li> a method to read a CSV file and store it into a LinkedHashMap of DataSample objects.</li>
 * <li> operations on inputStreams and uri of files.</li>
 * <li> mathematical operations to calculate hypotenuse, angle, etc...</li>
 * <li> conversion of data to be read intelligibly by the audio speaker</li>
 * <li> the method to read to the user an indication </li>
 * </ul>
 * </p>
 */

public class GeneralUtils {

    private static String TAG = GeneralUtils.class.getSimpleName();

    /**
     * Private constructor to prevent class instantiation
     */
    private GeneralUtils() {

    }

    /**
     * Displays a standardized toast message on the UI
     * @param context
     *              the context in which this method is called, to display the message on the
     *              right activity
     * @param string
     *              the message to be displayed
     */
    public static void toast(Context context, String string) {

        Toast toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER | Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    /**
     * Gets the name of file from it's uri passed. If the uri is not valid, i.e. is the
     * corresponding file name has been changed or deleted outside the app, the result is a specific
     * String and not a file name.
     * @param context
     *              the context in which the function is called
     * @param uri
     *          the Uri of the file
     * @return the name of the file in a String format, or a predefined String if the uri is not
     * valid.
     * @see com.example.judith.bluetoothtest.Activities.FileChoiceActivity
     * @see com.example.judith.bluetoothtest.Activities.RecordingFileChoiceActivity
     */
    public static String getFileName(Uri uri, Context context) {
        String result = null;
        Log.d("PRINT URI ", uri.toString() );
        if (uri.getScheme().equals("content")) {
            Cursor cursor;
            try {
                cursor = context.getContentResolver().query(uri, null, null, null, null);
            }catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "RETURN");
                return "uri not valid";
            }
            if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    cursor.close();
            }
            else{
                return "uri not valid";
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    /**
     * Converts the Uri passed into an inputStream. This inputStream is then what is required to
     * read the file's data.
     * @param context
     *              the context in which the function is called.
     * @param uri
     * @return null if the uri is null, otherwise an inputStream corresponding to the Uri object.
     * @see ReadersActivity
     * Other usages in AudioActivity, TableActivity and ValidationAudioActivity.
     */
    public static InputStream getStreamFromUri(Uri uri, Context context) {
        InputStream inputStream;
        if (uri != null && !uri.toString().equals("Default")) {
            Log.d(TAG, "URI NOT NULL AND NOT EQUAL TO DEFAULT");
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                Log.d("TABLE ACTIVITY", "INPT STEAM : " + inputStream.toString());
                return inputStream;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            Log.d(TAG, "URI NULL OR EQUAL TO DEFAULT");
            inputStream = context.getResources().openRawResource(R.raw.table);
            return inputStream;
        }
        return null;
    }


    public static File getPublicStorageDirectory(String directoryName) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, directoryName);
        if (!file.mkdirs()) {
            Log.d("UTILS", "Directory not created");
        }
        return file;
    }

    /**
     * Converts the double passed into an int rounded up. This value is needed by the audio speaker
     * which cannot read float or double values.
     * @param doubleToRoundUp
     *              the double to round up.
     * @return the double rounded up and casted into an int.
     * @see GeneralUtils#getAngleWithSinus(double, double, double)
     * @see GeneralUtils#getHypotenuse(double, double)
     */
    public static int roundDouble(double doubleToRoundUp){
        doubleToRoundUp = doubleToRoundUp*10;
        int intResult = (int)doubleToRoundUp;
        float floatResult = (float)intResult;
        floatResult = floatResult/10;
        if(floatResult - (int)floatResult >= 0.5)
            return (int)floatResult +1;
        else
            return (int)floatResult;
    }

    /**
     * Cuts an TreeMap containing Integers as key and DataSample as value to be the lenght specified
     * by the second parameter.
     * This function is used in the climbing mode - advanced.
     * @param treeMap
     *              the TreeMap to be truncated.
     * @param newSize
     *                  the desired length of the result.
     * @return the TreeMap passed as a parameter but with only its first newSize key-values.
     * @see NFCReader
     */
    public static TreeMap<Integer, DataSample> getFirstEntries (TreeMap<Integer, DataSample> treeMap, int newSize){
        Log.d(TAG, "SIZE = " + treeMap.size());
        int size = treeMap.size();
        Log.d(TAG, "CONT TOTAL = " + (treeMap.size()-newSize));
        for(int count = 1; count <= size-newSize; count++){
            treeMap.remove(treeMap.lastKey());
            Log.d(TAG, "COUNT = " +count);
        }
        return treeMap;
    }

    /**
     * Uses the Pythagorean theorem to calculate the hypotenuse of a rectangle triangle, with the
     * values of both other sides. Used to calculate the relative distance between to holds
     * knowing their absolute coordinates.
     * @param firstSide
     *              one side of the triangle, adjacent or opposite.
     * @param secondSide
     *              the second side of the triangle, adjacent or opposite.
     * @return the value of the hypothenuse rounded up and casted to an int with the method
     * roundDouble
     * @see GeneralUtils#roundDouble(double)
     * @see GeneralUtils#setRelativeDistancesAndAngles(DataSampleHashMap)
     * @see NFCReader
     */
    public static int getHypotenuse (double firstSide, double secondSide){
        double result;
        result = Math.sqrt((Math.abs(firstSide*firstSide)) + Math.abs(secondSide*secondSide));
        return roundDouble(result);
    }

    /**
     * Calculates the value of an angle in a rectangle triangle having the 3 sides of the triangle :
     * adjacent, opposite and the hypotenuse.
     * First calculates the sinus of the angle, and then uses the Math Android library to get the
     * arcsinus.
     * @param adjacentSide
     *              the adjacent side of the angle
     * @param oppositeSide
     *              the opposite side of the angle
     * @param hypotenuse
     *              the hypotenuse of the triangle
     * @return 90 degrees if the adjacent side length is inferior to 1, 0 if the hypotenuse is null,
     * and the angle value rounded up and casted into a int otherwise.
     * @see GeneralUtils#roundDouble(double)
     * @see GeneralUtils#setRelativeDistancesAndAngles(DataSampleHashMap)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    private static int getAngleWithSinus(double adjacentSide, double oppositeSide,double hypotenuse){
        double sinusAngle;
        double angleResult;
        if(Math.abs(adjacentSide) <1){
            angleResult = 90;
        }
        else{
            if(hypotenuse !=0){
                sinusAngle = (Math.abs(oppositeSide)/Math.abs(hypotenuse));
                angleResult = Math.asin(sinusAngle);
                angleResult = (angleResult*180)/Math.PI;
                if( adjacentSide < 0) // if the next hold is at the left, our angle will be > 90 degrees
                    angleResult = 180 - angleResult;
            }
            else {
                angleResult = 0;
            }
        }
        return roundDouble(angleResult);
    }

    /**
     * Returns a direction using clock reference from a positive angle passed. This direction is the
     * direction from one climbing hold to another. The angle is supposed to be positive because it
     * is assumed that each next hold is above the current one, however all the values possible
     * are treate because the author was motivated enough to do it (even if it might never be
     * useful).
     * @param angle
     *            the angle between to holds in degrees,referred to the horizontal axis.
     * @return an hour representing a direction in a clock reference.
     * @see DataSample#setDirectionHour(double)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static int positiveAngleToHour(double angle) {
        if (45 < angle && angle <= 75)
            return 1;
        else if (15 < angle && angle <= 45)
            return 2;
        else if (0 < angle && angle <= 15)
            return 3;
        else if (165 < angle && angle <= 180)
            return 9;
        else if (135 < angle && angle <= 165)
            return 10;
        else if (105 < angle && angle <= 135)
            return 11;
        else if (75 < angle && angle <= 105)
            return 12;
        else
            return -1;
    }

    /**
     * Returns a direction using cardinal points from a positive angle passed. This direction is the
     * direction from one climbing hold to another. The angle is positive to be positive because it
     * is assumed that each next hold is above the current one.
     * @param angle
     *            the angle between to holds in degrees, referred to the horizontal axis.
     * @param context
     *              the context in which this method is called, necessary because call of predefined
     *              String values stored in string.xml needs the context
     * @return a String containing a direction in cardinal points.
     * @see DataSample#setDirectionCard(double)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static String positiveAngleToCardinal(double angle, Context context) {
        if (0 <= angle && angle < 25)
            return context.getString(R.string.dataSample_E);
        else if (25 <= angle && angle < 70)
            return context.getString(R.string.dataSample_NE);
        else if (70 <= angle && angle < 110)
            return context.getString(R.string.dataSample_N);
        else if (110 <= angle && angle < 155)
            return context.getString(R.string.dataSample_NO);
        else if (155 <= angle && angle <= 180)
            return context.getString(R.string.dataSample_O);
        else
            return context.getString(R.string.dataSample_not_calculated);
    }

    /**
     * Returns a direction using sides of the climber (left, right, upper, etc...). This direction
     * is the direction from one climbing hold to another. The angle is positive because it is
     * assumed that each next hold is above the current one.
     * @param angle
     *            the angle between to holds in degrees, referred to the horizontal axis.
     * @param context
     *      *              the context in which this method is called, necessary because call of predefined
     *      *              String values stored in string.xml needs the context
     * @return a String containing a direction
     * @see DataSample#setDirectionSides(double)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static String positiveAngleToSide(double angle, Context context) {
        if (0 <= angle && angle < 25)
            return context.getString(R.string.dataSample_right);
        else if (25 <= angle && angle < 70)
            return context.getString(R.string.dataSample_upRight);
        else if (70 <= angle && angle < 110)
            return context.getString(R.string.dataSample_up);
        else if (110 <= angle && angle < 155)
            return context.getString(R.string.dataSample_upLeft);
        else if (155 <= angle && angle <= 180)
            return context.getString(R.string.dataSample_left);
        else
            return context.getString(R.string.dataSample_not_calculated);
    }

    /**
     * Returns a direction in degrees in a Sting format to be readable by the speaker. This
     * direction is the direction from one climbing hold to another. The angle is positive because
     * it is assumed that each next hold is above the current one.
     * @param angle
     *            the angle between to holds in degrees, referred to the horizontal axis.
     * @param context
     *               the context in which this method is called, necessary because call of predefined
     *               String values stored in string.xml needs the context
     * @return a String containing a direction in degrees
     * @see DataSample#setStringDirectionDegr(double) (double)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static String doubleToStringPositiveAngle(double angle, Context context) {
        if (0 <= angle && angle < 15)
            return context.getString(R.string.dataSample_0degresE);
        else if (15 <= angle && angle < 35)
            return context.getString(R.string.dataSample_30degreesE);
        else if (35 <= angle && angle < 55)
            return context.getString(R.string.dataSample_45degreesE);
        else if (55 <= angle && angle < 75)
            return context.getString(R.string.dataSample_70degreesE);
        else if (75 <= angle && angle < 105)
            return context.getString(R.string.dataSample_0degreesN);
        else if (105 <= angle && angle < 125)
            return context.getString(R.string.dataSample_70degreesO);
        else if (125 <= angle && angle < 145)
            return context.getString(R.string.dataSample_45degreesO);
        else if (145 <= angle && angle < 165)
            return context.getString(R.string.dataSample_30degreesO);
        else if (165 <= angle && angle <= 180)
            return context.getString(R.string.dataSample_0degreesO);
        return context.getString(R.string.dataSample_angle_unknown);
    }

    /**
     * Converts the distance in centimeters passed to a distance with quantitative qualifiers in
     * a String format to be readable by the speaker.This distance is the distance from one climbing
     * hold to another.
     * @param distanceCM
     *            the distance between the holds in centimeters
     * @param context
     *               the context in which this method is called, necessary because call of predefined
     *               String values stored in string.xml needs the context
     * @return a String containing a direction in degrees
     * @see DataSample#setDistanceQual(int)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static String distanceCMToQual(int distanceCM, Context context) {
        if (distanceCM < 50)
            return context.getString(R.string.dataSample_close);
        else if (distanceCM < 100)
            return context.getString(R.string.dataSample_midClose);
        else
            return context.getString(R.string.dataSample_midFar);
    }

    /**
     * Converts the distance in centimeters passed to a distance in meters casted to a String format
     * to be readable by the speaker. This distance is the distance from one climbing hold to
     * another.
     * @param distanceCM
     *            the distance between the holds in centimeters
     * @param context
     *               the context in which this method is called, necessary because call of predefined
     *               String values stored in string.xml needs the context
     * @return a String containing a direction in meters
     * @see DataSample#setDistanceQuantM(int)
     * @see GeneralUtils#audioLectureUtils(Context, DataSample, DataSample, TextToSpeech, boolean, int)
     */
    static String distanceCMToM(int distanceCM, Context context) {
        if (distanceCM < 50)
            return context.getString(R.string.dataSample_less_halfMeter);
        else if (distanceCM < 100)
            return context.getString(R.string.dataSample_less_meter);
        else
            return context.getString(R.string.dataSample_more_meter);
    }

    /**
     * Counts the number of lines in a CSV files and return this number as a result. This method is
     * used in the app to check the size of CSV files.
     * @param inputStream
     *            the inputStream associated with the CSV file
     * @return an int containing the number of lines
     * @see com.example.judith.bluetoothtest.Activities.FileChoiceActivity#onClick(View)
     * @see com.example.judith.bluetoothtest.Activities.RecordingFileChoiceActivity#onActivityResult(int, int, Intent)
     */
    public static int countTableLines(InputStream inputStream) {
        InputStreamReader readIn = new InputStreamReader(inputStream, Charset.forName("UTF-8"));

        BufferedReader reader = new BufferedReader(readIn);

        int count = 0;
        try {
            while (reader.readLine() != null) {
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "FILE COUNT = " + count);
        return count;
    }

    /**
     * Reads a CSV file and fills a LinkedHashMap<String,DataSample> DataSampleList with each line
     * corresponding to a DataSample object, its key being the values of the third column of the
     * file (the Tag IDs).
     * The first element of DataSampleList is set differently because it contains two more columns
     * than the other lines, filled with the size of the climber, and the number of holds to be
     * read simultaneously in advanced mode.
     * When done, calculates the relatives distances and angles between each element and the one
     * following it, but only in the beginner climbing mode. (Not necessary in the advanced mode).
     * This calculation is done thanks to the method
     * setRelativeDistancesAndAngles(DataSampleHashMap)
     * @param inputStream
     *            the inputStream associated with the CSV file
     * @param context
     *              the context in which this method is called, necessary to create each new
     *              DataSample object
     *
     * @return a DataSampleHashMap, which is a wrapper for a LinkedHashMap<String,DataSample>
     *     object. Each element of the DataSampleHashMap corresponds to a line of the CSV file
     *     (except the headers which are jumped).
     * @see ReadersActivity
     * Other usages in AudioActivity, TableActivity, ValidationAudioActivity
     * @see GeneralUtils#setRelativeDistancesAndAngles(DataSampleHashMap)
     */
    public static DataSampleHashMap readTableData(Context context, InputStream inputStream) {

        DataSampleHashMap DataSampleList = new DataSampleHashMap();

        /*Link between byte stream and character stream*/
        InputStreamReader readIn = new InputStreamReader(inputStream, Charset.forName("UTF-8"));

        /*Buffer to put the character stream*/
        BufferedReader reader = new BufferedReader(readIn);

        String line = "";
        try {
            reader.readLine();

            /*Reading firstLine to get the guiding mode*/

            String[] firstLineToken = reader.readLine().split(",");

            DataSample firstLine = new DataSample(context);
            firstLine.setHoldNumber(Integer.parseInt(firstLineToken[0]));
            firstLine.setIsTagInPath(true); // The first hold is by default part of the path.
            Log.d(TAG, "STRING UUID SIZE = " + firstLineToken[2].length());// The Tag UUID scanned has a size of 27, because there is one space at the end
            firstLine.setxAbs(Float.parseFloat(firstLineToken[3]));
            firstLine.setyAbs(Float.parseFloat(firstLineToken[4]));
            firstLine.setHandPrecision(firstLineToken[5]);
            firstLine.setHoldShape(firstLineToken[6]);
            firstLine.setHoldSize(firstLineToken[7]);
            if(firstLineToken[8] != null && !firstLineToken[8].isEmpty())
                DataKeeper.getInstance().setClimberSize(Float.parseFloat(firstLineToken[8]));
            else
                DataKeeper.getInstance().setClimberSize((float)1.70);
            DataSampleList.putDataSample(firstLineToken[2],firstLine);

            Log.d(TAG, "First line : " + firstLine.toString() + ", guiding mode " + DataKeeper.getInstance().isBeginnerMode());


            /*Loop to get one single line at one*/
            while ((line = reader.readLine()) != null) {
                // Split by commas

                String[] tokens = line.split(",");

                if(DataKeeper.getInstance().isBeginnerMode()){
                    if (Integer.parseInt(tokens[1]) == 1){ // If the hold is indicated as in the path
                        // Read the data
                        DataSample sample = new DataSample(context);
                        sample.setHoldNumber(Integer.parseInt(tokens[0]));
                        sample.setIsTagInPath(tokens[1].equals(String.valueOf(1)));
                        sample.setxAbs(Float.parseFloat(tokens[3]));
                        sample.setyAbs(Float.parseFloat(tokens[4]));
                        sample.setHandPrecision(tokens[5]);
                        sample.setHoldShape(tokens[6]);
                        sample.setHoldSize(tokens[7]);

                        DataSampleList.putDataSample(tokens[2],sample);

                        Log.d(TAG, "Just created : " + sample.toString());
                        Log.d(TAG, "Just created : " + tokens[2]);

                    }
                }
                else{
                    // Read the data
                    DataSample sample = new DataSample(context);
                    sample.setHoldNumber(Integer.parseInt(tokens[0]));
                    sample.setIsTagInPath(tokens[1].equals(String.valueOf(1)));
                    sample.setxAbs(Float.parseFloat(tokens[3]));
                    sample.setyAbs(Float.parseFloat(tokens[4]));
                    sample.setHandPrecision(tokens[5]);
                    sample.setHoldShape(tokens[6]);
                    sample.setHoldSize(tokens[7]);

                    DataSampleList.putDataSample(tokens[2],sample);

                    Log.d(TAG,  "Just created : " + sample.toString());
                    Log.d(TAG, "Just created : " + tokens[2]);
                }
            }
            Log.d(TAG, "TABLE FINISHED");
        } catch (IOException e) {
            /* error reading file*/
            Log.wtf(TAG, "Error in reading data in table on line" + line, e);
            e.printStackTrace();
        }
        /* count */
        Log.d(TAG, "DATASAMPLELIST SIZE = " + DataSampleList.getSize());
        setRelativeDistancesAndAngles(DataSampleList); // Set the relative distances from coordinates
        return DataSampleList;
    }


    /**
     * Calculates the relative distance between each hold of the path chosen by the  of
     * DataSampleList and the one following it,using their absolute coordinates.
     * Then adds this new result into DataSampleList which is the result of the method
     * readTableData(Context, InputStream). Does the same for the angles in degrees between each
     * element and the next one, still using their absolute coordinates.
     * These calculations are done using the mathematical method getHypotenuse(double, double)
     * and getAngleWithSinus(double, double, double).
     * Finally, sets the new hold numbers, from
     * @param DataSampleList
     *            the DataSampleHashMap objects to be completed ,already containing all the absolute
     *            coordinates of each element. Each element represents a hold in the climbing wall.
     * @see GeneralUtils#readTableData(Context, InputStream)
     * @see DataSampleHashMap
     * @see GeneralUtils#getAngleWithSinus(double, double, double)
     * @see GeneralUtils#getHypotenuse(double, double)
     */
    private static void setRelativeDistancesAndAngles(DataSampleHashMap DataSampleList) {
        Log.d(TAG, "Relatives Distance And Angles");
        for (int i = 0; i < DataSampleList.getHashMap().size() - 1; i++) {
            double xRelative = (double) (DataSampleList.getDataSampleByIndex(i + 1).getxAbs() - DataSampleList.getDataSampleByIndex(i).getxAbs());
            double yRelative = (double) (DataSampleList.getDataSampleByIndex(i + 1).getyAbs() - DataSampleList.getDataSampleByIndex(i).getyAbs());
            int relativeDistance = getHypotenuse(xRelative, yRelative);
            DataSampleList.getDataSampleByIndex(i).setDistanceQuantCM(relativeDistance);
            DataSampleList.getDataSampleByIndex(i).setDirectionDegr(getAngleWithSinus(xRelative, yRelative, relativeDistance));
        }
    }

    /**
     * Uses an TextToSpeech object to read a climbing indication to the user. This indications
     * differs according to the parameters retrieved in this method, such as the way of indicating
     * direction and distance, the level of precision about the hold, etc...
     * @param context
     *            the context in which this method is called, necessary because call of predefined
     *            String values stored in string.xml needs the context
     * @param sampleI
     *              a DataSample object representing the hold currently hold by the climber. Needed
     *              to have some characteristics, such as the distance and angle to the next hold and
     *              the hand actually holding the hold.
     * @param sampleJ
     *              a DataSample object representing the next hold to be caught by the climber.
     *              Needed to get some characteristics of the hold the climber is looking for, such
     *              as its size and shape.
     * @param mSpeaker
     *              The TextToSpeech object used to read the indication to the climber
     * @param beginnerMode
     *              a boolean indicating the climbing mode, beginner or advanced
     * @param hypotenuse the distance between sampleI and sampleJ. Needed to calculate the angle
     *                   between sampleI and sampleJ, only in advanced mode
     * @see GeneralUtils#getAngleWithSinus(double, double, double)
     * @see ReadersActivity
     * Other small usages in AudioActivity and ValidationAudioActivity to read examples of
     * indication to the user.
     */
    public static void audioLectureUtils(Context context, DataSample sampleI, DataSample sampleJ, TextToSpeech mSpeaker, boolean beginnerMode, int hypotenuse) {

        Log.d("UTILS", "I = " + sampleI + " , J = " + sampleJ);

        if (beginnerMode) {
            mSpeaker.speak(context.getString(R.string.tts_utils_nextHandle), TextToSpeech.QUEUE_ADD, null, null);
            if (DataKeeper.getInstance().getHandPrecision())
                mSpeaker.speak(sampleJ.getHandPrecision(), TextToSpeech.QUEUE_ADD, null, null);
        }

        /* If the hold is for a foot
        if (sampleI.getHandPrecision().equals("Pied gauche") || sampleI.getHandPrecision().equals("Pied droit")) {
            sampleI.setDirectionSides(sampleI.getDirectionDegr());
            sampleI.setDistanceFoot(sampleI.getDistanceQuantCM(),DataKeeper.getInstance().getClimberSize());
            mSpeaker.speak(context.getString(R.string.tts_utils_directionFoot,
                    sampleI.getDirectionSides(), sampleI.getDistanceFoot()), TextToSpeech.QUEUE_ADD, null, null);
        } else {*/

        double adjacentSide = sampleJ.getxAbs() - sampleI.getxAbs();
        Log.d("LECTURE", "XDistance = " + adjacentSide);
        double oppositeSide = sampleJ.getyAbs() - sampleI.getyAbs();
        Log.d("LECTURE", "YDistance = " + oppositeSide);
        Log.d("LECTURE", "DISTANCE = " + hypotenuse);
        int finalAngle = getAngleWithSinus(adjacentSide, oppositeSide, hypotenuse);
        Log.d("LECTURE", "ANGLE = " + finalAngle);


        switch (DataKeeper.getInstance().getDirection()) {
            case ("DirectionH"):

                sampleI.setDirectionHour(sampleI.getDirectionDegr());

                switch (DataKeeper.getInstance().getDistance()) {
                    case ("DistanceCM"):
                        if (beginnerMode) {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceCM,
                                    sampleI.getDirectionHour(), sampleI.getDistanceQuantCM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceCM,
                                    positiveAngleToHour(finalAngle), hypotenuse), TextToSpeech.QUEUE_ADD, null, null);
                        break;
                    case ("DistanceM"):
                        if (beginnerMode) {
                            sampleI.setDistanceQuantM(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceMQual,
                                    sampleI.getDirectionHour(), sampleI.getDistanceQuantM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceMQual,
                                    positiveAngleToHour(finalAngle), distanceCMToM(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        break;
                    case ("DistanceQual"):
                        if (beginnerMode) {
                            sampleI.setDistanceQual(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceMQual,
                                    sampleI.getDirectionHour(), sampleI.getDistanceQual()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionH_distanceMQual,
                                    positiveAngleToHour(finalAngle), distanceCMToQual(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                }
                break;
            case ("DirectionCard"):
                Log.d(TAG, "DIRECTION CARD");
                sampleI.setDirectionCard(sampleI.getDirectionDegr());
                switch (DataKeeper.getInstance().getDistance()) {
                    case ("DistanceCM"):
                        if (beginnerMode) {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    sampleI.getDirectionCard(), sampleI.getDistanceQuantCM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    positiveAngleToCardinal(finalAngle, context), hypotenuse), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                    case ("DistanceM"):
                        if (beginnerMode) {
                            sampleI.setDistanceQuantM(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getDirectionCard(), sampleI.getDistanceQuantM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    positiveAngleToCardinal(finalAngle, context), distanceCMToM(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                    case ("DistanceQual"):
                        if (beginnerMode) {
                            sampleI.setDistanceQual(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getDirectionCard(), sampleI.getDistanceQual()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            sampleI.setDistanceQual(hypotenuse);
                            sampleI.setDirectionCard(positiveAngleToHour(finalAngle));
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    positiveAngleToCardinal(finalAngle, context), distanceCMToQual(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                }
                break;
            case ("DirectionDegr"):
                Log.d(TAG, "DIRECTION DEGR");

                sampleI.setStringDirectionDegr(sampleI.getDirectionDegr());
                switch (DataKeeper.getInstance().getDistance()) {
                    case ("DistanceCM"):
                        if (beginnerMode) {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    sampleI.getStringDirectionDegr(), sampleI.getDistanceQuantCM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    doubleToStringPositiveAngle(finalAngle, context), hypotenuse), TextToSpeech.QUEUE_ADD, null, null);
                        }

                        break;
                    case ("DistanceM"):
                        if (beginnerMode) {
                            sampleI.setDistanceQuantM(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getStringDirectionDegr(), sampleI.getDistanceQuantM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    doubleToStringPositiveAngle(finalAngle, context), distanceCMToM(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }

                        break;
                    case ("DistanceQual"):
                        if (beginnerMode) {
                            sampleI.setDistanceQual(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getStringDirectionDegr(), sampleI.getDistanceQual()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    doubleToStringPositiveAngle(finalAngle, context), distanceCMToQual(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                }
                break;
            case ("DirectionCotes"):

                Log.d("UTILS", "DIRECTION COTES");
                sampleI.setDirectionSides(sampleI.getDirectionDegr());
                switch (DataKeeper.getInstance().getDistance()) {
                    case ("DistanceCM"):
                        if (beginnerMode) {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    sampleI.getDirectionSides(), sampleI.getDistanceQuantCM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceCM,
                                    positiveAngleToSide(finalAngle, context), hypotenuse), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                    case ("DistanceM"):
                        if (beginnerMode) {
                            sampleI.setDistanceQuantM(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getDirectionSides(), sampleI.getDistanceQuantM()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    positiveAngleToSide(finalAngle, context), distanceCMToM(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                    case ("DistanceQual"):
                        if (beginnerMode) {
                            sampleI.setDistanceQual(sampleI.getDistanceQuantCM());
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    sampleI.getDirectionSides(), sampleI.getDistanceQual()), TextToSpeech.QUEUE_ADD, null, null);
                        } else {
                            mSpeaker.speak(context.getString(R.string.tts_utils_directionCardDegrSide_distanceMQual,
                                    positiveAngleToSide(finalAngle, context), distanceCMToQual(hypotenuse, context)), TextToSpeech.QUEUE_ADD, null, null);
                        }
                        break;
                }
                break;
        }

        /*Precision of the hand (or foot) from which is indicated the next hold*/
        if(beginnerMode)
            mSpeaker.speak("de votre " + sampleI.getHandPrecision(), TextToSpeech.QUEUE_ADD, null, null);
        else
            mSpeaker.speak("de votre main", TextToSpeech.QUEUE_ADD, null, null);

        if (DataKeeper.getInstance().getHoldShape())
            mSpeaker.speak(context.getString(R.string.tts_utils_holdShape) + sampleJ.getHoldShape(), TextToSpeech.QUEUE_ADD, null, null);

        if (DataKeeper.getInstance().getHoldSize())
            mSpeaker.speak(context.getString(R.string.tts_utils_holdSize) + sampleJ.getHoldSize(), TextToSpeech.QUEUE_ADD, null, null);
    }

}
