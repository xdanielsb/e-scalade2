package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;

public final class UHFUtils {

    public static final byte AREA_CHINA1 = (byte) 81;
    public static final byte AREA_CHINA2 = (byte) 82;
    public static final byte AREA_EUROPE = (byte) 49;
    public static final byte AREA_JAPAN = (byte) 65;
    public static final byte AREA_KOREA = (byte) 17;
    public static final byte AREA_US1 = (byte) 33;
    public static final byte AREA_US2 = (byte) 34;
    public static final byte BANK_EPC = (byte) 1;
    public static final byte BANK_RFU = (byte) 0;
    public static final byte BANK_TID = (byte) 2;
    public static final byte BANK_USER = (byte) 3;
    public static final byte BUZZER_CLOSE = (byte) 0;
    public static final byte BUZZER_OPEN = (byte) 1;
    public static final byte INFO_VERSION = (byte) 5;
    public static final byte LIGHT_CLOSE = (byte) 0;
    public static final byte LIGHT_OPEN = (byte) 1;
    public static final byte LOCK_ACTION_LOCK = (byte) 1;
    public static final byte LOCK_ACTION_LOCK_PERM = (byte) 2;
    public static final byte LOCK_ACTION_UNLOCK = (byte) 0;
    public static final byte LOCK_OBJECT_ACC_PWD = (byte) 2;
    public static final byte LOCK_OBJECT_EPC = (byte) 0;
    public static final byte LOCK_OBJECT_KILL_PWD = (byte) 3;
    public static final byte LOCK_OBJECT_USER = (byte) 1;
    public static final byte POWER_MAX = (byte) 25;
    public static final byte POWER_MIN = (byte) 15;


    private static String hex2strWithSpace(byte[] bArr, String str) {
        StringBuilder stringBuilder = new StringBuilder();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuilder.append(String.format("%02X", new Object[]{Byte.valueOf(bArr[i])}));
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }

    static String hex2strWithSpace(byte[] bArr) {
        return hex2strWithSpace(bArr, " ");
    }


    static String binaryToHexWithSpace(byte[] bytes) {
        return UHFUtils.binaryToHexWithString(bytes, " ");
    }

    private static String binaryToHexWithString(byte[] bytes, String string) {
        StringBuilder stringBuilder = new StringBuilder();
        int length = bytes.length;
        for (int i = 0; i < length; i++) {
            stringBuilder.append(String.format("%02X", bytes[i]));
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    /**
     * Calculates the Received Signal Strength from a byteArray passed as an argument.
     * The byte array is an array of the last 4 bytes of the reader answer.
     * @return a float containing the RSSI from the tag detected.
     */
    private static float rssiCalculation(byte[] bArr) {
        /* make sure that the i values have the length of only one byte */
        int i = bArr[0] & 255;
        int i2 = bArr[1] & 255;
        int i3 = bArr[2] & 255;
        int i4 = bArr[3] & 255;
        try {
            double log10 = Math.log10((double) i) * 20.0d;
            double d = (double) i3;
            Double.isNaN(d);
            log10 = ((log10 - d) - 33.0d) - 30.0d;
            double log102 = Math.log10((double) i2) * 20.0d;
            double d2 = (double) i4;
            Double.isNaN(d2);
            log102 = ((log102 - d2) - 33.0d) - 30.0d;
            return new BigDecimal(Math.log10(Math.sqrt(Math.pow(Math.pow(10.0d, log10 / 20.0d), 2.0d) + Math.pow(Math.pow(10.0d, log102 / 20.0d), 2.0d))) * 20.0d).setScale(1, RoundingMode.UP).floatValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    static EpcReply frameDataToEpcReply(FrameData frameData) {
        byte[] epc = new byte[2];
        byte[] pc;
        if((frameData.getByteArray().length - 6) > 0)
            pc = new byte[((frameData.getByteArray().length) - 6)];
        else
            pc = new byte[0];
        byte[] byteArr = new byte[4];

        /* Copies to epc the first 2 bytes of the byte array contained in frameData*/
        System.arraycopy(frameData.getByteArray(), 0, epc, 0, epc.length);
        /* Copies to pc the bytes from the byte array of frameData, from the byte at rank 2 until
        * pc is filled.*/
        System.arraycopy(frameData.getByteArray(), epc.length, pc, 0, pc.length);
        /* Copies to byteArray the last 4 bytes of the byte array contained in frameData*/
        System.arraycopy(frameData.getByteArray(), frameData.getByteArray().length - byteArr.length, byteArr, 0, byteArr.length);
        return new EpcReply(epc, pc, rssiCalculation(byteArr));
    }

    /**
     * Creates a ByteBuffer and fills it with the data to be sent to the reader.
     * <ul>
     *     <li> Sets the size of the the buffer according to the parameters passed, </li>
     *     <li> Fills the buffer with -69 and 0 bytes, the byte b passed as a parameter and s, which
     *     is the sum of the sizes of each ByteArray for each element of the c0353bArray</li>
     *     <li> Then fills the ByteBuffer with each ByteArray previously mentionned.</li>
     *     <li> Finally puts formal bytes at the end of the ByteBuffer</li>
     * </ul>
     * @param b
     *       The byte corresponding to the name of the operation to be performed on the reader
     * @param c0353bArray
     *              An array of C0353b structures to contain additional information depending on
     *              the action to be be performed (see various called of fillByteBuffer at the
     *              bottom of UHF reader class to better understanding of this point).
     * @return the ByteBuffer filled casted to an array byte, i.e. the order or question to send to
     * the reader.
     */
    static byte[] fillByteBuffer(byte b, C0353b... c0353bArray) { //3 dots = zero or more C0353b are passed as an argument. If more than 0, it's an array
        int i = 0;
        short s = (short) 0;
        for (C0353b c035bIndex : c0353bArray) {
            s = (short) (s + c035bIndex.getByteArraySize());
            Log.d("UHF READER", "FILL BYTE BUFFER - BYTE ARR SIZE = " + c035bIndex.getByteArraySize());
        }
        ByteBuffer allocate = ByteBuffer.allocate(s + 8); //size of the byte buffer
        allocate.put((byte) -69);
        allocate.put((byte) 0);
        allocate.put(b);
        allocate.putShort(s);
        int length = c0353bArray.length;
        while (i < length) {
            allocate.put(c0353bArray[i].getByteArray());
            i++;
        }
        allocate.put((byte) 126);
        //Log.d("FILL BYTE BUFFER", "ALLOCATE.ARRAY = " + Arrays.toString(allocate.array()));
        //Log.d("FILL BYTE BUFFER", "M32RESULT = " + C0364g.m32a(allocate.array(),s+5));
        allocate.putShort(m32a(allocate.array(),s + 5));
        //Log.d("FILL BYTE BUFFER", "ALLOCATE ARRAY TO WRITE =" + Arrays.toString(allocate.array()));
        return allocate.array();
    }

    /**
     * Creates a C0353b data structure from a byte.
     */
    static C0353b byteToC0353b(final byte b) {
        return new C0353b() {

            public int getByteArraySize() {
                return 1;
            }

            /**
             * @return the byte passed as an argument to the constructor.
             */
            public byte[] getByteArray() {
                return new byte[]{b};
            }
        };
    }

    /**
     * Creates a C0353b data structure from a long.
     */
    public static C0353b longToC0353b(final long l) {
        return new C0353b() {
            public int getByteArraySize() {
                return 4;
            }

            /**
             * Creates a new byteArray, filled with the 4 LSB of the long passed as argument to the
             * constructor, starting from the left.
             * Then returns this byteArray
             * @return a byteArray of 4 bytes which are the 4 LSB of the long l.
             *
             */
            public byte[] getByteArray() {
                return new byte[]{(byte) ((int) (l >> 24)), (byte) ((int) (l >> 16)), (byte) ((int) (l >> 8)), (byte) ((int) l)};
            }
        };
    }

    /**
     * Creates a C0353b data structure from a short.
     */
    static C0353b shortToC0353b(final short s) {
        return new C0353b() {
            public int getByteArraySize() {
                return 2;
            }

            /**
             * Creates a new byteArray, filled with the 2 LSB of the short variable passed as an
             * argument to the constructor, starting from the left.
             * Then returns this byteArray.
             * @return a byteArray of 2 bytes which are the 2 bytes of the short s.
             *
             */
            public byte[] getByteArray() {
                return new byte[]{(byte) (s >> 8), (byte) s};
            }
        };
    }

    /**
     * Creates a C0353b data structure from a byteArray.
     */
    static C0353b byteArrayToC0353b(final byte[] byteArray) {
        return new C0353b() {

            public int getByteArraySize() {
                return byteArray.length;
            }

            /**
             * @return the byteArray given as an argument to the constructor.
             */
            public byte[] getByteArray() {
                return byteArray;
            }
        };
    }

    static short m32a(byte[] byteArray,int i2) {
        int i3 = 0;
        short s = (short) -1; //replace by 255 for more clarity ?
        while (i3 < i2) {
            short s2 = (short) (s ^ (byteArray[1 + i3] << 8));
            for (int i4 = 0; i4 < 8; i4++) {
                if((32768 & s2) != 0) //32768 = 1000 0000 0000 0000 -> 32768 & s2 !=0 == if the MSB of s2 is not 0 but 1
                    s2 = (short)((s2 << 1)^ 4129); //4129 = 00001 0000 0010 0001
                else
                    s2 = (short)(s2 <<1);
            }
            i3++;
            s = s2;
        }
        //Log.d("m23a", " short s  = " + s);
        return s;
    }
}
