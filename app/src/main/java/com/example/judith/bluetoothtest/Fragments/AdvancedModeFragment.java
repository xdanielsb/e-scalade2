package com.example.judith.bluetoothtest.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Button;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * A fragment used to display the UI view to allow the user to choose the number of holds to be
 * indicated in advanced mode.
 * @see Fragment
 */
public class AdvancedModeFragment extends Fragment implements View.OnClickListener{

    TextView mText;
    TextView mChosenMode;

    private onButtonPressed mOnButtonPressed;

    public interface onButtonPressed{
        void onNumberChosen();
    }

    /**
     * Required empty public constructor
     */
    public AdvancedModeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advanced_mode, container, false);

        Button mButton1;
        Button mButton2;
        Button mButton3;
        Button mButton4;

        mText = (TextView)view.findViewById(R.id.text_view_advanced_mode_fragment);
        mChosenMode = (TextView)view.findViewById(R.id.text_chosenMode_advanced_fragment);
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mText.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED);
            }
        },100);

        mButton1 = (Button)view.findViewById(R.id.btn_1_hold_advanced_mode_fragment);
        mButton2 = (Button)view.findViewById(R.id.btn_2_hold_advanced_mode_fragment);
        mButton3 = (Button)view.findViewById(R.id.btn_3_hold_advanced_mode_fragment);
        mButton4 = (Button)view.findViewById(R.id.btn_4_hold_advanced_mode_fragment);

        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mButton3.setOnClickListener(this);
        mButton4.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;

        try {
            mOnButtonPressed = (onButtonPressed) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    /**
     * Sets the number of holds chosen, depending on the button pressed. This number of holds is
     * stored in DataKeeper
     * @see DataKeeper#setHoldNumberAdvancedMode(int)
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_1_hold_advanced_mode_fragment:
                DataKeeper.getInstance().setHoldNumberAdvancedMode(1);
                mOnButtonPressed.onNumberChosen();
                break;

            case R.id.btn_2_hold_advanced_mode_fragment:
                DataKeeper.getInstance().setHoldNumberAdvancedMode(2);
                mOnButtonPressed.onNumberChosen();
                break;

            case R.id.btn_3_hold_advanced_mode_fragment:
                DataKeeper.getInstance().setHoldNumberAdvancedMode(3);
                mOnButtonPressed.onNumberChosen();
                break;

            case R.id.btn_4_hold_advanced_mode_fragment:
                DataKeeper.getInstance().setHoldNumberAdvancedMode(4);
                mOnButtonPressed.onNumberChosen();
                break;
        }
    }
}
