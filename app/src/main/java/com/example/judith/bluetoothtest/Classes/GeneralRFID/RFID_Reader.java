package com.example.judith.bluetoothtest.Classes.GeneralRFID;


import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;

import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCReader;
import com.example.judith.bluetoothtest.Classes.UHF_SLS.UHFReader;

/**
 * RFID_Reader is an interface to define general methods to be used by every RFID reader that might
 * be added to the app.
 * These methods declared here are the most generic possible, so they can be declined in several
 * versions for each reader. They are implemented by the RFIDBluetoothReader class.
 */

public interface RFID_Reader {

    /**
     * Starts the tag's polling for the RFID Reader.
     * @return true if success.
     */
    boolean startPolling();

    /**
     * Stops the tag's polling for the RFID Reader.
     * @return true if success.
     */
    boolean stopPolling();

    /**
     * Authenticates the reader
     * @param authenticationKey the authentication key in a byte array format
     */
    boolean authenticateReader(byte[] authenticationKey);

    /**
     * Checks if the reader object is null.
     * @return true if the reader is null
     */
    boolean isReaderNull();

    /**
     * Enable notifications for the reader. Method only necessary for the NFC reader so far.
     */
    void enableNotification(boolean enable);

    /**
     * Get the NFC reader instance.
     * @return the NFC reader instance wrapped in the RFIDBluetoothReader
     */
    NFCReader getNFCReader(); // add a get method for your new reader with the reader's type.

    /**
     * Get the UHF reader instance.
     * @return the UHF reader instance wrapped in the RFIDBluetoothReader
     */
    UHFReader getUHFReader();

    /**
     * Sets the BluetoothGatt instance and the BluetoothGattCallback instance linked to the
     * RFIDBluetoothReader instance.
     * @param bluetoothGatt
     *                  the BluetoothGatt instance to be linked
     * @param bluetoothGattCallback
     *                  the BluetoothGattCallback instance to be linked
     */
    void setGattAndCallback(BluetoothGatt bluetoothGatt, BluetoothGattCallback bluetoothGattCallback);

    /**
     * Interface for the callback of an operation when no answer is expected from the reader.
     */
    interface SetParameterCallback{
        void onResult(int errorCode);
    }

    /**
     * Interface for the callback of an operation when a byte answer is expected from the reader.
     */
    interface GetAnswerByteCallback{
        void onResult(int i, byte b);
    }

    /**
     * Interface for the callback of an operation when an int answer is expected from the reader.
     */
    interface GetAnswerIntCallback{
        void onResult(int i, int i1);
    }

    /**
     * Interface for the callback of an operation when a long answer is expected from the reader.
     */
    interface GetAnswerLongCallback{
        void onResult(int i, long l);
    }

    /**
     * Interface for the callback of an operation when a String answer is expected from the reader.
     */
    interface GetAnswerStringCallback{
        void onResult(int i, String s);
    }

}
