package com.example.judith.bluetoothtest.Activities;

import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import androidx.appcompat.app.AppCompatActivity;

/**
 * <b> BluetoothActivity is the activity in charge of the Bluetooth scanning.</b>
 *<p> This activity uses a BluetoothLeScanner object to perform scan related operation :
 *<ul>
 *     <il> scanning for Bluetooth devices around the phone, </il>
 *     <il>performing callbacks when a device is found.</il>
 *</ul>
 *</p>
 * <p> The object found are of type BluetoothDevice </p>Once the user has made his choice by selecting one button (yes or no), the result is kept
 * in the singleton call DataKeeper.
 * @see DataKeeper#setHoldShape(boolean)
 *</p>
 */



public class BluetoothActivity extends AppCompatActivity implements View.OnClickListener{

    /* API related declaration */

    private BluetoothAdapter mBluetoothAdapter; // Instance representing the local device adapter for BLE communication
    private BluetoothLeScanner mBluetoothLeScanner; // Instance to perform scan related operation


    /* Layout related declarations */

    private TextView mChosenMode;

    private Button mButtonScan;
    private Button mButtonConnect;
    private ListView mListView;
    private DeviceAdapter mDeviceAdapter;

    /* Functional declarations */

    private Handler mHandler;
    public static final int REQUEST_ENABLE_BT = 42; // Must be defined and greater than 0
    private boolean mScanning = false;
    private static final long SCAN_PERIOD = 3000;
    private static final String TAG = BluetoothActivity.class.getSimpleName();

    /* Queue to store devices found and parameters */

    private List<BluetoothDevice> mScanningQueue = new ArrayList<BluetoothDevice>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        /* Creation of the layout's fix components*/

        mChosenMode = (TextView)findViewById(R.id.text_chosenMode_ble_activity);
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        mButtonScan = (Button)findViewById(R.id.btn_scan_bluetooth_activity);
        mButtonConnect = (Button)findViewById(R.id.btn_connect_ble_activity);
        mButtonScan.setOnClickListener(this);
        mButtonConnect.setOnClickListener(this);

        mHandler = new Handler();

        final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        /*Check if BLE is supported by the device*/

        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)){
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_noBLE));
            finish();
        }

        /* Set the List adapter to display a list of found devices */

        mListView = (ListView)findViewById(R.id.list_view_ble_activity);
        mDeviceAdapter = new DeviceAdapter(BluetoothActivity.this,mScanningQueue);
        mListView.setAdapter(mDeviceAdapter);
        mListView.setItemsCanFocus(false);
    }

    @Override
    protected void onResume(){
        super.onResume();

        /*Checks that Bluetooth is enabled on the device and if not, displays a dialog asking the user to enable it */

        if(!mBluetoothAdapter.isEnabled()){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }


    /**
     * Receives the result of the action performed by the BluetoothAdapter to ask the user to
     * activate or allow Bluetooth on the phone.
     * If the user refused to allow Bluetooth, the application is closed by finish();
     * @param requestCode
     *                  the integer request code originally supplied to startActivityForResult,to
     *                  identify from where the result is coming from
     * @param resultCode
     *                  the integer result code returned.
     * @param data
     *          an Intent object, sometimes carrying extra information to the caller.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED){
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_scan_bluetooth_activity:
                /* Start scanning if scan button pressed */
                if (!mScanning && mBluetoothAdapter.isEnabled()) {
                    mScanningQueue.clear();
                    startScanning(true);
                }
                break;
            case R.id.btn_connect_ble_activity:
                if (DataKeeper.getInstance().isClimbingMode()) {
                    if (mScanningQueue.size() >= 2) {
                        final Intent uhfIntent = new Intent(this, ReadersActivity.class);
                        uhfIntent.putExtra(ReadersActivity.EXTRAS_DEVICE1_NAME, mScanningQueue.get(0).getName());
                        uhfIntent.putExtra(ReadersActivity.EXTRAS_DEVICE2_NAME, mScanningQueue.get(1).getName());
                        uhfIntent.putExtra(ReadersActivity.EXTRAS_DEVICE1_ADDRESS, mScanningQueue.get(0).getAddress());
                        uhfIntent.putExtra(ReadersActivity.EXTRAS_DEVICE2_ADDRESS, mScanningQueue.get(1).getAddress());
                        startActivity(uhfIntent);

                    } else {
                        GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_notEnough_device, String.valueOf(2)));
                    }
                    break;
                } else {
                    if (mScanningQueue.size() >= 1) {
                        Uri uriRecordingFile = DataKeeper.getInstance().getUriRecordingFile();
                        if (uriRecordingFile == null) {
                            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_no_recording_file));
                        } else {
                            /* Only one device is required for this */
                            final Intent oneReaderIntent = new Intent(this, ReadersActivity.class);
                            oneReaderIntent.putExtra(ReadersActivity.EXTRAS_DEVICE1_NAME, mScanningQueue.get(0).getName());
                            oneReaderIntent.putExtra(ReadersActivity.EXTRAS_DEVICE1_ADDRESS, mScanningQueue.get(0).getAddress());
                            startActivity(oneReaderIntent);
                        }
                    } else {
                        GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_notEnough_device, String.valueOf(1)));
                    }
                    break;
                }
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(mBluetoothAdapter.isEnabled()) {
            startScanning(false);
        }
    }

    /**
     * <p>
     *     Starts the scan. The scan is called by the function startScan (line 238) from the
     *     BluetoothLeScanner object.Filters are added to the startScan method to get only the
     *     devices wanted. Here, to select the devices the BLE services UUID  are used. These UUID
     *     are known for the 2 brands of reader that we are trying to find, so the scanner will
     *     only find these brands while scanning.
     * </p>
     * <p>
     *     After a period of SCAN_PERIOD (3 seconds here), the scan is stopped thanks to a delayed
     *     Handler object.
     * </p>
     * <p>
     *     When a BluetoothDevice with a corresponding Service UUID is found by the scanner, the
     *     ScanCallback is called.
     * </p>
     * @param enable
     *             true to start the scan, false to stop it.
     */
    private synchronized void startScanning(final boolean enable){
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        if(enable && !mScanning){
            /* Filters to only get the devices wanted when scanning*/
            List<ScanFilter> scanFilters = new ArrayList<>();
            final ScanSettings settings = new ScanSettings.Builder().build();
            ParcelUuid NFC_UuidService = new ParcelUuid(UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb"));
            ParcelUuid UHF_UuidService1 = new ParcelUuid(UUID.fromString("0000ffb0-0000-1000-8000-00805f9b34fb"));
            ParcelUuid UHF_UuidService2 = new ParcelUuid(UUID.fromString("0000fee7-0000-1000-8000-00805f9b34fb"));

            ScanFilter filter1 = new ScanFilter.Builder().setServiceUuid(NFC_UuidService).build();
            ScanFilter filter2 = new ScanFilter.Builder().setServiceUuid(UHF_UuidService1).build();
            ScanFilter filter3 = new ScanFilter.Builder().setServiceUuid(UHF_UuidService2).build();
            scanFilters.add(filter1);
            scanFilters.add(filter2);
            scanFilters.add(filter3);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothLeScanner.stopScan(mScanCallback);
                    GeneralUtils.toast(getApplicationContext(), "Scan terminé !");
                    for (int i=0; i<mScanningQueue.size(); i++){
                        Log.d(TAG, "MSCANNINGQUEUE UUID : " + i + " " + Arrays.toString(mScanningQueue.get(i).getUuids()));
                    }
                }
            },SCAN_PERIOD);

            mScanning = true;
            GeneralUtils.toast(this.getApplication(),getString(R.string.toast_begin_scanBLE));
            mBluetoothLeScanner.startScan(scanFilters,settings,mScanCallback);
        }
        else{
            mScanning = false;
            if(mBluetoothAdapter.isEnabled()) {
                mBluetoothLeScanner.stopScan(mScanCallback);
            }
        }
    }

    /**
     * Called when a Bluetooth device is found. This device is represented by a BluetoothDevice
     * instance, and associated with the ScanResult result with the result.getDevice() method.
     * When found, the device is added to the DeviceAdapter that handles the ListView displaying
     * all the results.
     */
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice mBluetoothDevice = result.getDevice();
            Log.d(TAG, "Device found : " + mBluetoothDevice.getName());
            mDeviceAdapter.addDevice(mBluetoothDevice);
            mDeviceAdapter.notifyDataSetChanged();

            Log.d(TAG, "SIZE : " + mScanningQueue.size());
            for (int i = 0; i < mScanningQueue.size(); i++)
                Log.d(TAG, "CONNECTIONQUEUE ELEM " + mScanningQueue.get(i).getName());
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_scan_failure));
        }
    };

    /**
     * <b>Adapter object to display a list of all the scan results.</b>
     * <p>
     *     This adapter contains a list of viewHolder object, one for each BluetoothDevice,
     *     containing its name and mac address.
     * </p>
     * <p>
     *     This adapter also uses the addDevice method to add each new device found in the
     *     List<BluetoothDevice> mScanningQueue, the list attached to the listView.
     * </p>
     */
    /* Adapter to handle the list view */

    public class DeviceAdapter extends ArrayAdapter<BluetoothDevice>{

        private DeviceAdapter(Context context, List<BluetoothDevice> bluetoothDeviceList){
            super(context, 0, bluetoothDeviceList);
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(view == null){
                view = inflater.inflate(R.layout.row_scan_result,viewGroup, false);
            }

            /* A view holder is a controller that holds the parameters of a view */

            DeviceViewHolder viewHolder = (DeviceViewHolder) view.getTag();

            final BluetoothDevice bluetoothDevice = getItem(position);

            if(viewHolder == null){
                viewHolder = new DeviceViewHolder();
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.adress);
                view.setTag(viewHolder);
            }

            final String deviceName = bluetoothDevice.getName();
            if(deviceName != null && deviceName.length()>0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(getString(R.string.unknown_device));
            viewHolder.deviceAddress.setText(bluetoothDevice.getAddress());

            return view;
        }

        private void addDevice(BluetoothDevice device) {
            /* If the device is not already found */
            if (!mScanningQueue.contains(device)) {
                Log.d(TAG, "doesnt contain device");
                mScanningQueue.add(device);
            }

            Log.d(TAG, "after adding " + mScanningQueue);

        }

    }

    static class DeviceViewHolder{
        TextView deviceName;
        TextView deviceAddress;
    }
}
