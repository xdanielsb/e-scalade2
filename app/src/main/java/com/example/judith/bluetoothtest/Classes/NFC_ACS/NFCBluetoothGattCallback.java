package com.example.judith.bluetoothtest.Classes.NFC_ACS;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import com.acs.bluetooth.BluetoothReader;
import com.acs.bluetooth.BluetoothReaderGattCallback;
import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.R;

/**
 * NFCBluetoothManager is a wrapper class of a BluetoothReaderGattCallback instance from the
 * ACS API. It implements the methods of this class and its behaviour.
 */
public class NFCBluetoothGattCallback extends BluetoothGattCallback {

    /**
     * Context object to represent the context in which the NFCReader is used.
     */
    private Context mContext;

    /**
     * Activity object to represent the activity calling the NFCReader.
     */
    private ReadersActivity mReadersActivity;

    /**
     * Object to represent the Gatt Callback of a NFC Bluetooth reader. Obtained from the ACS API.
     * @see BluetoothReaderGattCallback
     */
    private BluetoothReaderGattCallback mBluetoothReaderGattCallback;


    /**
     * Variable to identify the reader, 1 for the first reader, 2 for the second reader.
     * Each BluetoothReader object has its own BluetoothReaderGattCallback object corresponding.
     */
    private int mDeviceNumber;


    public NFCBluetoothGattCallback(Activity activity, int deviceNumber, final NFCBluetoothManager nfcBluetoothManager) {
        mContext = (Context) activity;
        mDeviceNumber = deviceNumber;
        mReadersActivity = (ReadersActivity) activity;
        mBluetoothReaderGattCallback = new BluetoothReaderGattCallback();
        Log.d("ReadersActivity","CREATION OF NFC GATT CALLBACK " + mDeviceNumber);
        mBluetoothReaderGattCallback.setOnConnectionStateChangeListener(new BluetoothReaderGattCallback.OnConnectionStateChangeListener() {

            /**
             * Overwritten method of the onConnectionStateChange method of the
             * BluetoothReaderGattCallback object.
             * When a change in the connection state between the device and the host (the smartphone
             * here) is detected, the UI is updated to display the new current connection state.
             * If the new state is "connected", then the detectReader method is called on the
             * BluetoothReaderManager.
             * If the new state is "disconnected", then the manager is set to null and the Gatt
             * profile is closed.
             * @param bluetoothGatt
             *                  the object representing the Gatt profile of the reader
             * @param state
             *          the state of the operation performed.
             * @param newState
             *              the new connection state
             */
            @Override
            public void onConnectionStateChange(final BluetoothGatt bluetoothGatt, final int state, final int newState) {
                Log.d("ReadersActivity","ON CONNECTION STATE CHANGED : " + mDeviceNumber);
                mReadersActivity.runOnUiThread((new Runnable() { // Only the main Thread can modify the layout
                    @Override
                    public void run() {
                        if (state != BluetoothGatt.GATT_SUCCESS) {
                            if (newState == BluetoothReader.STATE_CONNECTED) {
                                //mReadersActivity.updateConnectionState(newState,mDeviceNumber); don't update when failed.
                                GeneralUtils.toast(mContext.getApplicationContext(), mContext.getString(R.string.tts_connection_failure_nfc_reader,mDeviceNumber));
                            } else if (newState == BluetoothReader.STATE_DISCONNECTED) {
                                //mReadersActivity.updateConnectionState(newState, mDeviceNumber);
                                GeneralUtils.toast(mContext.getApplicationContext(),mContext.getString(R.string.tts_disconnection_nfc_reader,mDeviceNumber,
                                        mContext.getString(R.string.nfc_readers_reconnection),mContext.getString(R.string.nfc_reader_start_polling)));
                            }
                            return;
                        }
                        /*Show the message on fail to connect/disconnect.*/
                        mReadersActivity.updateConnectionState(newState, mDeviceNumber);
                        Log.d("ReadersActivity","NEW STATE = " +newState);
                        Log.d("ReadersActivity","BLUETOOTH MANAGER = " + nfcBluetoothManager.getBluetoothReaderManager() + " " + mDeviceNumber);

                        if (newState == BluetoothProfile.STATE_CONNECTED) {
                            /* Detect the connected reader and trigger the callback function */
                            if (nfcBluetoothManager.getBluetoothReaderManager() != null) {
                                Log.d("ReadersActivity", "DETECT CALLED " + mDeviceNumber);
                                nfcBluetoothManager.getBluetoothReaderManager().detectReader(bluetoothGatt,mBluetoothReaderGattCallback);
                                GeneralUtils.toast(mContext, mContext.getString(R.string.tts_nfc_reader_connected, mDeviceNumber));
                            }
                        } else if (newState == BluetoothProfile.STATE_DISCONNECTED){
                            nfcBluetoothManager.setBluetoothReaderManager(null);
                            if(bluetoothGatt !=null){
                                bluetoothGatt.close();
                            }
                        }
                    }
                }));

            }
        });
    }

    /**
     * Returns the BluetoothReaderGattCallback instance wrapped in this instance.
     * @return mBluetoothReaderGattCallback
     * @see ReadersActivity
     */
    public BluetoothReaderGattCallback getBluetoothReaderGattCallback(){
        return this.mBluetoothReaderGattCallback;
    }
}