package com.example.judith.bluetoothtest.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DataSample;
import com.example.judith.bluetoothtest.Classes.DataSampleHashMap;
import com.example.judith.bluetoothtest.R;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;

import java.io.InputStream;


/**
 * <b> ValidationAudioActivity allows the user to validate his customization of the audio message.
 * </b>
 * <p>
 *     This activity comes after the series of 5 activities that allow the user to customize the
 *     audio message he will hear during the climbing.
 *     In this activity, the user can listen his customized message as many times as wanted with the
 *     TextToSpeech object, and validate or invalidate the message.
 * </p>
 * <p>
 *     ValidationAudioActivity retrieves the user choices from DataKeeper, then read the audio
 *     message with these choices.
 *     If the user validates the message, the activity saves the choices in the app preferences
 *     thanks to a SharedPreferences object. Thus, the next time the app will be launched, these
 *     preferences will be retrieved from the SharedPreferences and the user won't have to complete
 *     this customization again.
 *     If the user invalidates the message, he gets to restart the whole customization process.
 *</p>
 * @see DataKeeper
 */

public class ValidationAudioActivity extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener {


    private String mDirection;
    private String mDistance;
    private boolean mHandPrecision;
    private boolean mHandleShape;
    private boolean mHandleSize;

    private Button mButtonYes;
    private Button mButtonNo;
    private Button mButtonRepeat;

    private Uri mUri;

    private final int SHORT_DURATION = 1000;
    private TextToSpeech mSpeaker;

    private DataSampleHashMap DataSampleList = new DataSampleHashMap();
    private DataSample mDataSample;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation_audio);

        mDirection = DataKeeper.getInstance().getDirection();
        mDistance = DataKeeper.getInstance().getDistance();
        mHandPrecision = DataKeeper.getInstance().getHandPrecision();
        mHandleShape = DataKeeper.getInstance().getHoldShape();
        mHandleSize = DataKeeper.getInstance().getHoldSize();


        Log.d("VALIDATION ACTIVITY","DATA = " + mDirection);
        Log.d("VALIDATION ACTIVITY","DATA = " + mDistance);
        Log.d("VALIDATION ACTIVITY","DATA = " + mHandPrecision);
        Log.d("VALIDATION ACTIVITY","DATA = " + mHandleShape);
        Log.d("VALIDATION ACTIVITY","DATA = " + mHandleSize);


        mButtonYes = (Button)findViewById(R.id.btn_oui_validationAudio_activity);
        mButtonNo = (Button)findViewById(R.id.btn_non_validationAudio_activity);
        mButtonRepeat = (Button)findViewById(R.id.btn_listen_again_validationAudio_activity);
        mButtonYes.setOnClickListener(this);
        mButtonNo.setOnClickListener(this);
        mButtonRepeat.setOnClickListener(this);

        mUri = DataKeeper.getInstance().getUriLectureFile();
        InputStream inputStream;
        if(mUri.toString().equals("Default")){
            inputStream = getApplicationContext().getResources().openRawResource(R.raw.table);
        }

        else{
            inputStream = GeneralUtils.getStreamFromUri(mUri,getApplicationContext());

        }

        DataSampleList = GeneralUtils.readTableData(getApplicationContext(),inputStream); // Lecture de table.csv
        mDataSample = DataSampleList.getDataSampleByIndex(1);

        mSpeaker = new TextToSpeech(this, this);
        final Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Context context = getApplicationContext();
                mSpeaker.speak(getString(R.string.tts_validation_activity_guiding_mode), TextToSpeech.QUEUE_ADD, null, null);
                mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                GeneralUtils.audioLectureUtils(context,mDataSample,mDataSample,mSpeaker, true,0);
                mSpeaker.playSilentUtterance(SHORT_DURATION, TextToSpeech.QUEUE_ADD, null);
                mSpeaker.speak(getString(R.string.tts_validation_activity_choice,getString(R.string.yes),getString(R.string.no),getString(R.string.listen_audioMessage)),TextToSpeech.QUEUE_ADD, null, null);
            }
         },1000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_oui_validationAudio_activity:
                Intent connectReaderIntent = new Intent(ValidationAudioActivity.this, BluetoothActivity.class);

                /* save this data in internal storage for next time */

                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("Direction", mDirection);
                editor.putString("Distance", mDistance);
                if(mHandPrecision)
                    editor.putString("PrecisionMain", "true");
                else
                    editor.putString("PrecisionMain", "false");
                if(mHandleShape)
                    editor.putString("FormePrise", "true");
                else
                    editor.putString("FormePrise", "false");
                if(mHandleSize)
                    editor.putString("TaillePrise", "true");
                else
                    editor.putString("TaillePrise", "false");

                editor.putString("Uri", mUri.toString());
                /* apply the edit */
                editor.commit();
                startActivity(connectReaderIntent);
                break;
            case R.id.btn_non_validationAudio_activity:
                Intent tableRestartIntent = new Intent(ValidationAudioActivity.this, DirectionActivity.class);
                startActivity(tableRestartIntent);
                break;
            case R.id.btn_listen_again_validationAudio_activity:
                Context context = getApplicationContext();
                GeneralUtils.audioLectureUtils(context, mDataSample,mDataSample, mSpeaker, true,0);
                break;
        }
    }

    protected void onStop() {
        if(mSpeaker != null) {
            mSpeaker.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            Log.d("TEXT TO SPEECH", "SUCCES");
        }
    }
}
