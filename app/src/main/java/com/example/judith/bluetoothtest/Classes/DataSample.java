package com.example.judith.bluetoothtest.Classes;
import android.content.Context;
import com.example.judith.bluetoothtest.R;

/**
 * <b> DataSample is the class representing a hold on the climbing wall.</b>
 * <p>
 * A hold has several characteristics :
 * <ul>
 * <li> A number.</li>
 * <li> An absolute coordinate on the x-axis, with reference the first hold of the wall.</li>
 * <li> An absolute coordinate on the y-axis, with reference the first hold of the wall.</li>
 * <li> A direction to the next hold.</li>
 * <li> A distance to the next hold.</li>
 * <li> The hand by wich is preferable to hold it.</li>
 * <li> A size.</li>
 * <li> A shape.</li>
 * </ul>
 * </p>
 */

public class DataSample {

    /**
     * The context in which this object is used. Necessary to use predefined Strings in the
     * value/string.xml file.
     */
    private Context mContext;

    public DataSample(Context context){
        this.mContext = context.getApplicationContext();
    }

    /* TableActivity variables */
    /**
     * The number of the hold, the first being the hold at the lower left of the climbing wall.
     * @see DataSample#getHoldNumber()
     * @see DataSample#setHoldNumber(int)
     */
    private int mHoldNumber;

    /**
     * A boolean to characterize if the hold is in the constrained path choose for the climber in
     * beginner mode. True if yes, False if not.
     * @see DataSample#isTagInPath()
     * @see DataSample#setIsTagInPath(boolean)
     */
    private boolean isTagInPath;


    /**
     * The x-axis coordinates of the hold, in centimeters.
     * @see DataSample#getxAbs()
     * @see DataSample#setxAbs(float)
     */
    private float xAbs;
    /**
     * The y-axis coordinates of the hold, in centimeters.
     * @see DataSample#getyAbs()
     * @see DataSample#setyAbs(float)
     */
    private float yAbs;

    /* Different ways to indicate the direction to the next hold */
    /**
     * The direction to the next hold in degrees to be readable by the speaker, assuming the
     * next hold is above this current hold.
     * @see DataSample#getDirectionDegr()
     * @see DataSample#setDirectionDegr(int)
     */
    private int mDirectionDegr;
    /**
     * The direction to the next hold in degrees to be readable by the speaker, assuming the
     * next hold is above this current hold.
     * @see DataSample#getStringDirectionDegr()
     * @see DataSample#setStringDirectionDegr(double)
     */
    private String mStringDirectionDegr;
    /**
     * The direction to the next hold in a clock reference, assuming the next hold is above our
     * current hold.
     * @see DataSample#getDirectionHour()
     * @see DataSample#setDirectionHour(double)
     */
    private int mDirectionHour;
    /**
     * The direction to the next hold in sides (upper left, right, etc...), assuming the next hold
     * is above this current hold.
     * @see DataSample#getDirectionSides()
     * @see DataSample#setDirectionSides(double)
     */
    private String mDirectionSides;
    /**
     * The direction to the next hold using cardinal points, assuming the next hold is above this
     * current hold.
     * @see DataSample#getDirectionCard()
     * @see DataSample#setDirectionCard(double)
     */
    private String mDirectionCard;


    /* Different ways to indicate the distance to the next hold */
    /**
     * The distance to the next hold in centimeters.
     * @see DataSample#setDistanceQuantCM(int)
     * @see DataSample#getDistanceQuantCM()
     */
    private int mDistanceQuantCM;
    /**
     * The distance to the next hold in meters.
     * @see DataSample#setDistanceQuantM(int)
     * @see DataSample#getDistanceQuantM()
     */
    private String mDistanceQuantM;
    /**
     * The distance to the next hold with qualitative indicators (far, close, etc...).
     * @see DataSample#setDistanceQual(int)
     * @see DataSample#getDistanceQual()
     */
    private String mDistanceQual;

    //private String mDistanceFoot;

    /**
     * The actual hand holding the hold, to be use when indicating distance and direction in
     * reference to this hand to the climber.
     * @see DataSample#setHandPrecision(String)
     * @see DataSample#getHandPrecision()
     */
    private String mHandPrecision;
    /**
     * The size of the hold.
     * @see DataSample#setHoldSize(String)
     * @see DataSample#getHoldSize()
     */
    private String mHoldSize;
    /**
     * The shape of the hold.
     * @see DataSample#setHoldShape(String)
     * @see DataSample#getHoldShape()
     */
    private String mHoldShape;

    /* Getters and Setters for types of directional indication */

    /**
     * Updates the number of the hold
     * @param holdNumber
     *               the new  number of the hold
     */
    public void setHoldNumber(int holdNumber) {
        mHoldNumber = holdNumber;
    }

    /**
     * Returns the number of the hold
     * @return mHoldNumber
     */
    public int getHoldNumber() {
        return mHoldNumber;
    }

    /**
     * Returns a boolean indicating if the hold is in the constrained path in climbing beginner
     * mode.
     * @return isTagInPath, true if yes, false if not.
     */
    public boolean isTagInPath() {
        return isTagInPath;
    }


    /**
     * Set the boolean characterizing if the hold is part or not of the constrained climbing path in
     * beginner mode.
     * @param isTagInPath
     *              the new number of the hold in beginner mode
     */
    public void setIsTagInPath(boolean isTagInPath) {
        this.isTagInPath = isTagInPath;
    }

    /**
     * Updates the x-axis coordinates of the hold.
     * @param xAbs
     *                     the new x-axis coordinates of the hold.
     */
    public void setxAbs(float xAbs) {
        this.xAbs = xAbs;
    }

    /**
     * Returns the x-axis coordinate of the hold.
     * @return xAbs
     */
    public float getxAbs() {
        return xAbs;
    }

    /**
     * Updates the y-axis coordinates of the hold.
     * @param yAbs
     *                     the new y-axis coordinates of the hold.
     */
    public void setyAbs(float yAbs) {
        this.yAbs = yAbs;
    }

    /**
     * Returns the y-axis coordinate of the hold.
     * @return yAbs
     */
    public float getyAbs() {
        return yAbs;
    }

    /**
     * Get the direction to the nex hold in degrees.
     * @return mDirectionDegr
     */
    public float getDirectionDegr() {
        return mDirectionDegr;
    }
    /**
     * Updates the direction to the next hold in degrees.
     * @param directionDegr
     *                     the new direction of the hold in degrees.
     */
    public void setDirectionDegr(int directionDegr) {
        mDirectionDegr = directionDegr;
    }

    /**
     * Converts the direction to the next hold in a direction in degree readable by the speaker.
     * The conversion is made by a static function in GeneralUtils.
     * @param intDirectionDegr
     *                     the angle in degrees from which the result is calculated.
     * @see GeneralUtils#doubleToStringPositiveAngle(double, Context)
     */
    public void setStringDirectionDegr(double intDirectionDegr) {
        this.mStringDirectionDegr = GeneralUtils.doubleToStringPositiveAngle(intDirectionDegr, mContext);
    }

    /**
     * Returns the direction to the next hold in degrees in a readable type by the speaker.
     * @return mStringDirectionDegr
     */
    public String getStringDirectionDegr(){return mStringDirectionDegr;}

    /**
     * Converts the direction to the next hold in clock referential and updates it.
     * The conversion is made by a static function in class GeneralUtils.
     * @param intDirectionDegr
     *                      the angle in degrees from which the direction in hour is calculated.
     * @see GeneralUtils#positiveAngleToHour(double)
     */
    public void setDirectionHour(double intDirectionDegr) {
        this.mDirectionHour = (int) GeneralUtils.positiveAngleToHour(intDirectionDegr); }

    /**
     * Returns the direction to the next hold in clock referential.
     * @return mDirectionHour
     */
    public int getDirectionHour(){return mDirectionHour;}

    /**
     * Converts the direction to the next hold in a direction using cardinal points.
     * The conversion is made by a static function in class GeneralUtils.
     * @param directionDegr
     *                  the angle in degrees from which the result is calculated.
     * @see GeneralUtils#positiveAngleToCardinal(double, Context)
     */
    public void setDirectionCard(double directionDegr){
        this.mDirectionCard = GeneralUtils.positiveAngleToCardinal(directionDegr, mContext); }

    /**
     * Returns the direction to the next hold using cardinal points.
     * @return mDirectionCard
     */
    public String getDirectionCard(){return mDirectionCard;}

    /**
     * Converts the direction to the next hold in a direction using sides (upper left, right...).
     * The conversion is made by a static function in GeneralUtils.
     * @param directionDegr
     *                     the angle in degrees from which the result is calculated.
     * @see GeneralUtils#positiveAngleToSide(double, Context)
     */
    public void setDirectionSides(double directionDegr) {
        this.mDirectionSides = GeneralUtils.positiveAngleToSide(directionDegr, mContext); }

    /**
     * Returns the direction to the next hold using left, right, etc...
     * @return mDirectionSides
     */
    public String getDirectionSides(){return mDirectionSides;}

    /* Getters and Setters for types of distance indication */

    /**
     * Updates the distance to the next hold in centimeters.
     * @param distanceQuantCM
     *                     the new distance to next hold in centimeters
     */
    public void setDistanceQuantCM(int distanceQuantCM) {this.mDistanceQuantCM = distanceQuantCM;}

    /**
     * Returns the distance to the next hold in centimeters.
     * @return mDistanceQuantCM
     */
    public int getDistanceQuantCM() {return mDistanceQuantCM;}

    /**
     * Returns the distance to the next hold using qualitative qualifications (far, close,etc...).
     * @return mDistanceQual
     */
    public String getDistanceQual() {return mDistanceQual;}

    /**
     * Converts the distance to the next hold in centimeters to a qualitative description(far,
     * close,etc...).
     * The conversion is made by a static function in GeneralUtils.
     * @param distanceQuantCM
     *                     the distance to next hold in centimeters.
     * @see GeneralUtils#distanceCMToQual(int, Context)
     */
    public void setDistanceQual(int distanceQuantCM) {
        this.mDistanceQual = GeneralUtils.distanceCMToQual(distanceQuantCM, mContext);
    }

    /**
     * Converts the distance to the next hold in centimeters to a distance in meters
     * The conversion is made by a static function in GeneralUtils.
     * @param distanceQuantCM
     *                     the distance to next hold in centimeters.
     * @see GeneralUtils#distanceCMToM(int, Context)
     */
    public void setDistanceQuantM(int distanceQuantCM) {
        this.mDistanceQuantM = GeneralUtils.distanceCMToM(distanceQuantCM, mContext);
    }

    /**
     * Returns the distance to the next hold in meters.
     * @return mDistanceQuantM
     */
    public String getDistanceQuantM() {return mDistanceQuantM;}

    /*Getters and setters for other parameters*/

    /**
     * Returns the hand from which the hold will be indicated.
     * @return mDistanceQuantM
     */
    public String getHandPrecision(){
        return mHandPrecision;
    }

     /**
     * Updates the hand from which the indication is given.
     * @param handPrecision
     *                     the hand to set as read from the CSV file.
     */
    public void setHandPrecision(String handPrecision) {
        mHandPrecision = handPrecision;

    }

    /**
     * Returns the hold shape.
     * @return mHoldShape
     */
    public String getHoldShape(){
        return mHoldShape;
    }

    /**
     * Updates the shape of the hold
     * @param holdShape
     *                     the hold shape to set as read from the CSV file.
     */
    public void setHoldShape(String holdShape) {
        mHoldShape = holdShape;
    }

    /**
     * Returns the hold size
     * @return mHoldSize
     */
    public String getHoldSize(){
        return mHoldSize;
    }

    /**
     * Updates the size of the hold
     * @param holdSize
     *                     the hold size to set as read from the CSV file.
     */
    public void setHoldSize(String holdSize){
        mHoldSize = holdSize;
    }

    public String toString() {
        return "DataSample{" +
                " tag=" + mHoldNumber +
                ", Précision main='" + mHandPrecision + '\'' +
                ", DirectionH ='" + mDirectionHour + '\'' +
                ", DirectionCard ='" + mDirectionCard + '\'' +
                ", DirectionCot ='" + mDirectionCard + '\'' +
                ", DirectionSides ='" + mDirectionSides + '\'' +
                ", DirectionDegr ='" + mDirectionDegr + '\'' +
                ", DistanceQual ='" + mDistanceQual + '\'' +
                ", DistanceQuantCM ='" + mDistanceQuantCM + '\'' +
                ", DistanceQuantM ='" + mDistanceQuantM + '\'' +
                "Forme Prise =" + mHoldShape + '\'' +
                "Taille Prise =" + mHoldSize + '\'' +
                '}';
    }

    /*public int getHoldNumber() {
        return mTagOrder;
    }

    public void setHoldNumber(int tagOrder) {
        mTagOrder = tagOrder;
    }*/

     /*public String getDistanceFoot() {
        return mDistanceFoot;
    }

    public void setDistanceFoot(double distanceQuantHandCM, float climberSize) {
        if(climberSize == 0){
            this.mDistanceFoot = "";
        }
        else{
            if(distanceQuantHandCM <= ((climberSize*30) / 100))
                this.mDistanceFoot = mContext.getString(R.string.dataSample_knees);
            else if(distanceQuantHandCM > ((climberSize*30) / 100) && distanceQuantHandCM <= ((climberSize*40) / 100))
                this.mDistanceFoot = mContext.getString(R.string.dataSample_thighs);
            else if (distanceQuantHandCM > ((climberSize*40) / 100) && distanceQuantHandCM < ((climberSize*50) / 100))
                this.mDistanceFoot = mContext.getString(R.string.dataSample_hips);
            else if (distanceQuantHandCM > ((climberSize*50) / 100) && distanceQuantHandCM < ((climberSize*65) / 100))
                this.mDistanceFoot = mContext.getString(R.string.dataSample_waist);
            else
                this.mDistanceFoot = mContext.getString(R.string.dataSaple_above_waist);
        }
    }*/

}

