package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> DistanceAcivity is the second activity of a series of 5 activities that allow the user to
 * customize the audio message he will hear during the climbing.</b>
 *<p> This activity is about the manner in which the distance to the next hold is indicated.
 * 3 options are available :
 *<ul>
 *<li> in centimeters, </li>
 *<li> in meters,</li>
 *<li> using qualifiers (far, close, etc...) </li>
 *</ul>
 *</p>
 *<p>
 * Once the user has made his choice by selecting one button, the result is kept in the singleton
 * call DataKeeper.
 * @see DataKeeper#setDistance(String)
 *</p>
 *
 */


public class DistanceActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mQualit;
    private Button mQuantCM;
    private Button mQuantM;

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);

        /* Layout */
        mQualit = (Button) findViewById(R.id.btn1_distance_activity);
        mQuantCM = (Button) findViewById(R.id.btn2_distance_activity);
        mQuantM = (Button) findViewById(R.id.btn3_distance_activity);
        mTextView = (TextView)findViewById(R.id.text_distance_activity);

        mQualit.setOnClickListener(this);
        mQuantCM.setOnClickListener(this);
        mQuantM.setOnClickListener(this);
        mTextView.setText(getString(R.string.choice_introduction_message, 3,getString(R.string.distance_activity)));
    }

    @Override
    public void onClick(View v) {
        Intent distanceIntent = new Intent(DistanceActivity.this, HandPrecisionActivity.class);
        switch (v.getId()) {
            case R.id.btn1_distance_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDistance(getString(R.string.distanceQual));
                startActivity(distanceIntent);
                break;
            case R.id.btn2_distance_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDistance(getString(R.string.distanceCM));
                startActivity(distanceIntent);
                break;
            case R.id.btn3_distance_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDistance(getString(R.string.distanceM));
                startActivity(distanceIntent);
                break;
        }
    }
}
