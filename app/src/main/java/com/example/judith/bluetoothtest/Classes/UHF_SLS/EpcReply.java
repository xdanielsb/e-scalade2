package com.example.judith.bluetoothtest.Classes.UHF_SLS;

/**
 * EPCReply is a class that contains a typical SmartLink UHF reader response from tag read.
 * This class contains 3 variables :
 * <ul>
 *     <li> epc, stands for Electronic Product Code, the tag's id in a byte array format. This
 *     EPC is unic for each RFID tag.</li>
 *     <li> pc,  stands for Protocol Control, defines the lenght of the EPC code. </li>
 *     <li> rssi, the Received Signal Strength Indication from the tag. The RSSI can be used to
 *     obtain the distance between the reader and the tag (thus not always reliable and should not
 *     be used alone).</li>
 * </ul>
 */
public class EpcReply {
    private byte[] epc;
    private byte[] pc;
    private float rssi;

    public EpcReply(byte[] pc, byte[] epc, float rssi) {
        this.pc = pc;
        this.epc = epc;
        this.rssi = rssi;
    }

    public byte[] getEpc() {
        return this.epc;
    }

    public byte[] getPc() {
        return this.pc;
    }

    public float getRssi() {
        return this.rssi;
    }
}
