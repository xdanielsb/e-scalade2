package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.util.Log;

import java.util.Arrays;

public class FrameProcessing {

    private static final int errorInt = -4;

    private byte[] mBytes = new byte[20];

    /**
     * Index kept to restart the copy in mBytes if several frames need to be copied in mBytes.
     * mIndexToCopy is 0 for the first frame and is then incremented by the lenght of byteArray each time a
     * new byteArray is copied to mBytes.
     * For example, if a byteArray of 40 bytes is copied to mBytes, the next byteArray to come will
     * be copied in mBytes to the index of 40 so the first byteArray copied is not erased. */
    private int mIndexToCopy;

    /**
     * int to store the lenght of data bytes in the frame. The frame contains indeed data bytes and
     * metadata bytes. The third and fourth bytes of the frame indicates the lenght of data in
     * number of bytes. This lenght is set to -1 when the object FrameProcessing is initialized and
     * then keeps the supposes data lenght.
     * */
    private int mDataLenght = -1;

    /* renamed from: k */
    private boolean f38k;

    private boolean mTagRead = false;

    private static String TAG = "FrameProcessing";

    FrameProcessing(boolean bool) {
        this.mTagRead = bool;
        if (bool) { //if this is about single inventory
            this.mBytes = new byte[0];
        }
    }

    /* renamed from: a */
    int m19a(byte[] byteArray) {
        while (mBytes.length - mIndexToCopy < byteArray.length) {
            /* Expands  mBytes long enough to contain a whole number of frames of 20 bytes from byteArray.
             * For example, if the lenght of byteArray is 56 bytes, the final lenght of mBytes will
              * be of its original lenght + 40 byes (= 2 * 20 bytes).*/
            byte[] obj = new byte[(mBytes.length + 20)];
            System.arraycopy(mBytes, 0, obj, 0, mBytes.length); //copies entirely mBytes to obj - CHANGED FROM 0 TO 1
            mBytes = obj;

        }
        System.arraycopy(byteArray, 0, mBytes, mIndexToCopy, byteArray.length);
        //Log.d("FrameProcessing", "mBytes = " + Arrays.toString(mBytes));
        //Log.d("FrameProcessing", "BYTE ARRAY = " + Arrays.toString(mBytes));
        this.mIndexToCopy += byteArray.length;
        /* If this is the first frame (i.e. mDataLenght = -1, which means it hasn't been initialized yet
         * and if the byteArray contains data bytes (i.e. if its lenght is >= 5) */
        if (this.mDataLenght == -1 && this.mIndexToCopy >= 5) {
            this.mDataLenght = ((this.mBytes[3] & 255) << 8) + (this.mBytes[4] & 255);
        }
        /* If this frame is not the last of the message, return 1 */
        if (mIndexToCopy < mDataLenght + 8) {
            return 1;
        }
        /* Each data frame needs to start with -69 and end with 126 (metadata bytes included) to be
         * valid.*/
        if (mBytes[0] != (byte) -69) {
            return -1;
        }
        if (mBytes[mDataLenght + 5] != (byte) 126) {
            return -2;
        }
        short a = UHFUtils.m32a(mBytes, mDataLenght + 5);

        /* A short has a lenght of two bytes. The short a needs to be equal to the concatenation
         * of both last bytes of mBytes. Otherwise, its an error. */
        if(mBytes[mDataLenght + 6] != ((byte) (a >>8)) ||(mBytes[mDataLenght + 7] != (byte)(a)))
            return -3;
        f38k = true;
        return 0;
    }

    /**
     * @return true if the frame to process is a tag ID, false if not
     */
    boolean isTagReading() {
        return this.mTagRead;
    }

    /**
     * Retrieves one frame after another an stores them in order into the mBytes byteArray.
     * @param value
     *          the actual frame received to be treated.
     */
    int m21b(byte[] value) {
        Log.d(TAG, "M 21B - Frame received, mBytes lenght = " +mBytes.length);
        byte[] obj = new byte[(mBytes.length + value.length)];
        /* Copies the content of mBytes to a buffer obj */
        System.arraycopy(mBytes, 0, obj, 0, mBytes.length);
        /* Add to obj the content of value after mBytes, so obj has mBytes + value */
        System.arraycopy(value, 0, obj, mBytes.length, value.length);
        /* Updates mBytes to store the content of obj */
        mBytes = obj;
        Log.d(TAG, "M 21B - mBytes new lenght = " +mBytes.length);
        /* Data in each frame starts at byte 5, so if the length is shorter than 5, there isn't any data in the frame received.*/
        if (mBytes.length < 5) {
            return 1;
        }
        /* Third and fourth byte of the answer contains the length of the data in the frame. Typical is 18 for a tag ID */
        int dataLenght = ((this.mBytes[3] & 255) << 8) + (this.mBytes[4] & 255);
        Log.d(TAG, "M 21B -  data lenght = " + dataLenght);
        /* i2 = data lenght + lenght of one byte */
        int i2 = dataLenght + 8;
        Log.d(TAG, "M21B -  I2 = " + i2);
        /* If the lenght of mBytes is inferior to the supposed lenght of the data + 8, then it
        means that some frames are following, so the FrameProcessing object needs to store the data
         and wait for the entire message to be received, i.e. to keep receiving the following frames.*/
        if (this.mBytes.length < i2) {
            return 1;
        }
        byte[] obj2 = new byte[i2];
        System.arraycopy(this.mBytes, 0, obj2, 0, i2);

        /* Each data frame needs to start with -69 and end with 126 (metadata bytes included) to be
        * valid.*/
        if (obj2[0] != (byte) -69) {
            return -1;
        }
        int i3 = dataLenght + 5;
        if (obj2[i3] != (byte) 126) {
            return -2;
        }
        short a = UHFUtils.m32a(obj2,i3);
        if (obj2[dataLenght + 6] == ((byte) (a >> 8))) {
            if (obj2[dataLenght + 7] == ((byte) a)) {
                if (mBytes.length > i2 && this.mBytes[2] == UHFUtils.AREA_US2) {
                    return 2;
                }
                if (this.mBytes.length == i2) {
                    if (this.mBytes[2] == UHFUtils.AREA_US2) {
                        return this.mBytes[1] == (byte) 3 ? 0 : 2;
                    } else {
                        if (this.mBytes[2] == (byte) -1) {
                            return 0;
                        }
                    }
                }
                return errorInt;
            }
        }
        return -3;
    }

    FrameData m22b() {
        if (!this.f38k) {
            return null;
        }
        byte[] obj = new byte[mDataLenght];
        /* Copies the entire content of mBytes without the 5 first bytes which are not data bytes. */
        System.arraycopy(this.mBytes, 5, obj, 0, mDataLenght);
        return new FrameData(this.mBytes[1], this.mBytes[2], obj);
    }

    /**
     * Extract data from the byteArray mBytes and stores it into a FrameData object.
     * The byteArray of the FrameData object will contain only the data bytes. The two bytes of this
     * object will contain respectively the first and second byte of the frame (which has been
     * previously stored entirely in mBytes).
     * @return a new FrameData object containing extracted data from the frame(s) received.
     */
    FrameData m23c() {
        byte[] totalFrame = new byte[mBytes.length];
        /* get the lenght of data present in the frame. Indicated by the third and fourth bytes,
         * which are metadata bytes. */
        int dataLenght = ((this.mBytes[3] & 255) << 8) + (this.mBytes[4] & 255);
        Log.d(TAG, "M 23C - data lenght = " +dataLenght);
        byte[] dataFrame = new byte[dataLenght];
        /* Copies the entire content of mBytes without the 5 first bytes and the last 3 bytes that
        are not data bytes but metadata bytes */
        System.arraycopy(this.mBytes, 5, dataFrame, 0, dataLenght);
        /* Copies the entire content of mBytes with the metadata bytes. */
        System.arraycopy(this.mBytes, 0, totalFrame, 0, totalFrame.length);
        Log.d(TAG, "M23C - OBJ2= " + Arrays.toString(dataFrame));
        Log.d(TAG, "M23C - OBJ = " + Arrays.toString(totalFrame));
        byte[] obj3 = new byte[((this.mBytes.length - dataLenght) - 8)];
        /* Store the resting bytes in mBytes. */
        System.arraycopy(this.mBytes, dataLenght + 8, obj3, 0, obj3.length);
        Log.d(TAG, "M23C - OBJ3 = " + Arrays.toString(obj3));
        this.mBytes = obj3;
        Log.d(TAG, "M23C - MBYTES = " + Arrays.toString(mBytes));
        return new FrameData(totalFrame[1], totalFrame[2], dataFrame);
    }
}
