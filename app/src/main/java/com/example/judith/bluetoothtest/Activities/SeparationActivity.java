package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> Activity to choose between the climbing mode and the recording mode.</b>
 * @see DataKeeper#setClimbingMode(boolean)
 */

public class SeparationActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButtonClimb;
    private Button mButtonNewRoute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_separation);

        mButtonClimb = (Button)findViewById(R.id.button_climb_separation_activity);
        mButtonNewRoute = (Button)findViewById(R.id.button_record_new_route_separation_activity);

        mButtonNewRoute.setOnClickListener(this);
        mButtonClimb.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_climb_separation_activity:
                Intent climbIntent = new Intent(SeparationActivity.this, FileChoiceActivity.class);
                DataKeeper.getInstance().setClimbingMode(true);
                startActivity(climbIntent);
                break;
            case R.id.button_record_new_route_separation_activity:
                Intent recordIntent = new Intent(SeparationActivity.this, FileChoiceActivity.class);
                DataKeeper.getInstance().setClimbingMode(false);
                startActivity(recordIntent);
        }
    }
}
