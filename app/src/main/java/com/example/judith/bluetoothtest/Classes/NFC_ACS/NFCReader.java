package com.example.judith.bluetoothtest.Classes.NFC_ACS;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.acs.bluetooth.Acr1255uj1Reader;
import com.acs.bluetooth.BluetoothReader;
import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.ErrorCodes;
import com.example.judith.bluetoothtest.Classes.ReaderToActivity;
import com.example.judith.bluetoothtest.R;

/**
 * NFCReader is a wrapper class for a BluetoothReader instance from the ACS API. This class
 * implements the methods and behaviour of a BluetoothReader object.
 */

public class NFCReader implements TextToSpeech.OnInitListener{

    private static final byte[] AUTO_POLLING_START = { (byte) 0xE0, 0x00, 0x00, 0x40, 0x01 };

    private static final byte[] AUTO_POLLING_STOP = { (byte) 0xE0, 0x00, 0x00, 0x40, 0x00 };

    /**
     * Object to represent a NFC Bluetooth reader. Obtained from the ACS API.
     * @see BluetoothReader
     */
    private BluetoothReader mBluetoothReader;

    /* Links to the activity*/
    /**
     * Activity object to represent the activity calling the NFCReader.
     */
    private ReadersActivity mReadersActivity;

    /**
     * Context object to represent the context in which the NFCReader is used.
     */
    private Context mContext;

    /* Default escape command - set the reader's sleep mode to "no sleep" */
    private static final String ESCAPE_COMMAND_NO_SLEEP = "E0 00 00 48 04";

    /* Default apdu command - ask for tag's UUID. */
    private static final String APDU_COMMAND_ASK_UUID = "FF CA 00 00 00";

    /**
     * int to identify the reader, 1 or 2.
     */
    private int mDeviceNumber;

    /**
     * TextToSpeech instance to read audio messages to the user.
     */
    private TextToSpeech mSpeaker;

    private ReaderToActivity mInterface;

    public NFCReader(Activity activity, int deviceNumber){
        mContext = (Context)activity;
        mReadersActivity = (ReadersActivity)activity;
        mDeviceNumber = deviceNumber;
        mSpeaker = new TextToSpeech(mReadersActivity, mReadersActivity);
        mInterface = (ReaderToActivity)activity;
        Log.d("NFC READER", "CREATION OF NFC READER");
    }

    /**
     * Updates the instance of BluetoothReader wrapped in the NFCReader instance.
     * @param bluetoothReader
     *                      the new instance of BluetoothReader
     * @see NFCBluetoothManager
     */
    void setBluetoothReader (BluetoothReader bluetoothReader){
        mBluetoothReader = bluetoothReader;
    }

    /**
     * Set the status change listener of the NFC reader. The listener triggers a response when
     * one of these 5 events happens :
     * <ul>
     *     <li> when the authenticate method is called on the BluetoothReader object, </li>
     *     <li> when the enableNotification method is called on the BluetoothReader object, </li>
     *     <li> when the status of the card/tag listener changes, e.g. when a tags is detected by
     *     the reader. </li>
     *     <li> when the NFC reader answer to an escape command, </li>
     *     <li> when the NFC reader answer to an apdu command, </li>
     * </ul>
     * The setListenerNFCReader method set the behaviour of the listener of the NFC Readers, which is the
     * same for both readers when both are used (i.e. in climbing mode).
     * @see BluetoothReader
     */
    void setListenerNFCReader(){
        mBluetoothReader.setOnAuthenticationCompleteListener(new BluetoothReader.OnAuthenticationCompleteListener() {
            /**
             * Overwritten method of the BluetoothReader class to set the behaviour of the
             * listener when the authentication of the reader is complete.
             * If the authentication has succeeded, a default escape command is sent to the reader,
             * to put it in "no sleep" mode. This mode makes sure that the readers won't go into
             * sleep mode during the climbing.
             * @see BluetoothReader#transmitEscapeCommand(byte[])
             */
            @Override
            public void onAuthenticationComplete(BluetoothReader bluetoothReader, final int errorCode) {
                        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
                            Log.i("NFC Reader", "Reader " + mDeviceNumber + " - authentication complete !");
                            byte[] escapeCommand = NFCUtils.getTextInHexBytes(ESCAPE_COMMAND_NO_SLEEP);
                            if (escapeCommand != null && escapeCommand.length > 0) {
                                if (!mBluetoothReader.transmitEscapeCommand(escapeCommand)) {
                                    mInterface.postToastMessage(mContext.getString(R.string.toast_escape_command_failure_nfc_reader,mDeviceNumber));
                                }
                            } else {
                                mInterface.postToastMessage(mContext.getString(R.string.toast_error_responseFormat_nfc_reader,mDeviceNumber));
                            }
                        } else
                            mInterface.postToastMessage(mContext.getString(R.string.toast_auth_failure_nfc_reader,mDeviceNumber));
            }
        });
        /* When notifications are enabled */
        mBluetoothReader.setOnEnableNotificationCompleteListener(new BluetoothReader.OnEnableNotificationCompleteListener() {

            @Override
            public void onEnableNotificationComplete(BluetoothReader bluetoothReader, final int result) {
                if (result != BluetoothGatt.GATT_SUCCESS) {
                    /* Fail */
                    Log.i("NFC Reader","Reader " + mDeviceNumber + " cannot enable notifications!");
                } else {
                    Log.i("NFC Reader","Reader " + mDeviceNumber + " - notifications authorized !");
                }
            }
        });

        /* When an escape response is ready */
        mBluetoothReader.setOnEscapeResponseAvailableListener(new BluetoothReader.OnEscapeResponseAvailableListener() {
            @Override
            public void onEscapeResponseAvailable(BluetoothReader bluetoothReader, byte[] response, int errorCode) {

                Log.d("NFC READER", "LECTEUR - ESCAPE RESPONSE : " + getResponseString(response, errorCode));
            }
        });
        /* Change of tag status */
        mBluetoothReader.setOnCardStatusChangeListener(new BluetoothReader.OnCardStatusChangeListener() {

            /**
             * Overwritten method of the BluetoothReader class to set the behaviour of the
             * listener when a change is the status of card/tag listener is observed. When a tag is
             * detected, send an apdu command to the reader asking for the tag's UUID.
             * @see BluetoothReader#transmitApdu(byte[])
             */
            @Override
            public void onCardStatusChange(
                    BluetoothReader bluetoothReader, final int sta) {

                Log.i("NFC READER", "mCardStatusListener reader 1 sta: " + sta);
                        if(getCardStatusString(sta).equals(mContext.getString(R.string.card_status_tag_present_nfc_reader))){
                            Log.d("READERS ACTIVITY","TAG PRESENT");
                            byte[] apduCommand = NFCUtils.getTextInHexBytes(APDU_COMMAND_ASK_UUID);
                            if (apduCommand != null && apduCommand.length > 0) {
                                /* Transmit APDU command, if the state is "Tag présent". */
                                if (!mBluetoothReader.transmitApdu(apduCommand)) {
                                    mInterface.postToastMessage(mContext.getString(R.string.toast_apdu_notReady_nfc_reader,mDeviceNumber));
                                }
                            } else {
                                mInterface.postToastMessage(mContext.getString(R.string.toast_error_character_format));
                            }
                        }
            }
        });

        mBluetoothReader.setOnResponseApduAvailableListener(new BluetoothReader.OnResponseApduAvailableListener() {
            /**
             * <p>
             *     Overwritten method of the BluetoothReader class to set the behaviour of the
             *     listener when an apdu response is available. In this application, an apdu
             *     response corresponds to a tag's UUID, because the apdu command sent by default to
             *     the reader when a tag is detected (i.e. when the onCardStatusChange
             *     method is called and when the new status is CARD_STATUS_PRESENT) is an
             *     interrogation to the tag to get its UUID.
             * </p>
             * <p>
             *     The listener implements 3 different behaviours by calling the onTagRead
             *     method in the readers activity. Before, this methods checks the UUID's format
             *     and length, which must be 27 or 26 (sometimes there is space at the end of the
             *     UUID).
             * </p>
             * @param bluetoothReader
             *                      the instance of BluetoothReader who received an apdu response
             * @param apdu
             *          a byte array containing the apdu response
             * @param errorCode
             *                the error code qualifying the communication, ERROR_SUCCESS if
             *                everything went well
             *
             * @see BluetoothReader#setOnResponseApduAvailableListener(BluetoothReader.OnResponseApduAvailableListener)
             * @see ReadersActivity#onTagRead(String, int)
             */
            @Override
            public void onResponseApduAvailable(BluetoothReader bluetoothReader, byte[] apdu, int errorCode) {
                Log.d("UUID", "Reader :" + mDeviceNumber + "  " + getResponseString(apdu, errorCode) + " lenght = " + getResponseString(apdu, errorCode).length());
                final String apduResponse = getResponseString(apdu, errorCode);
                /*Check that the length is 27 for a 7 bytes UUID, or 26 without the space at the end*/
                if (apduResponse.isEmpty() || apduResponse == null)
                    mSpeaker.speak(mContext.getString(R.string.tts_no_tagAnswer_nfc_reader), TextToSpeech.QUEUE_ADD, null, null);
                else if(apduResponse.length() !=26 && apduResponse.length() != 27)
                    mSpeaker.speak(mContext.getString(R.string.tts_badResponseFormat_nfc_reader), TextToSpeech.QUEUE_ADD, null, null);

                /* Remove spaces, first byte (manufacturer byte) and the two last bytes (special bytes) */
                else {
                    mReadersActivity.onTagRead(apduResponse,mDeviceNumber);
                }
            }
        });
    }

    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            Log.d("TTS NFC READER","SUCCESS");
        }
        else
            Log.d("TTS NFC READER", "FAILURE");
    }

    /**
     * Updates the state of mOnRestartRecording boolean.
     * @param onRestartRecording
     *                      the new value to be set
     * @see ReadersActivity#restartTagListFragment()
     */
    //public void setOnRestartRecording(boolean onRestartRecording) {
        //mOnRestartRecording = onRestartRecording;
    //}

    /**
     * Gets the card status in a String format readable by the user.
     * @param cardStatus
     *              the status of the card (or tag)
     * @return a String from string.xml with the corresponding value of the status.
     */
    private String getCardStatusString(int cardStatus) {
        if (cardStatus == BluetoothReader.CARD_STATUS_ABSENT) {
            return mContext.getString(R.string.card_status_no_tag_nfc_reader);
        } else if (cardStatus == BluetoothReader.CARD_STATUS_PRESENT) {
            return mContext.getString(R.string.card_status_tag_present_nfc_reader);
        } else if (cardStatus == BluetoothReader.CARD_STATUS_POWERED) {
            return mContext.getString(R.string.card_status_tag_powered_nfc_reader);
        } else if (cardStatus == BluetoothReader.CARD_STATUS_POWER_SAVING_MODE) {
            return mContext.getString(R.string.card_status_powerSaving_nfc_reader);
        }
        return mContext.getString(R.string.card_status_unknown_nfc_reader);
    }

    /**
     * Gets the answer from the NFC reader and treats it. Without errors, the answer is returned in
     * a String format.
     * @param response
     *              a byte array containing the answer sent by the reader
     * @param errorCode
     *              the error code of the communication, ERROR_SUCCESS if no errors
     * @return a String containing the answer of the reader, or an empty string if an error occurred
     * during the communication.
     */
    private String getResponseString(byte[] response, int errorCode) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            if (response != null && response.length > 0) {
                return NFCUtils.toHexString(response);
            }
            return "";
        }
        return ErrorCodes.getErrorNFCString(errorCode, mContext);
    }

    /**
     * Checks is the BluetoothReader instance wrapped into the NFCReader instance is null.
     * @return true if it's null
     */
    public boolean isACSReaderNull(){
        return mBluetoothReader == null;
    }

    /**
     * Checks is the BluetoothReader instance wrapped into the NFCReader is linked to a NFC reader
     * ACR1255-UJ1.
     * @return true if yes
     */
    public boolean isReaderInstanceOfACR1255UJ1(){
        return mBluetoothReader instanceof Acr1255uj1Reader;
    }

    /**
     * Enables the notifications for the BluetoothReader instance wrapped.
     * @param enable a parameter to enable notification if equal to true, and disable them if equal
     *               to false.
     * @see BluetoothReader#enableNotification(boolean)
     */
    public boolean enableNotificationNFC(boolean enable){
        return mBluetoothReader.enableNotification(enable);
    }

    /**
     * Authenticates the NFC reader.
     * @param authenticationKey the authentication key for this brand and version of NFC readers.
     * @see BluetoothReader#authenticate(byte[])
     */
    public boolean authenticateNFC(byte[] authenticationKey){
        return mBluetoothReader.authenticate(authenticationKey);
    }

    /**
     * Sets the reader in polling mode by transmit it the corresponding escape command.
     * @see BluetoothReader#transmitEscapeCommand(byte[])
     */
    public boolean startTagPollingNFC() {
        return (mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_START));
    }

    /**
     * Sets the reader out of polling mode by transmit it the corresponding escape command.
     * @see BluetoothReader#transmitEscapeCommand(byte[])
     */
    public boolean stopTagPollingNFC(){
        return (mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_STOP));
    }
}

