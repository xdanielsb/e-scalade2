package com.example.judith.bluetoothtest.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * A fragment used to display the UI view to allow the user to choose the difficulty mode of
 * climbing : beginner, or advanced.
 * In beginner mode, the holds in the path are constrained, and the user only has one instruction
 * by hold; indicating the next one.
 * In advanced mode, for each hold grabbed the user ears an indication of the X holds being less
 * than 2 meters above him, X being a number chosen in the AdvancedModeFragment fragment.
 * @see Fragment
 */
public class DifficultyChoiceFragment extends Fragment implements View.OnClickListener {

    /**
     * Required empty public constructor
     */
    public DifficultyChoiceFragment() {
    }

    /**
     * Interface to communicate with the DifficultyChoiceActivity and send an action to be performed
     * when one of the fragment's button is pressed.
     */
    public interface onChoiceMade{
        void onBeginnerModeChosen();

        void onAdvancedModeChosen();
    }

    /**
     * Instance of the interface to communicate with the activity.
     */
    private onChoiceMade mOnChoiceMade;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_difficulty_choice, container, false);

        TextView mChosenMode;
        mChosenMode = (TextView)view.findViewById(R.id.text_chosenMode_difficulty_fragment);
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        Button mAdvanceButton;
        Button mBeginnerButton;

        mAdvanceButton = (Button)view.findViewById(R.id.btn_advanced_mode_difficulty_choice_fragment);
        mBeginnerButton = (Button)view.findViewById(R.id.btn_beginner_mode_difficulty_choice_fragment);

        mAdvanceButton.setOnClickListener(this);
        mBeginnerButton.setOnClickListener(this);

        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = (Activity) context;
        try {
            mOnChoiceMade = (onChoiceMade) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_advanced_mode_difficulty_choice_fragment:
                mOnChoiceMade.onAdvancedModeChosen();
                break;

            case R.id.btn_beginner_mode_difficulty_choice_fragment:
                mOnChoiceMade.onBeginnerModeChosen();
                break;
        }

    }
}
