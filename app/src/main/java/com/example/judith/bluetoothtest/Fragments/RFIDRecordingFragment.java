package com.example.judith.bluetoothtest.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Activities.ReadersActivity;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DoneEditorActionListener;
import com.example.judith.bluetoothtest.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment used to display the UI view on recording mode.
 * @see Fragment
 */
public class RFIDRecordingFragment extends Fragment implements View.OnClickListener{

    /**
     * The list from which the ListView will be displayed.
     */
    private List<TagScanResult> mItemList = new ArrayList<>();

    private onButtonClicked mOnButtonClicked;

    /**
     * The view adapter managing the listView
     */
    private CustomAdapter mCustomAdapter;

    /**
     * Required empty public constructor
     */
    public RFIDRecordingFragment() {
    }

    /**
     * Interface to implement in the linked activity to trigger actions when a button in the
     * fragment is pressed.
     * @see ReadersActivity#startPollingTagListFragment()
     * @see ReadersActivity#validateTagListFragment()
     * @see ReadersActivity#restartTagListFragment()
     */
    public interface onButtonClicked{
        void startPollingTagListFragment();

        void validateTagListFragment();

        void restartTagListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_rfid_recording, container, false);

        TextView mChosenMode;
        mChosenMode = (TextView)view.findViewById(R.id.text_chosenMode_recording_fragment);
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        Button mStartPollingButton;
        Button mOkButton;
        Button mRestartButton;

        mStartPollingButton = (Button)view.findViewById(R.id.btn_startPolling_tagList);
        mStartPollingButton.setOnClickListener(this);

        mOkButton = (Button)view.findViewById(R.id.btn_validate_tag_list_fragment);
        mOkButton.setOnClickListener(this);

        mRestartButton = (Button)view.findViewById(R.id.btn_restart_recording_tag_list_fragment);
        mRestartButton.setOnClickListener(this);

        /* Headers of the listView */
        TextView listTitle = new TextView(getContext());
        listTitle.setText(getContext().getString(R.string.list_view_title));
        listTitle.setTextSize(15);
        listTitle.setTextColor(getResources().getColor(R.color.colorPrimary));

        ListView mListView = view.findViewById(R.id.tagList);
        mCustomAdapter = new CustomAdapter(getContext(), mItemList);

        /* Attach the listView to the adapter and add the headers of the listView*/
        mListView.setAdapter(mCustomAdapter);
        mListView.addHeaderView(listTitle);

        /* Inflate the layout for this fragment*/
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_startPolling_tagList:
                Log.d("FRAGMENT TAG LIST", "START POLLING...");
                mOnButtonClicked.startPollingTagListFragment();
                break;

            case R.id.btn_validate_tag_list_fragment:
                mOnButtonClicked.validateTagListFragment();
                break;

            case R.id.btn_restart_recording_tag_list_fragment:
                mOnButtonClicked.restartTagListFragment();
                break;
        }
    }

    /**
     * Creates a new TagScanResult object and sets its contents with the parameters passed.
     * Then adds this new element to the adapter and alerts the adapter of the modification of its
     * data.
     * @param uuid
     *          the uuid of the new TagScanResult object
     * @param xAbs
     *          the x-axis coordinates of the new TagScanResult object
     * @param yAbs
     *          the y-axis coordinates of the new TagScanResult object
     * @param size
     *          the size of the new TagScanResult object
     * @param shape
     *          the shape of the new TagScanResult object
     */
    public void addListItem(String uuid, String xAbs, String yAbs, String size, String shape){
        TagScanResult newItem = new TagScanResult();
        newItem.setUuid(uuid);
        newItem.setxAbs(xAbs);
        newItem.setyAbs(yAbs);
        newItem.setShape(shape);
        newItem.setSize(size);
        mCustomAdapter.addItem(newItem);
        mCustomAdapter.notifyDataSetChanged();
        Log.d("FRAGMENT", "ADD ITEM CALLED");
    }

    /**
     * Call the clear list function of the adapter and alerts it about the modification of its data.
     * @param i
     *      the parameter to be passed to the clearList method of the adapter. Indicates the
     *      position from which the list needs to be cleared.
     */
    public void clearList(int i){
        mCustomAdapter.clearList(i);
        mCustomAdapter.notifyDataSetChanged();
    }

    /**
     * An adapter to display and handle the listView. Each iter
     */
    public class CustomAdapter extends ArrayAdapter<TagScanResult> {
        Context context = getContext();

        private CustomAdapter(Context context, List<TagScanResult>list) {
            super(context,0,list);
        }

        /**
         * A viewHolder containing all the elements to be displayed on each row of the listView.
         */
        private class ItemViewHolder {
            TextView mScanResult;
            TextView mXAbs;
            TextView mYAbs;
            TextView mSize;
            TextView mShape;
        }

        public View getView(int position, View view, ViewGroup viewGroup) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (view == null) {
                view = inflater.inflate(R.layout.row_recoding_list, viewGroup, false);
            }

            ItemViewHolder viewHolder = (ItemViewHolder) view.getTag();

            final TagScanResult newItem = getItem(position);

            if (viewHolder == null) {
                /* Default values, stored in string.xml*/
                viewHolder = new ItemViewHolder();
                viewHolder.mScanResult = (TextView) view.findViewById(R.id.scan_result_uuid);
                viewHolder.mScanResult.setOnEditorActionListener(new DoneEditorActionListener());
                viewHolder.mXAbs = (TextView)view.findViewById(R.id.scan_result_xAbs);
                viewHolder.mYAbs = (TextView)view.findViewById(R.id.scan_result_yAbs);
                viewHolder.mShape = (TextView)view.findViewById(R.id.scan_result_shape);
                viewHolder.mSize = (TextView)view.findViewById(R.id.scan_result_size);
                view.setTag(viewHolder);
            }

            if (newItem != null && newItem.getUuid().length() > 0){
                viewHolder.mScanResult.setText(newItem.getUuid());
                viewHolder.mXAbs.setText(newItem.getxAbs());
                viewHolder.mYAbs.setText(newItem.getyAbs());
                viewHolder.mSize.setText(newItem.getSize());
                viewHolder.mShape.setText(newItem.getShape());
            }
            return view;
        }

        /**
         * Adds the new element passed to the List<TagScanResult> mItemList. The view is created
         * from this list.
         * @param newItem
         *             a TagScanResult objects to be added to mItemList.
         */
        private void addItem(TagScanResult newItem) {
            Log.d("CUSTOM ADAPTER", "ADD ITEM CALLED");
            mItemList.add(newItem);
        }

        /**
         * Clears the List<TagScanResult> mItemList from a specified index.
         * @param i
         *        the index from which to clean the list. Every element above the element at i
         *        position (including the element at i position) will be remove from the list.
         */
        private void clearList(int i){
            mItemList.subList(i, mItemList.size()).clear();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        Activity activity = (Activity) context;

        try {
            mOnButtonClicked = (RFIDRecordingFragment.onButtonClicked) activity;
        } catch(ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }
    /**
     * A class containing all the items to be displayed in a row.
     */
    public class TagScanResult {
        private String uuid = "";
        private String xAbs = "";
        private String yAbs = "";
        private String size = "";
        private String shape = "";

        /**
         * @return the uuid of the tag scan result object in a String format to be displayed on the
         * listView
         */
        public String getUuid() {
            return uuid;
        }

        /**
         * Updates the uuid of the scan result object
         * @param paramUuid
         *              the new uuid
         */
        public void setUuid(String paramUuid){
            this.uuid = paramUuid;
        }

        /**
         * @return the x-axis coordinates of the tag scan result object in a String format to be
         * displayed on the listView
         */
        public String getxAbs() {
            return xAbs;
        }

        /**
         * Updates the x-axis coordinates of the scan result object
         * @param paramXAbs
         *              the new x-axis coordinates
         */
        public void setxAbs(String paramXAbs){
            this.xAbs = paramXAbs;
        }

        /**
         * @return the y-axis coordinates of the tag scan result object in a String format to be
         * displayed on the listView
         */
        public String getyAbs() {
            return yAbs;
        }
        /**
         * Updates the y-axis coordinates of the scan result object
         * @param paramYAbs
         *              the new y-axis coordinates
         */
        public void setyAbs(String paramYAbs){
            this.yAbs = paramYAbs;
        }

        /**
         * @return the size of the tag scan result object in a String format to be displayed on the
         * listView
         */
        public String getSize() {
            return size;
        }

        /**
         * Updates the size of the scan result object
         * @param paramSize
         *              the new size
         */
        public void setSize(String paramSize){
            this.size = paramSize;
        }

        /**
         * @return the shape of the tag scan result object in a String format to be displayed on the
         * listView
         */
        public String getShape() {
            return shape;
        }

        /**
         * Updates the size of the scan result object
         * @param paramShape
         *              the new shape
         */
        public void setShape(String paramShape){
            this.shape = paramShape;
        }
    }
}


