//
//
// Smart Label Solutions, Inc.
// InventoryRunnable.java
//
//

package com.example.judith.bluetoothtest.Classes.UHF_SLS;

import android.content.Context;
import android.util.Log;

import com.example.judith.bluetoothtest.Classes.ReaderToActivity;

import java.util.List;

/**
 * InventoryRunnable is the Runnable class representing a tag polling, or tag inventory performed by
 * the UHF SmartLink reader.
 */
public class InventoryRunnable implements Runnable {

    private String LOG_TAG = "Inventory_Runnable";
    private boolean mIsInventorying;

    /**
     * An Interface to communicate with the Activity running.
     */
    private ReaderToActivity mInterface;

    /**
     * The UHF reader instance performing the tag inventory.
     */
    private UHFReader mUHFReader;

    InventoryRunnable(Context context, UHFReader uhfReader) {
        mInterface = (ReaderToActivity)context;
        mIsInventorying = false;
        mUHFReader = uhfReader;
    }

    public void run() {
        //Log.i(LOG_TAG, "Inventory Runnable started");
        mUHFReader.singleInventory();
        if (mIsInventorying) mInterface.postRunnableToHandler(this);

    }

    /**
     * Method called when an answer from a tag is detected by the reader. The reader can detect
     * a lot of tag simultaneously, including detecting the same tag several times in a very small
     * time interval before the polling is stopped. Because of this, and because only one answer at
     * a time is wanted, the method only sends to the activity the first element of the list, i.e.
     * the first tag detected.
     * @param errorCode
     *              the error code for the communication; if equal to 0 no error occurred.
     * @param list
     *          A list of EPCReply objects containing all the EPC detected during the inventory.
     * @see UHFReader#singleInventory()
     */
    void responseRun(int errorCode, List<EpcReply> list) {
        //Log.i(LOG_TAG, "i = " + errorCode);
        if (list != null) {
            Log.i(LOG_TAG, "list size = " + list.size());
            if (list.size() == 0) {
                Log.i(LOG_TAG, "No smartLINK Tag Read");
            } else {
                Log.i(LOG_TAG, "responseRun success!");
                if (list != null) {
                    EpcReply reply = list.get(0);
                    Log.i(LOG_TAG, "EPC value = " + UHFUtils.hex2strWithSpace(reply.getEpc()));
                    //mInterface.sendTagToDecode(UHFUtils.hex2strWithSpace(reply.getEpc()).replace(" ", ""));
                }
            }
        }
    }

    void toggleIsInventorying(boolean newValue) {
        mIsInventorying = newValue;
    }

}
