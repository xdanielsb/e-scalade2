package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DoneEditorActionListener;
import com.example.judith.bluetoothtest.R;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

/**
 * <b> Activity to choose the file that is going to be written in recording mode.</b>
 * <p>
 *     The recording mode copies a file containing information about a climbing wall and fills
 *     the copy with the information missing, i.e. the tags UUID (each RFID tag being fixed on one
 *     climbing hold).
 *     In this activity, the user chooses the file that is going to be filled with this information.
 *     He can either create a new file or choose an existing file, but this file must be absolutely
 *     the same size of the first file in order to correctly copy the data (the same size means the
 *     same number of lines).
 *     If the file is created, it will automatically be filled with random data just to be sure that
 *     its size is the correct one.
 * </p>
 * <p>
 *     The files are stored preferably in a special folder in the phone filesystem named
 *     "Climbing_Tables". If this folder doesn't exists, it is created by the activity when creating
 *     a new file and this new file is automatically stored in Climbing_Tables.
 * </p>
 * <p>
 *     When the file is created or chosen by the user its uri is saved in DataKeeper.
 * </p>
 * @see DataKeeper#setUriRecordingFile(Uri)
 */

public class RecordingFileChoiceActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int READ_REQUEST_CODE = 42;
    private static final String TAG = "RECORD FILE ACTIVITY";

    private Button mButtonSearch;
    private Button mButtonCreate;
    private Button mButtonOk;
    private TextView mTextViewFileSelected;
    private EditText mEditText;

    private String mCreatedFileName = "";
    private String mFileName = "";
    private int mSize;

    private File mFolder;
    private File mFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recording_file_choice);

        Intent separationActivityIntent = getIntent();
        mSize = separationActivityIntent.getIntExtra("TABLE SIZE", 0);

        mButtonSearch = (Button)findViewById(R.id.button_search_table_choice_activity);
        mButtonCreate = (Button)findViewById(R.id.button_create_recording_file_choice_activity);
        mButtonOk = (Button)findViewById(R.id.button_ok_recording_file_choice_activity);
        mButtonSearch.setOnClickListener(this);
        mButtonCreate.setOnClickListener(this);
        mButtonOk.setOnClickListener(this);

        mEditText = (EditText)findViewById(R.id.edit_fil_recording_choice_recording_file_choice_activity);
        mEditText.setOnEditorActionListener(new DoneEditorActionListener());
        mTextViewFileSelected = (TextView)findViewById(R.id.text_fileSelected_recording_file_choice_activity);

        /* Creation of the Climbing_Tables folder if not already existing */

        mFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + getString(R.string.folder_name_climbing_tables));
        if(!mFolder.exists()){
            mFolder = GeneralUtils.getPublicStorageDirectory(getString(R.string.folder_name_climbing_tables));
            GeneralUtils.toast(getApplicationContext(), "Folder created :" + mFolder);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_search_table_choice_activity:
                performFileSearch();
            break;

            case R.id.button_create_recording_file_choice_activity:
                mCreatedFileName = mEditText.getText().toString() + ".csv";
                if(mCreatedFileName.equals(".csv")){
                    mEditText.requestFocus();
                    GeneralUtils.toast(getApplicationContext(),"Pas de nom de fichier !");
                }
                else{
                    mFile = new File(mFolder, mCreatedFileName);
                    if(!mFile.exists()) {
                        try {
                            mFile.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        GeneralUtils.toast(getApplicationContext(), "Le fichier existe déjà !");
                    }
                    mTextViewFileSelected.setText(mCreatedFileName);
                    Uri uri = Uri.fromFile(mFile);
                    Log.d("CHOICE ACT", "URI Of FILE CREATED = " + uri);
                    /* Save the uri in the datakeeper instance for the recording */
                    DataKeeper.getInstance().setUriRecordingFile(uri);
                    try{
                        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "w");
                        FileOutputStream fileOutputStream = new FileOutputStream(parcelFileDescriptor.getFileDescriptor());
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                        CSVWriter csvWriter = new CSVWriter(outputStreamWriter, ',', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
                        /* Fill the empty file with default values */
                        writeToCSV(mSize, csvWriter);
                    }
                    catch(FileNotFoundException e){
                        e.printStackTrace();
                    }
                }
            break;
            case R.id.button_ok_recording_file_choice_activity:
                mFileName = mTextViewFileSelected.getText().toString();
                if(mFileName.length() == 0){
                    GeneralUtils.toast(getApplicationContext(), getString(R.string.toast_no_selected_file));
                }
                else{
                    Intent bluetoothIntent = new Intent(RecordingFileChoiceActivity.this, BluetoothActivity.class);
                    startActivity(bluetoothIntent);
                }
            break;
        }
    }

    /* Function to fill the empty file created with default values to ensure it's size will be big enough for the recording */

    public static void writeToCSV(int size, CSVWriter csvWriter){
        for (int i=0;i<size;i++){
            String[] line = {"0","0","00 00 00 00 00 00 00 00","1000","1000","Main Droite et Gauche","Non renseignée", "Non renseignée"};
            csvWriter.writeNext(line);
        }
        try{
            csvWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void performFileSearch(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        /* Select only CSV files or folders */
        intent.setType("text/comma-separated-values");

        intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData){
        super.onActivityResult(requestCode, resultCode, resultData);
        if(requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri uri;
            if(resultData != null){
                uri = resultData.getData();
                Log.d(TAG, "Uri : " + uri);
                InputStream inputStream;
                try {
                    inputStream = getContentResolver().openInputStream(uri);
                }
                catch (IOException e){
                    e.printStackTrace();
                    return;
                }
                int i = GeneralUtils.countTableLines(inputStream);
                /* Check that the file size is enough to contain the data */
                if(i<mSize){
                    Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.toast_file_to_small,String.valueOf(mSize)), Toast.LENGTH_LONG);
                    toast.show();
                }
                else{
                    getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    String fileName = GeneralUtils.getFileName(uri, getApplicationContext());
                    /* Save the uri in the datakeeper instance for the recording */
                    DataKeeper.getInstance().setUriRecordingFile(uri);
                    mTextViewFileSelected.setText(fileName);
                    Log.d(TAG, "URI DATA KEEPER = " + DataKeeper.getInstance().getUriLectureFile());
                }
            }
        }
    }
}
