package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.Button;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> First activity to welcome the user.</b>
 * <p>
 *  *     This activity first retrieves a boolean indicating if it is the first time the app is used,
 *  *     and if yes, a new instance of DataKeeper is created.
 *  *     Then, according to the user's choice, the activity stores in DataKeeper the chosen mode,
 *  *     climbing or recording.
 *  * </p>
 * @see DataKeeper#initInstance()
 */

public class FirstActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mButtonClimb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        mButtonClimb = (Button)findViewById(R.id.button_climb_first_activity);
        mButtonClimb.setOnClickListener(this);

        /* Get preferences from last time */
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean firstTimeApplication = settings.getBoolean("First time", true);

        DataKeeper.getInstance();
    }

    @Override
    public void onClick(View v) {
        AccessibilityManager accessibilityManager = (AccessibilityManager)getSystemService(ACCESSIBILITY_SERVICE);
        boolean isAccessibilityEnabled = accessibilityManager.isEnabled();
        boolean isExploredByTouchEnabled = accessibilityManager.isTouchExplorationEnabled();
        if(isAccessibilityEnabled && isExploredByTouchEnabled) {
            DataKeeper.getInstance().setClimbingMode(true);
            Intent talkbackUseIntent = new Intent(FirstActivity.this, FileChoiceActivity.class);
            startActivity(talkbackUseIntent);
        }
        else{
            Intent climbIntent = new Intent(FirstActivity.this, SeparationActivity.class);
            startActivity(climbIntent);
        }
    }
}
