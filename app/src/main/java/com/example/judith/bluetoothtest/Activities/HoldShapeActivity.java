package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> HoldShapeActivity is the fourth activity of a series of 5 activities that allow the user
 * to customize the audio message he will hear during the climbing.</b>
 *<p> This activity allows him to choose or not to have an additional indication about next hold
 * shape.
 *<p>
 * Once the user has made his choice by selecting one button (yes or no), the result is kept
 * in the singleton call DataKeeper.
 * @see DataKeeper#setHoldShape(Boolean)
 *</p>
 */

public class HoldShapeActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButtonOui;
    private Button mButtonNon;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hold_shape);

        /* Layout */

        mButtonOui = (Button)findViewById(R.id.btnoui_hold_shape_activity);
        mButtonNon = (Button)findViewById(R.id.btnnon_hold_shape_activity);
        mTextView = (TextView)findViewById(R.id.text_hold_shape_activity);
        mButtonOui.setOnClickListener(this);
        mButtonNon.setOnClickListener(this);

        mTextView.setText(getApplicationContext().getString(R.string.hold_activity_text,getApplicationContext().getString(R.string.holdShape)));


    }

    @Override
    public void onClick(View v) {
        Intent formePriseIntent = new Intent(HoldShapeActivity.this, HoldSizeActivity.class);
        switch (v.getId()) {
            case R.id.btnoui_hold_shape_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setHoldShape(true);
                startActivity(formePriseIntent);
                break;
            case R.id.btnnon_hold_shape_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setHoldShape(false);
                startActivity(formePriseIntent);
                break;
        }
    }
}
