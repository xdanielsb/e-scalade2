package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;

/**
 * <b> DirectionActivity is the beginning of a series of 5 activities that allow the user to
 * customize the audio message he will hear during the climbing.</b>
 *<p> Each activity allows the user to modify one of the 5 parameters composing the audio message :
 * the direction of the next hold, the distance to the next hold, the precision or not of the hand
 * that should catch the hold, and precisions about the size and shape of the hold.
 * This first activity is about the manner in which the direction to the next hold is indicated.
 * 4 options are available :
 *<ul>
 *<li> in angle in degrees, </li>
 *<li> in a clock reference,</li>
 *<li> using cardinal points, </li>
 *<li> using the sides of the user (left, right, upper right, etc...).</li>
 *</ul>
 *</p>
 *<p>
 * Once the user has made his choice by selecting one button, the result is kept in the singleton
 * DataKeeper.
 * @see DataKeeper#setDirection(String)
 *</p>
 *
 */

public class DirectionActivity extends AppCompatActivity implements View.OnClickListener{

    private Button mButtonHour;
    private Button mButtonCard;
    private Button mButtonDegr;
    private Button mButtonSides;

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);

        /* Layout */

        mButtonHour = (Button) findViewById(R.id.btn_horaire_direction_activity);
        mButtonCard = (Button) findViewById(R.id.btn_card_direction_activity);
        mButtonDegr = (Button) findViewById(R.id.btn_degre_direction_activity);
        mButtonSides = (Button) findViewById(R.id.btn_cotes_direction_activity);
        mTextView = (TextView)findViewById(R.id.text_direction_activity);

        mButtonHour.setOnClickListener(this);
        mButtonCard.setOnClickListener(this);
        mButtonDegr.setOnClickListener(this);
        mButtonSides.setOnClickListener(this);
        mTextView.setText(getString(R.string.choice_introduction_message,4,getString(R.string.direction_activity)));

    }

    @Override
    public void onClick(View v) {
        Intent distanceIntent = new Intent(DirectionActivity.this, DistanceActivity.class);
        switch (v.getId()){
            case R.id.btn_horaire_direction_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDirection(getString(R.string.directionHour));
                Log.d("DIRECTION ACTIVITY", "DirectionData = " + DataKeeper.getInstance().getDirection());
                startActivity(distanceIntent);
                break;
            case R.id.btn_card_direction_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDirection(getString(R.string.directionCard));
                Log.d("DIRECTION ACTIVITY", "DirectionData = " + DataKeeper.getInstance().getDirection());

                startActivity(distanceIntent);
                break;

            case R.id.btn_degre_direction_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDirection(getString(R.string.directionDegr));
                Log.d("DIRECTION ACTIVITY", "DirectionData = " + DataKeeper.getInstance().getDirection());

                startActivity(distanceIntent);
                break;
            case R.id.btn_cotes_direction_activity:
                /* Save data in the Data Keeper */
                DataKeeper.getInstance().setDirection(getString(R.string.directionSides));
                Log.d("DIRECTION ACTIVITY", "DirectionData = " + DataKeeper.getInstance().getDirection());
                startActivity(distanceIntent);
                break;
        }
    }
}
