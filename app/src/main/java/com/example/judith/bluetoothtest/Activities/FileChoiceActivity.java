package com.example.judith.bluetoothtest.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.R;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * <b> Activity to choose the file that is going to be read during the climbing.</b>
 *<p>
 *     This file represents the climbing wall with each line containing an information about one
 *     hold of the climbing path. Fore more information about the format of the CSV file, see
 *     (to come).
 *</p>
 *<p>
 *     The activity first get the default preferences registered by the user in
 *     ValidationAudioActivity of last use of the app, and set them into DataKeeper. The file used
 *     last time the app was launched is also retrieved from the preferences. If this is the first
 *     time the app is launched,default parameters and a default file stored in the app files are
 *     set.
 *</p>
 *<p>
 *     The user has then the possibility to change the file, by browsing into the phone filesystem.
 *     He can also check the file by selecting the mButtonCheckFile button, that will get him to
 *     TableActivity.
 *</p>
 * @see ValidationAudioActivity#onClick(View)
 * @see DataKeeper
 */

public class FileChoiceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int READ_REQUEST_CODE = 42;
    public static final String TAG = FileChoiceActivity.class.getSimpleName();

    private InputStream mInputStream;
    private String fileName;

    private TextView mTextView;
    private TextView mChosenMode;
    private Button mButtonBrowse;
    private Button mButtonFileName;
    private Button mButtonCheckFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_choice);


        Intent intent = getIntent();
        boolean firstTimeApp = intent.getBooleanExtra("Repeat",false);

        mTextView = (TextView)findViewById(R.id.text_fileChoice_activity);
        mChosenMode = (TextView)findViewById(R.id.text_chosenMode_fileChoice_activity);

        mButtonBrowse = (Button)findViewById(R.id.btn_search_fileChoice_activity);
        mButtonFileName = (Button)findViewById(R.id.btn_validate_choice_fileChoice_activity);
        mButtonCheckFile = (Button)findViewById(R.id.btn_check_filChoice_activity);
        mButtonBrowse.setOnClickListener(this);
        mButtonFileName.setOnClickListener(this);
        mButtonCheckFile.setOnClickListener(this);

        mTextView.setText(getString(R.string.text_file_choice,getString(R.string.btn_check_climbing_path)));
        mChosenMode.setText(getString(R.string.display_chosen_mode, DataKeeper.getInstance().isClimbingMode()?
                getString(R.string.datakeeper_climbing_mode) : getString(R.string.datakeeper_recording_mode)));

        Uri uri;

        if(!firstTimeApp){

            /* Get preferences from last time and fills DataKeeper */
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            DataKeeper.getInstance().setDirection(settings.getString("Direction","DirectionH"));
            DataKeeper.getInstance().setDistance(settings.getString("Distance","DistanceCM"));
            DataKeeper.getInstance().setHandPrecision(Boolean.parseBoolean(settings.getString("PrecisionMain","true")));
            DataKeeper.getInstance().setHoldShape(Boolean.parseBoolean(settings.getString("FormePrise","false")));
            DataKeeper.getInstance().setHoldSize(Boolean.parseBoolean(settings.getString("TaillePrise","false")));

            uri = Uri.parse(settings.getString("Uri","Default"));
            Log.d("FILE CHOICE", "URI = " + uri);
            DataKeeper.getInstance().setUriLectureFile(uri);
        }

        else
            /* Default name for uri */
            uri = Uri.parse("Default");
            DataKeeper.getInstance().setUriLectureFile(uri);
            Log.d("UTI DATA KEEPER", "URI = " +DataKeeper.getInstance().getUriLectureFile());

        if( uri.toString().equals("Default") || GeneralUtils.getFileName(uri, getApplicationContext()).equals("uri not valid") ) {
            fileName = "table.csv";
            mInputStream = getApplicationContext().getResources().openRawResource(R.raw.table);
        }

        else {
            fileName = GeneralUtils.getFileName(uri, getApplicationContext());
            try {
                mInputStream = getContentResolver().openInputStream(uri);
            }catch(IOException e ) {
                e.printStackTrace();
            }
        }

        mButtonFileName.setText(getString(R.string.display_selected_file,fileName));
        Log.d(TAG, "URI FILE NAME = " + fileName);

        getApplicationContext().grantUriPermission("com.example.judith.bluetoothtest",
                uri,Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

        DataKeeper.getInstance().setBeginnerMode(false); // Set the beginner mode to false to display all the holds in TableActivity
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.btn_search_fileChoice_activity):
                performFileSearch();
                break;
            case(R.id.btn_validate_choice_fileChoice_activity):

                Uri uri = DataKeeper.getInstance().getUriLectureFile();
                mInputStream = GeneralUtils.getStreamFromUri(uri, getApplicationContext());
                /* Check that the file isn't empty */
                int i = GeneralUtils.countTableLines(mInputStream);
                if(i==0){
                    Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.toast_empty_file_file_choice_activity), Toast.LENGTH_LONG);
                    toast.show();
                }
                else {
                    /* If the user chose the climbing mode */
                    if(DataKeeper.getInstance().isClimbingMode()){
                        Intent climbIntent = new Intent(FileChoiceActivity.this, DifficultyChoiceActivity.class);
                        startActivity(climbIntent);
                    }
                    else{
                        Intent recordIntent = new Intent(FileChoiceActivity.this, RecordingFileChoiceActivity.class);
                        recordIntent.putExtra("TABLE SIZE", i);
                        startActivity(recordIntent);
                    }
                }
                break;
            case (R.id.btn_check_filChoice_activity):
                Intent checkTableIntent = new Intent(FileChoiceActivity.this, TableActivity.class);
                startActivity(checkTableIntent);
                break;
        }

    }

    /**
     * Open the default android file browser to browse among the files. Look only for CSV files.
     */
    public void performFileSearch(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        /* Select only CSV files or folders */
        intent.setType("text/comma-separated-values");
        intent.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    /**
     * Gets the result of the call of startActivityForResult in performFileSearch method, and put
     * the result passed via resultData (the file selected) into a Uri object.
     * Then gives the Uri persistable permission to read and write the file, and save it into
     * DataKeeper
     * @see DataKeeper#setUriLectureFile(Uri)
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData){
        super.onActivityResult(requestCode, resultCode, resultData);
        if(requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            Uri uri = null;
            if(resultData != null){
                uri = resultData.getData();
                Log.d(TAG, "Uri : " + uri);
                getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                String fileName = GeneralUtils.getFileName(uri, getApplicationContext());
                DataKeeper.getInstance().setUriLectureFile(uri);
                mButtonFileName.setText(getString(R.string.display_selected_file,fileName));
                Log.d(TAG, "URI DATA KEEPER = " + DataKeeper.getInstance().getUriLectureFile());

            }
        }
    }
}
