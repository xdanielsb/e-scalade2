package com.example.judith.bluetoothtest.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.judith.bluetoothtest.Classes.DataSampleHashMap;
import com.example.judith.bluetoothtest.Classes.GeneralUtils;
import com.example.judith.bluetoothtest.Classes.DataKeeper;
import com.example.judith.bluetoothtest.Classes.DataSample;
import com.example.judith.bluetoothtest.R;

import java.io.InputStream;

/**
 * <b> AudioActivity is an activity only available to the user in climbing mode.</b>
 * In AudioActivity, the user has the opportunity to listen to a typical audio message that he will
 * hear during the ascent.
 * The user then has a choice between keeping the guiding mode if its suits him, or to modify it
 * before climbing.
 * The sample read to the reader as an example is the first element of the CSV file (i.e. the first
 * line).
 * The user has the opportunity to listen to the audio message as many time as wanted, thanks to the
 * mListenAgainButton button.
 *
 */

public class AudioActivity extends AppCompatActivity implements View.OnClickListener, TextToSpeech.OnInitListener{

    private Button mModification;
    private Button mStartClimbing;
    private Button mListenAgain;
    private TextView mTextView;

    DataSampleHashMap DataSampleList = new DataSampleHashMap();

    /* Audio playback*/
    private TextToSpeech mSpeaker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        /* Layout */
        mTextView = (TextView)findViewById(R.id.begin_text_audio_activity);
        mModification = (Button) findViewById(R.id.btn_change_message_audio_activity);
        mStartClimbing = (Button) findViewById(R.id.btn_climbing_audio_activity);
        mListenAgain = (Button)findViewById(R.id.btn_listen_again_audio_activity);

        mListenAgain.setContentDescription(getString(R.string.talkback_listen_again_button,mListenAgain.getText()));
        mStartClimbing.setContentDescription(getString(R.string.talkback_start_climbing,mStartClimbing.getText()));
        mModification.setContentDescription(getString(R.string.talkback_change_guiding,mModification.getText()));

        mTextView.setText(getString(R.string.choose_option,
                (DataKeeper.getInstance().isBeginnerMode() ?
                        getString(R.string.datakeeper_guiding_difficulty_beginner) : getString(R.string.datakeeper_guiding_difficulty_advanced)),
                getString(R.string.listen_audioMessage), getString(R.string.change_guiding), getString(R.string.start_climbing)));
        mModification.setOnClickListener(this);
        mStartClimbing.setOnClickListener(this);
        mListenAgain.setOnClickListener(this);

        Uri mUri = DataKeeper.getInstance().getUriLectureFile();

        InputStream inputStream;

        if(mUri.toString().equals("Default")){
            inputStream = getApplicationContext().getResources().openRawResource(R.raw.table);
        }
        else{
          inputStream = GeneralUtils.getStreamFromUri(mUri,getApplicationContext());
        }

        /* Lecture of CSV Table*/
        DataSampleList = GeneralUtils.readTableData(getApplicationContext(),inputStream);

        mSpeaker = new TextToSpeech(this, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_climbing_audio_activity:
                Intent bluetoothIntent = new Intent(AudioActivity.this, BluetoothActivity.class);
                startActivity(bluetoothIntent);
                break;

            case R.id.btn_change_message_audio_activity:
                Intent tableActivityIntent = new Intent(AudioActivity.this, DirectionActivity.class);
                startActivity(tableActivityIntent);
                break;
            case R.id.btn_listen_again_audio_activity:
                DataSample sample = DataSampleList.getDataSampleByIndex(1);
                GeneralUtils.audioLectureUtils(getApplicationContext(),sample, sample,mSpeaker, true,0);
                break;
        }
    }

    protected void onStop(){
        if(mSpeaker != null) {
            mSpeaker.shutdown();
        }
        super.onStop();
    }

    @Override
    public void onInit(int status) {
        if(status == TextToSpeech.SUCCESS){
            Log.d("TEXT TO SPEECH", "SUCCESS");
        }
    }
}
