package com.example.judith.bluetoothtest.Classes;

/**
 * <b>ReaderToActivity is an interface used by the RFID readers to communicate with the activity
 * running.</b>
 * The reader sends via the implemented methods either information or a runnable to be executed by
 * the activity.
 **/
public interface ReaderToActivity {

    //Context getMainActivityContext();
    void postRunnableToHandler(Runnable runnable);
    void postDelayedRunnableToHandler(Runnable runnable, long delayTime);
    void removeRunnableFromHandler(Runnable runnable);
    void postToastMessage(String message);
    void onTagRead(String tagID, int deviceNumber); //maybe add rssi as a parameter ?

}
