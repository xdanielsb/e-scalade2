package com.example.judith.bluetoothtest.Classes.GeneralRFID;

import android.app.Activity;
import android.bluetooth.BluetoothManager;
import android.content.Context;

import com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCBluetoothManager;

/**
 * RFIDBluetoothManager is a wrapper class for every instance of bluetooth managers for the readers.
 * This wrapper is required mostly because the ACS NFC readers have their own Bluetooth Manager
 * class to handle the readers, the BluetoothReaderManager class, wrapped for more clarity and
 * abstraction here in the NFCBluetoothManager class.
 * This wrapper also include an instance of a classic BluetoothManager in case of the adding of some
 * other type of RFID reader in the future that might use it, but so far the SmartLink UHF reader
 * doesn't need a BluetoothManager.
 */

public class RFIDBluetoothManager {

    /**
     * The instance of NFC Bluetooth Manager, wrapping the ACS API bluetooth manager.
     * @see NFCBluetoothManager
     * @see com.acs.bluetooth.BluetoothReaderManager
     */
    private NFCBluetoothManager mNFCBluetoothManager;

    /**
     * An instance of a classical Bluetooth API BluetoothManager.
     */
    private BluetoothManager mBluetoothManager;

    /**
     * The instance of the RFID Bluetooth Reader handled by the manager. Not in use so far.
     */
    private RFIDBluetoothReader mRFIDBluetoothReader;


    /**
     * int to identify the reader, 1 or 2.
     */
    private int mDeviceNumber;

    /**
     * An int indicating which type of reader is used : 1 for UHF and 2 for NFC. This type is set
     * in the reader constructor thanks to the deviceName String which contains the formal name
     * of the BluetoothDevice.
     * This int allows then the class to know which type of Bluetooth manager to construct when
     * called in ReadersActivity.
     */
    private int mDeviceType;

    private Context mContext;

    public RFIDBluetoothManager(Activity activity, int deviceNumber, RFIDBluetoothReader rfidBluetoothReader, String deviceName){
        mContext = (Context)activity;
        mDeviceNumber = deviceNumber;
        mRFIDBluetoothReader = rfidBluetoothReader;
        if(deviceName.contains("smartLINK"))
            mDeviceType = 1;
        else if(deviceName.contains("ACR")) {
            mDeviceType = 2;
            mNFCBluetoothManager = new NFCBluetoothManager(activity, deviceNumber, rfidBluetoothReader.getNFCReader());
        }
    }

    /**
     * @return the instance of Bluetooth manager for NFC readers.
     * @see com.example.judith.bluetoothtest.Classes.NFC_ACS.NFCBluetoothGattCallback
     */
    NFCBluetoothManager getNFCBluetoothManager(){
        return mNFCBluetoothManager;
    }

    /**
     *  @return a boolean indicating if the instance of Bluetooth Manager is null. True if yes,
     *  false if not.
     */
    public boolean isBluetoothManagerNull() {
        if (mDeviceType == 1)
            return (mBluetoothManager == null);
        else if (mDeviceType == 2)
            return (mNFCBluetoothManager == null);
        return true;
    }



}
