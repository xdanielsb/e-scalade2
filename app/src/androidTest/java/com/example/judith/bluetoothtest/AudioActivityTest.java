package com.example.judith.bluetoothtest;

import androidx.test.espresso.accessibility.AccessibilityChecks;
import androidx.test.espresso.intent.rule.IntentsTestRule;

import com.example.judith.bluetoothtest.Activities.AudioActivity;
import com.example.judith.bluetoothtest.Activities.FileChoiceActivity;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNull.notNullValue;

public class AudioActivityTest {

    @BeforeClass
    public static void enableAccessibilityChecks(){
        AccessibilityChecks.enable();
    }

    @Rule
    public IntentsTestRule<AudioActivity> mActivityRule = new IntentsTestRule<>(AudioActivity.class);

    @Test
    public void buttonListenAgainTest(){


        onView(withId(R.id.btn_listen_again_audio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_listen_again_audio_activity)).perform(click());
    }

    @Test
    public void buttonValidateTest(){


        onView(withId(R.id.btn_change_message_audio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_change_message_audio_activity)).perform(click());

    }

    @Test
    public void buttonCheckTest(){
        onView(withId(R.id.btn_climbing_audio_activity)).check(matches(notNullValue()));
        onView(withId(R.id.btn_climbing_audio_activity)).perform(click());
    }
}
