package com.example.judith.bluetoothtest;

import androidx.test.espresso.accessibility.AccessibilityChecks;
import androidx.test.espresso.intent.rule.IntentsTestRule;

import com.example.judith.bluetoothtest.Activities.FileChoiceActivity;
import com.example.judith.bluetoothtest.Activities.SeparationActivity;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNull.notNullValue;

public class FileChoiceTest {

    @BeforeClass
    public static void enableAccessibilityChecks(){
        AccessibilityChecks.enable();
    }

    @Rule
    public IntentsTestRule<FileChoiceActivity> mActivityRule = new IntentsTestRule<>(FileChoiceActivity.class);

    @Test
    public void buttonFileChoiceTest(){


        onView(withId(R.id.btn_search_fileChoice_activity)).check(matches(notNullValue()));
        //onView(withId(R.id.btn_search_fileChoice_activity)).perform(click());
    }

    @Test
    public void buttonValidateTest(){


        onView(withId(R.id.btn_validate_choice_fileChoice_activity)).check(matches(notNullValue()));
        //onView(withId(R.id.btn_validate_choice_fileChoice_activity)).perform(click());

    }

    @Test
    public void buttonCheckTest(){
        onView(withId(R.id.btn_check_filChoice_activity)).check(matches(notNullValue()));
        //onView(withId(R.id.btn_check_filChoice_activity)).perform(click());
    }




}
